import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import NavBar from "./component/NavBar";

const MainRoute = () => {
  return (
    <BrowserRouter>
      <NavBar/>
    </BrowserRouter>
  );
};
export default MainRoute;
