import React, { useState, useEffect } from "react";
import { Row, Col, Card, Icon, Avatar } from "antd";
import {
  Form,
  Input,
  Tooltip,
  Cascader,
  Select,
  Checkbox,
  Button,
  AutoComplete
} from "antd";
import { fetchData } from "../reducer/GameReducer";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import { SubmitAction } from "../actions/GameActions";
const EditFormPage = props => {
  let history = useHistory();
  const { fetchData, SubmitAction } = props;
  const { data, loading } = props.data;
  const [current, setCurrent] = useState();
  const [name, setName] = useState();
  const [image, setImage] = useState();
  const [price, setPrice] = useState();
  const [editMode, setEditMode] = useState(false);
  const [editName , setEditname] = useState(false);

  useEffect(() => {
    if (data.length == 0) {
      fetchData();
    }
  }, []);

  useEffect(() => {
    getDetail();
  }, [data]);

  useEffect(() => {
    if (current) {
      setName(current.name);
      setImage(current.image);
      setPrice(current.price);
    }
  }, [current]);

  const getDetail = () => {
    const filter = data.find(value => {
      return value.id == props.match.params.id;
    });
    setCurrent(filter);
  };
  const handleSubmit = event => {
    event.preventDefault();
    const data = {
      id: +props.match.params.id,
      name: name,
      image: image,
      price: price
    };
    console.log(data);
    SubmitAction(data);
    history.push("/");
  };
  const edit = () => {
    setEditMode(!editMode);
  };
  return (
    <div>
      <h1 style={{ color: "white" }}>
        | EditFormPage ID : {props.match.params.id}
      </h1>
      <div style={{ margin: "10px 0 10px 0" }}>
        <Button type="danger" onClick={() => edit()}>
          Edit
        </Button>
      </div>
      <Row>
        <Form onSubmit={handleSubmit}>
          <Row>
            <Col span={7}>
              {editName ? (
                <Form.Item>
                  <Input
                    style={{
                      backgroundColor: "black",
                      color: "white",
                      width: "200px"
                    }}
                    type="text"
                    placeholder="name"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }}
                  />
                  <a onClick={()=>setEditname(!editName)}>save</a>
                </Form.Item>
              ) : (
                <h2><a onClick={()=>setEditname(!editName)}><Icon type="edit"/></a>Name : {name}</h2>
              )}
            </Col>
            <Col span={5}>
              {editMode ? (
                <Form.Item>
                  <Input
                    style={{
                      backgroundColor: "black",
                      color: "white",
                      width: "200px"
                    }}
                    type="text"
                    value={image}
                    onChange={e => setImage(e.target.value)}
                  />
                </Form.Item>
              ) : (
                <h2>Image url : {image}</h2>
              )}
            </Col>
            <Col span={5}>
              {editMode ? (
                <Form.Item>
                  <Input
                    style={{
                      backgroundColor: "black",
                      color: "white",
                      width: "200px"
                    }}
                    placeholder="price"
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                  />
                </Form.Item>
              ) : (
                <h2>Price : {price}</h2>
              )}
            </Col>
          </Row>
          <Row>
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Update
              </Button>
            </Form.Item>
          </Row>
        </Form>
      </Row>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    data: state.data
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData, SubmitAction }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFormPage);
