import React, { useState, useEffect } from "react";
import { Layout, BackTop, Row, Col } from "antd";
import PageHeaderSection from "../component/EditpageComponents/EditFormPage/FirstSection/PageHeaderSection";
import VideoSection from "../component/EditpageComponents/EditFormPage/FirstSection/VideoSection";
import DetailsSection from "../component/EditpageComponents/EditFormPage/SecondSection/DetailsSection";
import SystemRequireSection from "../component/EditpageComponents/EditFormPage/SecondSection/SystemRequireSection";
import ReviewsSection from "../component/EditpageComponents/EditFormPage//ThirdSection/ReviewsSection";
import {
  Button,
  Spin
} from "antd";
import {fethComments,fetchData} from '../reducer/GameReducer'
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
import { SubmitAction } from "../actions/GameActions";
const EditFormPage = props => {
  let history = useHistory();
  const { fetchData, SubmitAction ,fethComments } = props;
  const { loadingDetail, data ,comments} = props.data;
  const [targetData, settargetData] = useState();

  const [name, setName] = useState();
  const [movie, setMovie] = useState();
  const [detail, setDetail] = useState();
  const [release, setRelease] = useState();
  const [develop, setDevelop] = useState();
  const [publisher, setPublisher] = useState();
  const [image, setImage] = useState();
  const [price, setPrice] = useState();
  const [genres, setGenres] = useState();
  const [album, setAlbum] = useState();
  const [pc, setPc] = useState();
  const [linux, setLinux] = useState();
  const [mac, setMac] = useState();
  const { Content } = Layout;

  useEffect(() => {
      fetchData();
    if(comments.length == 0 ){
      fethComments();
    }
  }, []);
  console.log(data);
  useEffect(() => {
    if (targetData) {
      setName(targetData.name);
      setImage(targetData.img);
      setPrice(targetData.price);
      setDetail(targetData.detailed_description);
      setDevelop(targetData.developers);
      setRelease(targetData.release_date.date);
      setPublisher(targetData.publishers);
      setGenres(targetData.genres);
      setAlbum(targetData.screenshots);
      setPc(targetData.pc_requirements.minimum);
      setLinux(targetData.linux_requirements.minimum);
      setMac(targetData.mac_requirements.minimum);
      setMovie(targetData.movies[0].webm.max)
    }
  }, [targetData]);

  useEffect(() => {
    getDetail();
  }, [data]);

  const getDetail = () => {
    const filter = data.find(value => {
      return value.steam_appid == props.match.params.id;
    });
    settargetData(filter);
  };
  const Submit = () => {
    const data = {
      steam_appid: +props.match.params.id,
      name: name,
      img: image,
      price: price,
      detailed_description: detail,
      release_date: {
        date: release
      },
      developers: develop,
      publishers: publisher,
      genres: genres,
      screenshots: album,
      pc_requirements: {
        minimum: pc
      },
      linux_requirements: {
        minimum: linux
      },
      mac_requirements: {
        minimum: mac
      }
    };
    SubmitAction(data);
    history.push("/edit");
  };

  return (
    <Layout
      style={{ backgroundColor: "#2B2B2B", height: "100vh", overflow: "auto" }}
    >
      <Spin spinning={loadingDetail}>
        <Content>
          <Content className="gameBackground">
            <Button
              type="primary"
              onClick={Submit}
              style={{
                position: "fixed",
                zIndex: "1",
                margin: "10px",
                textAlign: "center",
                bottom: "50px",
                right: "100px"
              }}
            >
              SAVE
            </Button>
            <Row type="flex" justify="start">
              <Col>
                <PageHeaderSection
                  //editMode={editMode}
                  setName={setName}
                  name={name}
                />
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col>
                <VideoSection movie={movie}/>
              </Col>
            </Row>
          </Content>

          <Row>
            <Col>
              <DetailsSection
                setDetail={setDetail}
                setDevelop={setDevelop}
                setPublisher={setPublisher}
                setRelease={setRelease}
                detail={detail}
                release={release}
                publisher={publisher}
                develop={develop}
                genres={genres}
                images={album}
              />
              <SystemRequireSection pc={pc} linux={linux} mac={mac} setPc={setPc} setLinux={setLinux} setMac={setMac}/>
              <ReviewsSection id={props.match.params.id} comments={comments}/>
            </Col>
          </Row>
        </Content>
        <BackTop />
      </Spin>
    </Layout>
  );
};

const mapStateToProps = state => {
  return {
    data: state.data
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ SubmitAction,fethComments,fetchData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFormPage);
