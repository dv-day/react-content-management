import React, { useEffect, useState } from "react";
import { Row, Col, Card, Icon, Avatar } from "antd";
import { Select, Spin } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Divider } from "antd";
import CardItem from "../component/EditpageComponents/CardItem";
import { useHistory } from "react-router-dom";
import { data } from "../data";
import { fetchData } from "../reducer/GameReducer";
const { Option } = Select;
const { Meta } = Card;
const EditPage = props => {
  const { fetchData } = props;
  const { data, loading } = props.data;
  const [sorting, setSorting] = useState("none");
  const handleChange = value => {
    setSorting(value);
  };

  useEffect(() => {
    if (data.length == 0) {
      fetchData();
    }
  }, []);

  useEffect(() => {
    sortArray();
  }, [sorting]);

  const sortArray = () => {
    if (sorting == "none") {
      return data;
    } else if (sorting == "byName") {
      return data.sort(function(a, b) {
        var nameA = a.name.toLowerCase(),
          nameB = b.name.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });
    } else if (sorting == "byPrice") {
      return data.sort(function(a, b) {
     
        if (a.price < b.price) return -1;
        if (a.price > b.price) return 1;
        return 0;
      });
    }
  };

  return (
    <div>
      <Row style={{ marginBottom: 20 }}>
        <h1 style={{ color: "white" }}> | Edit Page</h1>
      </Row>
      <Row style={{ marginBottom: 30 }}>
        <h3 style={{ color: "white", display: "inline-block" }}>Sort by :</h3>
        <Select
          defaultValue="sort"
          style={{ width: 120, display: "inline-block", marginLeft: 10 }}
          onChange={handleChange}
        >
          <Option value="byName">Name</Option>
          <Option value="byPrice">Price</Option>
        </Select>
      </Row>

      <Row>
        <Spin spinning={loading}>
          <CardItem data={sortArray()} />
        </Spin>
      </Row>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    data: state.data
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPage);
