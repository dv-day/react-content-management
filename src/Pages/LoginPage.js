import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
  Row,
  Col,
  notification,
  Spin
} from "antd";
// import { loginProcess, setUsername, setPassword } from "../actions/AuthAction";
import { loginProcess } from "../reducer/AuthReducer";
import { resetError } from "../actions/AuthAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";
const LoginPage = props => {
  //   const { getFieldDecorator } = this.props.form;
  //   const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  //     NormalLoginForm
  //   );
  const [username, SetUsername] = useState();
  const [password, SetPassword] = useState();
  let history = useHistory();
  const { loginProcess, resetError } = props;
  const { err, isLogin, process } = props.error;
  const handleSubmit = e => {
    e.preventDefault();
    //history.push("/processLogin");
    loginProcess(username, password);
  };

  useEffect(() => {
    if (isLogin == true) {
      history.push("/edit");
    }
  }, [isLogin]);
  useEffect(() => {
    if (err !== "") {
      openNotificationWithIcon("error");
      resetError();
    }
  });

  const openNotificationWithIcon = type => {
    notification[type]({
      message: "Can not login to the system",
      description: "Incorrect Username or password. Please try again."
    });
  };

  return (
    <div
      style={{
        background: "#000",
        padding: "50px 5%",
        minHeight: 280,
        margin: "auto",
        borderRadius: "10px",
        width: "650px"
      }}
    >
      <Spin spinning={process} style={{ backgroundColor: "#000"}}>
        <Row>
          <Col span={14}>
            <h1 style={{ color: "orange" }}>Log in</h1>
            <Form
              style={{ maxWidth: "300px" }}
              className="login-form"
              onSubmit={handleSubmit}
            >
              <Form.Item>
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.6)" }} />
                  }
                  style={{ backgroundColor: "black" }}
                  placeholder="Username"
                  value={username}
                  onChange={e => SetUsername(e.target.value)}
                />
              </Form.Item>
              <Form.Item>
                <Input.Password
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.6)" }} />
                  }
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={e => SetPassword(e.target.value)}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  // type="danger"
                  ghost
                  htmlType="submit"
                  style={{ width: "100%" }}
                  className="login-form-button"
                >
                  Login
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col span={1}>
            <div
              style={{ borderRight: "1px solid  white", minHeight: "250px" }}
            ></div>
          </Col>
          <Col xs={{ span: 8, offset: 1 }}>
            <h1 style={{ color: "orange" }}>Why log in?</h1>
            <h4 style={{ color: "#fff" }}>
              Log in to edit information
              <br />
              &emsp; It's easy to use. Continue on to edit the information, the
              leading digital content for PC and Mac gamers.
            </h4>
            <a style={{ alignSelf: "center" }}>Forget your password?</a>
            {/* <Button
              // type="danger"
              disabled
              ghost
              style={{ width: "100%" }}
            >
              Create new account
            </Button> */}
          </Col>
        </Row>
      </Spin>
      </div>
    
  );
};

const mapStateToProps = state => {
  return {
    error: state.login
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ loginProcess, resetError }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
