import { data } from "../data"
import axios from "axios"
import Axios from "axios"
export const FETCH_GAMELIST = 'FETCH_GAMELIST'
export const FETCH_DATA_LIST_SUCCESS = 'FETCH_DATA_LIST_SUCCESS'
export const FETCH_DATA_LIST_ERROR = 'FETCH_DATA_LIST_ERROR'
export const SEARCH_GAMELIST = 'SEARCH_GAMELIST'
export const FILTER_GAMELIST = 'FILTER_GAMELIST'


export const fetchData = () => {
  return dispatch => {
    dispatch(fetchDataBegin());
    setTimeout(() => {
      axios.get('http://18.141.146.223:4999/games')
      .then(function (response){
        dispatch(fetchDataSuccess(response.data))
      })
      .catch(function (error){
        dispatch(fetchDataError(error))
      })
    }, 1000);
  };
};

export const fetchDataBegin = () => {
  return {
    type: FETCH_GAMELIST
  };
};

export const fetchDataSuccess = value => {
  return {
    type: FETCH_DATA_LIST_SUCCESS,
    payLoad: value
  };
};

export const fetchDataError = error => {
  return {
    type: FETCH_DATA_LIST_ERROR,
    payLoad: error
  };
};


export const searchGameList = (value) => {
  return {
    type: SEARCH_GAMELIST,
    payLoad: value
  }
}

export const filter = (value) => {
  return {
    type: FILTER_GAMELIST,
    payLoad: value
  }
}


// export const fetchDataSuccess = () => {
//     return {
//         type: FETCH_GAMELIST,
//         // payLoad: data
//     }
// }
