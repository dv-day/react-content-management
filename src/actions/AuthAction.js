export const LOGIN_BEGIN = "LOGIN_BEGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";
export const SET_USERNAME = "SET_USERNAME";
export const SET_PASSWORD = "SET_PASSWORD";

export const loginBegin = data => {
  return {
    type: LOGIN_BEGIN,
    payLoad: data
  };
};
export const loginSucess = data => {
  return {
    type: LOGIN_SUCCESS,
    payLoad: data
  };
};
export const loginFail= () => {
  return {
    type: LOGIN_FAIL
  };
};

export const logout = () => {
  return {
    type: LOGOUT
  };
};

export const setUsername = username => {
  return {
    type: SET_USERNAME,
    payLoad: username
  };
};

export const resetError = () => {
  return {
    type: 'RESET_ERROR'
  };
};

export const setPassword = password => {
  return {
    type: SET_PASSWORD,
    payLoad: password
  };
};
