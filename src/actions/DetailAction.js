import axios from 'axios';
export const FETCH_GAME_DETAIL_BEGIN = 'FETCH_GAME_DETAIL_BEGIN'
export const FETCH_GAME_DETAIL_SUCCESS = 'FETCH_GAME_DETAIL_SUCCESS'
export const FETCH_GAME_DETAIL_ERROR = 'FETCH_GAME_DETAIL_ERROR'

export const fetchData = (id) => {
  return dispatch => {
    dispatch(fetchDataBegin());
    setTimeout(() => {
      axios.get('http://18.141.146.223:4999/games/'+id)
  .then(function (response) {
    console.log(response)
    dispatch(fetchDataSuccess(response.data));
  })
  .catch(function (error) {
    dispatch(fetchDataError(error))
  })
    }, 1000);
  };
};




export const fetchDataBegin = () => {
    return {
      type: FETCH_GAME_DETAIL_BEGIN
    }
  }
  
  export const fetchDataSuccess = value => {
    return {
      type: FETCH_GAME_DETAIL_SUCCESS,
      payLoad: value
    }
  }
  
  export const fetchDataError = error => {
    return {
      type: FETCH_GAME_DETAIL_ERROR,
      payLoad: error
    }
  }