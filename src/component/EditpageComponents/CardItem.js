import React from "react";
import { Row, Col, Card, Icon, Avatar } from "antd";
import { useHistory } from "react-router-dom";
const CardItem = ({ data }) => {
  let history = useHistory();
  return data.map((value, index) => {
    const styles = {
      box: {
        margin: "10px",
        width: 216,
        height: 288,
        backgroundImage: `url(${value.img})`,
        backgroundSize: "cover",
        borderRadius: "5px"
      }
    };

    return (
      <Col className="GameItem" key={index} style={styles.box} span={3}>
        <div
          style={{
            position: "absolute",
            bottom: "0",
            background: "rgba(0, 0, 0, 0.9)",
            width: "100%",
            height: "30%",
            borderRadius: "0 0 4px 5px",
            color: "white",
            textAlign: "left",
            padding: "10px"
          }}
        >
          <p style={{ fontWeight: "bold" }}>{value.name}</p>
          <a onClick={() => history.push("edit/" + value.steam_appid)}>
            <Icon
              type="edit"
              key="edit"
              style={{
                position: "absolute",
                left: "10px",
                bottom: "0",
                marginBottom: "14px",
                fontSize: "16px"
              }}
            />
         
          </a>

          <p style={{ position: "absolute", right: "10px", bottom: "0" }}>
            {value.price > 0 ? value.price : "Free"}
          </p>
        </div>
      </Col>
    );
  });
};

export default CardItem;
