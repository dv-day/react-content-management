import React, { useState } from "react";
import { Layout, Icon,Form, Row, PageHeader, List, Input } from "antd";
import { useSelector } from "react-redux";

const PageHeaderSection = ({ name, setName }) => {
  const { Content } = Layout;
  const data = useSelector(state => state.data.targetData);
  const [editName, setEditname] = useState(false);

  return (
    <Content>
      {editName ? (
        <Form.Item>
        <Input
          style={{
            backgroundColor: "black",
            color: "white",
            width: "200px"
          }}
          type="text"
          placeholder="name"
          value={name}
          onChange={e => {
            setName(e.target.value);
          }}
        />
        <a onClick={()=>setEditname(!editName)}> <Icon type="check" style={{fontSize : "20px"}}/></a>
      </Form.Item>
      ) : (
        <Row>
          <PageHeader title={name} style={{ display: "inline-block"}} />
          <a
            style={{ display: "inline-block", fontSize: "20px" }}
            onClick={() => setEditname(!editName)}
          >
            <Icon type="edit" />
          </a>
        </Row>
      )}
    </Content>
  );
};

export default PageHeaderSection;
