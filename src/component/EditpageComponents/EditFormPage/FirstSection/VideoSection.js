import React from "react";
import { Layout } from "antd";
import ReactPlayer from "react-player";

const VideoSection = ({movie}) => {
  const { Content } = Layout;

  return <ReactPlayer url={movie} playing controls={true} muted />;
};

export default VideoSection;
