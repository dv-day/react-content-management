import React, { useState } from "react";
import {
  Layout,
  Row,
  Col,
  Carousel,
  Form,
  Typography,
  Tag,
  Input,
  Icon
} from "antd";
import Slider from "react-slick";

const DetailsSection = ({
  detail,
  develop,
  release,
  publisher,
  setDetail,
  setDevelop,
  setRelease,
  setPublisher,
  genres,
  images
}) => {
  const { Content } = Layout;
  const { TextArea } = Input;
  const { Text } = Typography;
  const [editDetail, setEditdetail] = useState(false);
  const [editDevelop, setEditDevelop] = useState(false);
  const [editRelease, setEditRelease] = useState(false);
  const [editPublisher, setEditPublisher] = useState(false);
  console.log(images);
  return (
    <Content style={{ padding: "20px 50px" }}>
      <Row>
        <Col span={11}>
          <Slider autoplay dots={true}>
            {images
              ? images.map(value => (
                  <div>
                    <img src={value.path_full} />
                  </div>
                ))
              : ""}
          </Slider>
        </Col>
        <Col span={11} offset={1}>
          {editDetail ? (
            <Form.Item>
              <TextArea
                rows={20}
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "100%"
                }}
                type="text"
                placeholder="detail"
                value={detail}
                onChange={e => {
                  setDetail(e.target.value);
                }}
              />
              <a onClick={() => setEditdetail(!setEditdetail)}>
                {" "}
                <Icon type="check" style={{ fontSize: "20px" }} />
              </a>
            </Form.Item>
          ) : (
            <div>
              {" "}
              <div
                dangerouslySetInnerHTML={{
                  __html: detail
                }}
                style={{ color: "white" }}
              ></div>
              <a
                style={{ display: "inline-block", fontSize: "20px" }}
                onClick={() => setEditdetail(!editDetail)}
              >
                <Icon type="edit" />
              </a>
            </div>
          )}

          <br />
          <br />
          {editRelease ? (
            <Form.Item>
              <Input
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "200px"
                }}
                type="text"
                placeholder="detail"
                value={release}
                onChange={e => {
                  setRelease(e.target.value);
                }}
              />
              <a onClick={() => setEditRelease(!setEditRelease)}>
                {" "}
                <Icon type="check" style={{ fontSize: "20px" }} />
              </a>
            </Form.Item>
          ) : (
            <div>
              <Text style={{ color: "white" }}>RELEASE DATE: {release}</Text>
              <a
                style={{ display: "inline-block", fontSize: "20px" }}
                onClick={() => setEditRelease(!editRelease)}
              >
                <Icon type="edit" />
              </a>
            </div>
          )}

          <br />
          <br />
          {editDevelop ? (
            <Form.Item>
              <Input
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "200px"
                }}
                type="text"
                placeholder="developer"
                value={develop}
                onChange={e => {
                  setDevelop(e.target.value);
                }}
              />
              <a onClick={() => setEditDevelop(!setEditDevelop)}>
                {" "}
                <Icon type="check" style={{ fontSize: "20px" }} />
              </a>
            </Form.Item>
          ) : (
            <div>
              <Text style={{ color: "white" }}>DEVELOPER: {develop}</Text>
              <a
                style={{ display: "inline-block", fontSize: "20px" }}
                onClick={() => setEditDevelop(!editDevelop)}
              >
                <Icon type="edit" />
              </a>
            </div>
          )}
          <br />
          <br />
          {editPublisher ? (
            <Form.Item>
              <Input
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "200px"
                }}
                type="text"
                placeholder="publisher"
                value={publisher}
                onChange={e => {
                  setPublisher(e.target.value);
                }}
              />
              <a onClick={() => setEditPublisher(!setEditPublisher)}>
                {" "}
                <Icon type="check" style={{ fontSize: "20px" }} />
              </a>
            </Form.Item>
          ) : (
            <div>
              <Text style={{ color: "white" }}>PUBLISHER: {publisher}</Text>
              <a
                style={{ display: "inline-block", fontSize: "20px" }}
                onClick={() => setEditPublisher(!editPublisher)}
              >
                <Icon type="edit" />
              </a>
            </div>
          )}
          <br />
          <br />
          {genres
            ? genres.map(value => <Tag color="orange">{value.description}</Tag>)
            : ""}
        </Col>
      </Row>
    </Content>
  );
};

export default DetailsSection;
