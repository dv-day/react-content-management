import React, { useState } from "react";
import { Layout, Row, Col, Typography, Tabs, Icon , Form , Input } from "antd";

const SystemRequireSection = ({ pc, linux, mac ,setPc,setLinux,setMac}) => {
  const { Content } = Layout;
  const { Title, Text } = Typography;
  const { TabPane } = Tabs;
  const [editMode, setEditMode] = useState(false);

const { TextArea} = Input
  return (
    <Content style={{ padding: "20px 50px" }}>
      
      { editMode ? <div>
        <Row>
          <Col span={12}>
          <Form.Item>
          <h3>PC : </h3>
              <TextArea
                rows={10}
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "100%"
                }}
                type="text"
                placeholder="pc requirement"
                value={pc}
                onChange={e => {
                  setPc(e.target.value);
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
          <h3>Linux : </h3>
            <Form.Item>
              <TextArea
                rows={10}
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "100%"
                }}
                type="text"
                placeholder="linux requirement"
                value={linux}
                onChange={e => {
                  setLinux(e.target.value);
                }}
              />
              
            </Form.Item>
          </Col>
        </Row>
            <Row>
              <Col span={12}>
              <h3> Mac : </h3>
            <Form.Item>
              <TextArea
                rows={10}
                style={{
                  backgroundColor: "black",
                  color: "white",
                  width: "100%"
                }}
                type="text"
                placeholder="mac requirement"
                value={mac}
                onChange={e => {
                  setMac(e.target.value);
                }}
              />
              
            </Form.Item>
              </Col>
            </Row>
            
            <a onClick={() => setEditMode(!editMode)}>
                {" "}
                <Icon type="check" style={{ fontSize: "20px" }} />
              </a>
              
              </div>: <Row>
              <a
                style={{ display: "inline-block", fontSize: "20px" }}
                onClick={() => setEditMode(!editMode)}
              >
                <Icon type="edit" />
              </a>
        <Col span={12}>
          <Title style={{ color: "white" }}>
            <b>
              SYSTEM <p style={{ color: "orange" }}>REQUIREMENTS</p>
            </b>
          </Title>
          <Tabs defaultActiveKey="1">
            <TabPane
              style={{ color: "white" }}
              tab={
                <span style={{ color: "white" }}>
                  <Icon type="windows" />
                  Windows
                </span>
              }
              key="1"
            >
              <div
                dangerouslySetInnerHTML={{
                  __html: pc
                }}
                style={{ color: "white" }}
              ></div>
            </TabPane>
            <TabPane
              style={{ color: "white" }}
              tab={
                <span style={{ color: "white" }}>
                  <Icon type="android" />
                  Mac OS X
                </span>
              }
              key="2"
            >
              <div
                dangerouslySetInnerHTML={{
                  __html: mac
                }}
                style={{ color: "white" }}
              ></div>
            </TabPane>
            <TabPane
              style={{ color: "white" }}
              tab={
                <span style={{ color: "white" }}>
                  <Icon type="android" />
                  SteamOS + Linux
                </span>
              }
              key="3"
            >
              <div
                dangerouslySetInnerHTML={{
                  __html: linux
                }}
                style={{ color: "white" }}
              ></div>
            </TabPane>
          </Tabs>
        </Col>
        
      </Row>}
      
    </Content>
  );
};

export default SystemRequireSection;
