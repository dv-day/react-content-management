import React from "react"
import { Layout, Row, Col, Typography, Tabs, Comment, Tooltip, List, Avatar, Form, Button, Input } from "antd"
import CommentSection from "./CommentSection"

const ReviewsSection = ({comments,id}) => {
    const { Content } = Layout
    const { Title } = Typography

    return (
        <Content style={{ padding: '20px 50px' }}>
            <Row>
                <Col span={12}>
                    <Title>
                        <b style={{ color: 'white' }}>REVIEWS</b>
                    </Title>
                    <CommentSection comments={comments} id={id}/>
                </Col>
            </Row>
        </Content>
    )
}

export default ReviewsSection