import React from "react"
import { Layout, PageHeader, List } from "antd"

const PageHeaderSection = ({ name }) => {
    const { Content } = Layout

    return (
        <Content>
            <PageHeader
                title={name}
            />
        </Content>
    )
}

export default PageHeaderSection