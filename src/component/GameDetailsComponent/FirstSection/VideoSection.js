import React from "react"
import { Layout } from "antd"
import ReactPlayer from 'react-player'

const VideoSection = ({ movie }) => {
    const { Content } = Layout

    return (
        <Content style={{ paddingBottom: '100px' }}>
            <ReactPlayer
                url={movie}
                playing
                controls={true}
                muted
            />
        </Content>
    )
}
export default VideoSection