import React from "react"
import { Layout, Row, Col, Carousel, Typography, Tag } from "antd"
import Slider from "react-slick"

const DetailsSection = ({
    detail,
    develop,
    release,
    publisher,
    genres,
    images,
    price
}) => {
    const { Content } = Layout
    const { Text } = Typography

    return (
        <Content style={{ padding: '20px 50px' }}>
            <Row>
                <Col span={11} >
                    <Slider autoplay dots={true}>
                        {images
                            ? images.map(value => (
                                <div>
                                    <img src={value.path_full} />
                                </div>
                            ))
                            : ""}
                    </Slider>
                </Col>
                <Col span={11} offset={1}>
                    <Text style={{ color: 'white' }}>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: detail
                            }}
                            style={{ color: "white" }}
                        ></div>
                    </Text>
                    <br /><br />
                    <Text style={{ color: 'white' }}>
                        <b>RELEASE DATE: </b>{release}
                    </Text>
                    <br /><br />
                    <Text style={{ color: 'white' }}>
                        <b>DEVELOPER: </b>{develop}
                    </Text>
                    <br /><br />
                    <Text style={{ color: 'white' }}>
                        <b>PUBLISHER: </b>{publisher}
                    </Text>
                    <br /><br />
                    <Text style={{ color: 'white' }}>
                        <b>PRICE: </b>{price}
                    </Text>
                    <br /><br />
                    {genres
                        ? genres.map(value => <Tag color="orange">{value.description}</Tag>)
                        : ""}
                </Col>
            </Row>
        </Content>
    )
}

export default DetailsSection