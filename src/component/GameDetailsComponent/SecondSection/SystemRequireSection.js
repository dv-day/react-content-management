import React from "react"
import { Layout, Row, Col, Typography, Tabs, Icon } from "antd"

const SystemRequireSection = ({ pc, linux, mac }) => {
    const { Content } = Layout
    const { Title } = Typography
    const { TabPane } = Tabs

    return (
        <Content style={{ padding: '20px 50px' }}>
            <Row>
                <Col span={12}>
                    <Title style={{ color: 'white' }}>
                        <b>SYSTEM <p style={{ color: 'orange' }}>REQUIREMENTS</p></b>
                    </Title>
                    <Tabs defaultActiveKey="1">
                        <TabPane style={{ color: 'white' }}
                            tab={
                                <span style={{ color: 'white' }}>
                                    <Icon type="windows" />
                                    Windows
                                </span>
                            }
                            key="1">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: pc
                                }}
                                style={{ color: "white" }}
                            ></div>
                        </TabPane>
                        <TabPane style={{ color: 'white' }}
                            tab={
                                <span style={{ color: 'white' }}>
                                    <Icon type="android" />
                                    Mac OS X
                                </span>
                            }
                            key="2">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: mac
                                }}
                                style={{ color: "white" }}
                            ></div>
                        </TabPane>
                        <TabPane style={{ color: 'white' }}
                            tab={
                                <span style={{ color: 'white' }}>
                                    <Icon type="android" />
                                    SteamOS + Linux
                                </span>
                            }
                            key="3">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: linux
                                }}
                                style={{ color: "white" }}
                            ></div>
                        </TabPane>
                    </Tabs>
                </Col>
                <Col span={10} offset={2}>
                    <img src='https://www.lenovo.com/medias/lenovo-laptop-legion-y7000-hero.png?context=bWFzdGVyfHJvb3R8MTAwMjQ2fGltYWdlL3BuZ3xoYzQvaDg5LzEwMDAyMjcyNjE2NDc4LnBuZ3xlMTEwMWY5Zjk0OTljZGZhMjc1ZDVhZDk4MjEwYzBlOTg2MDk1YjM2MjFmNGQ1ZjBiZjkzMTZlMzEyYzg2ZTNh'
                        style={{ width: '80%',marginTop:'100px' }} />
                </Col>
            </Row>
        </Content>
    )
}

export default SystemRequireSection