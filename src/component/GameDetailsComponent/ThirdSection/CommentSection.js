import React from "react"
import { Comment, Avatar, Form, Button, List, Input, Layout, Tooltip } from 'antd'
import moment from 'moment'

const { TextArea } = Input
const { Content } = Layout

const CommentList = ({ comments }) => (
    <List
        dataSource={comments}
        itemLayout="horizontal"
        renderItem={props => <Comment {...props} />}
        style={{ color: 'white' }}
    />
)

const Editor = ({ onChange, onSubmit, submitting, value }) => (
    <Content>
        <Form.Item>
            <TextArea onChange={onChange} value={value} />
        </Form.Item>
        <Form.Item>
            <Button htmlType="submit" loading={submitting} onClick={onSubmit} ghost>
                Post
            </Button>
        </Form.Item>
    </Content>
)

const data = [
    {
        author: <p style={{ color: 'white' }}>Han Solo</p>,
        avatar: 'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png',
        content: (
            <p style={{ color: 'white' }}>
                Amazing game forever!!!
        </p>
        ),
        datetime: (
            <Tooltip
                title={moment()
                    .subtract(1, 'days')
                    .format('YYYY-MM-DD HH:mm:ss')}
            >
                <span>
                    {moment()
                        .subtract(1, 'days')
                        .fromNow()}
                </span>
            </Tooltip>
        ),
    },
    {
        author: <p style={{ color: 'white' }}>Joseph</p>,
        avatar: <Avatar
            style={{ backgroundColor: 'green' }}
            icon="user"
            alt="Christ"
        />,
        content: (
            <p style={{ color: 'white' }}>
                I love this game (0W0)
        </p>
        ),
        datetime: (
            <Tooltip
                title={moment()
                    .subtract(2, 'days')
                    .format('YYYY-MM-DD HH:mm:ss')}
            >
                <span>
                    {moment()
                        .subtract(2, 'days')
                        .fromNow()}
                </span>
            </Tooltip>
        ),
    },
];

class CommentSection extends React.Component {
    state = {
        comments: [],
        submitting: false,
        value: ''
    }

    handleSubmit = () => {
        if (!this.state.value) {
            return
        }

        this.setState({
            submitting: true,
        })

        setTimeout(() => {
            this.setState({
                submitting: false,
                value: '',
                comments: [
                    {
                        author: <p style={{ color: 'white' }}>John Doe</p>,
                        avatar: <Avatar style={{ backgroundColor: 'orange' }} icon="user" />,
                        content: <p style={{ color: 'white' }}>{this.state.value}</p>,
                        datetime: moment().fromNow(),
                    },
                    ...this.state.comments,
                ],
            })
        }, 1000)
    }

    handleChange = e => {
        this.setState({
            value: e.target.value,
        })
    }

    render() {
        const { comments, submitting, value } = this.state

        return (
            <Content style={{ backgroundColor: '#1A1A1A', borderRadius: '5px', padding: '0px 50px' }}>
                <Comment
                    avatar={
                        <Avatar
                            style={{ backgroundColor: 'orange' }}
                            icon="user"
                            alt="John Doe"
                        />
                    }
                    content={
                        <Editor
                            onChange={this.handleChange}
                            onSubmit={this.handleSubmit}
                            submitting={submitting}
                            value={value}
                        />
                    }
                />
                {comments.length > 0 && <CommentList comments={comments} />}
                <List
                    className="comment-list"
                    itemLayout="horizontal"
                    dataSource={data}
                    renderItem={item => (
                        <li>
                            <Comment
                                actions={item.actions}
                                author={item.author}
                                avatar={item.avatar}
                                content={item.content}
                                datetime={item.datetime}
                            >
                            </Comment>
                        </li>
                    )}
                />
            </Content>
        )
    }
}

export default CommentSection