import React from "react"
import { Layout, Row, Col, Typography, Icon, Card, Avatar } from "antd"
import CommentSection from "./CommentSection"

const ReviewsSection = ({ metacriticScore, metacriticUrl }) => {
    const { Content } = Layout
    const { Title } = Typography
    const { Meta } = Card;

    return (
        <Content style={{ padding: '20px 50px' }}>
            <Row>
                <Col span={12}>
                    <Title>
                        <b style={{ color: 'white' }}>REVIEWS</b>
                    </Title>
                    <CommentSection />
                </Col>
                <Col span={10} offset={2}>
                    <Card
                        style={{ width: '240px', marginTop:'70px' }}
                        cover={
                            <div style={{
                                backgroundColor: '#FCBB62',
                                height: '160px',
                                textAlign: 'center',
                                fontSize: '50px'
                            }}>
                                <b>{metacriticScore}</b></div>
                        }
                    >
                        <Meta
                            avatar={<Avatar src="https://upload.wikimedia.org/wikipedia/commons/f/f2/Metacritic_M.png" />}
                            title="metacritic"
                            description={<a href={metacriticUrl} style={{color:'#888888'}}>Read Critic Reviews<Icon type="export" /></a>}
                        />
                    </Card>
                </Col>
            </Row>
        </Content>
    )
}

export default ReviewsSection