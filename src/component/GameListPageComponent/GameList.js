import React, { useEffect, useState } from "react";
import "../../App.css";
import { List, Card, Col, Row, Pagination, Select, Spin } from "antd";
import { useSelector, connect, useDispatch } from "react-redux";
import { fetchData, searchGameList, filter } from "../../action/GameListAction";
import { bindActionCreators } from "redux";
import GameListTool from "./GameListTool";
import { useHistory } from "react-router-dom";

const { Meta } = Card;
const { Option } = Select;

const GameList = props => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { gameList, gameSearch, filterList, loading } = props;

  const onFilter = () => {
    if (filterList == "All") {
      return sortArray();
    } else {
      return sortArray().filter((game, index) => {
        // console.log(game.genres.description)
        let isInclude = false;

        game.genres.forEach(genre => {
          if (genre.id.toString() == filterList) {
            isInclude = true;
          }
        });
        return isInclude;
      });
    }
  };
  console.log(gameList);

  const [sorting, setSorting] = useState("none");

  const handleChange = value => {
    setSorting(value);
  };

  const sortArray = () => {
    if (sorting == "none") {
      return gameList;
    } else if (sorting == "byName") {
      return gameList.sort(function(a, b) {
        var nameA = a.name.toLowerCase(),
          nameB = b.name.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });
    } else if (sorting == "byPrice") {
      return gameList.sort(function(a, b) {
        if (a.price < b.price) return -1;
        if (a.price > b.price) return 1;
        return 0;
      });
    }
  };

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  const getDataToShow = () => {
    // filter
    // sort
    let dataToShow = onFilter();

    // search
    if (gameSearch != "") {
      dataToShow = dataToShow.filter(data =>
        data.name.toLowerCase().match(gameSearch.toLowerCase())
      );
    }
    // return gameSearch == '' ?
    //     onFilter()
    //     :
    //     onFilter().filter(data => data.name.toLowerCase().match(gameSearch.toLowerCase()))

    return dataToShow;
  };

  return (
    <div>
      <h1
        style={{
          width: "450px",
          color: "orange",
          backgroundImage: "linear-gradient(-90deg,#262626,black)",
          padding: "16px 16px"
        }}
      >
        All games
      </h1>
      
      <Spin tip="Loading..." spinning={loading}>
      <Row style={{ marginBottom: -30 }}>
        <Col>
          <GameListTool />
        </Col>
      </Row>
        <h3 style={{ color: "white", display: "inline-block",marginBottom:"20px" }}>Sort by :</h3>
        <Select
          defaultValue="sort"
          style={{ width: 120, display: "inline-block", marginLeft: 10 }}
          onChange={handleChange}
        >
          <Option value="byName">Name</Option>
          <Option value="byPrice">Price</Option>
        </Select>
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: page => {
              console.log(page);
            },
            pageSize: 8,
            alightment: "bottom right"
          }}
          style={{ marginTop: "-16px" }}
          dataSource={getDataToShow()}
          renderItem={item => (
            <Col span={6}>
              <Card
                style={{ width: 270, backgroundColor: "rgba(0, 0, 0, 0.8)",cursor:"pointer",boxShadow: `3px 3px 3px #000`,border: "unset" }}
                cover={<img style={{ height: 360 }} src={item.img} />}
                onClick={() => history.push("/gamelist/" + item.steam_appid)}
              >
                <Meta
                  title={item.name}
                  description={
                    item.price > 0
                      ? item.price
                          .toFixed(2)
                          .replace(/\d(?=(\d{3})+\.)/g, "$&,") + " Baht"
                      : "Free"
                  }
                />
              </Card>
            </Col>
          )}
        />
      </Spin>
    </div>
  );
};

const mapStateToprops = state => {
  return {
    gameList: state.listGame.data,
    gameSearch: state.listGame.searchData,
    filterList: state.listGame.filterType,
    loading: state.listGame.loading
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData, searchGameList, filter }, dispatch);
};
export default connect(mapStateToprops, mapDispatchToProps)(GameList);

// const gameList = useSelector((state) => state.listGame.data);
// const gameSearch = useSelector(state => state.listGame.searchData);
// const filterList = useSelector(state => state.listGame.filterType);

// const OnSearch = () => {
//     if (gameSearch == '') {
//         OnFilter().map((item) => {
//             return (
//                 <Col span={6}>
//                     <Card style={{ width: 270, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
//                         cover={<img style={{ height: 360 }} src={item.img} />}>
//                         <Meta
//                             title={item.name}
//                             description={item.price} />
//                     </Card>
//                 </Col>
//             )

//         })
//     } else {
//         OnFilter().filter(data => data.title.match(gameSearch)).map((item) => {
//             return (
//                 <Col span={6}>
//                     <Card style={{ width: 270, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
//                         cover={<img style={{ height: 360 }} src={item.img} />}>
//                         <Meta
//                             title={item.name}
//                             description={item.price} />
//                     </Card>
//                 </Col>
//             )
//         }
//         )
//     }
// }

//     <List>
//     <center>
//         <div style={{ padding: '30px' }}>
//             <Row gutter={12, 16}>
//                 {
//                     gameSearch == '' ?
//                         OnFilter().map((item) => {
//                             return (
//                                 <Col span={6}>
//                                     <Card style={{ width: 270, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
//                                         cover={<img style={{ height: 360 }} src={item.img} />}>
//                                         <Meta
//                                             title={item.name}
//                                             description={item.price} />
//                                     </Card>
//                                 </Col>

//                             )
//                         })
//                         :
//                         OnFilter().filter(data => data.name.toLowerCase().match(gameSearch.toLowerCase())).map((item) => {
//                             return (
//                                 <Col span={6}>
//                                     <Card style={{ width: 270, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
//                                         cover={<img style={{ height: 360 }} src={item.img} />}>
//                                         <Meta
//                                             title={item.name}
//                                             description={item.price} />
//                                     </Card>
//                                 </Col>
//                             )
//                         })
//                 }
//             </Row>
//         </div>
//         <Paginations
//             postPages={postPerPage}
//             totalPosts={gameList.length}
//             paginate={paginates}
//         />
//     </center>

// </List>

// const indexOfLastPost = currentPage * postPerPage;
// const indexOfFirstPost = indexOfLastPost - postPerPage
// const currentPosts = gameList.slice(indexOfFirstPost, indexOfLastPost);

// const paginates = (pageNumber) => setCurrentPage(pageNumber)

// const [currentPage, setCurrentPage] = useState(1);
// const [postPerPage] = useState(8);
