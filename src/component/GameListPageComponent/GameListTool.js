import React, { useState, useEffect } from 'react'
import '../../App.css'
import { Input, Menu, Dropdown, Icon, Col, Row, Pagination, Select } from 'antd';
import { useDispatch } from 'react-redux';
import { searchGameList, filter } from '../../action/GameListAction';
const { Search } = Input;
const { Option, OptGroup } = Select;

const GameListTool = () => {

    const dispatch = useDispatch()

    const onSeachList = (evnet) => {
        let textValue = evnet.target.value
        dispatch(searchGameList(textValue))

    }

    const onFilter = (value) => {
        let textFilter = value
        dispatch(filter(textFilter))
    }

    return (
        <div>
            <Row style={{ marginTop: '-36px' }}> 
                <Col style={{ float: 'right' }}>
                    <div>
                        <Search
                            placeholder="Game Title..."
                            onChange={onSeachList}
                            style={{ width: 200, background: '#565656' }}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col style={{ float: 'right' }}>
                    <div>
                        <hr></hr>
                        <Select defaultValue={"All"} style={{ width: 200 }} onChange={(value) => onFilter(value)} >
                            <OptGroup label="Categories" >
                                <Option value="All">All</Option>
                                <Option value="1">Action</Option>
                                <Option value="2">Strategy</Option>
                                <Option value="3">RPG</Option>
                                <Option value="4">Casual</Option>
                                <Option value="18">Sports</Option>
                                <Option value="23">Indie</Option>
                                <Option value="25">Adventure</Option>
                                <Option value="28">Simulation</Option>
                                <Option value="29">Massively Multiplayer</Option>
                            </OptGroup>
                        </Select>
                    </div>
                </Col>

            </Row>
        </div >
    )
}
export default GameListTool