import React from 'react'
import { Pagination } from 'antd';

const Paginations = ({ postPages, totalPosts, paginate }) => {

    const pageNumber = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postPages); i++) {
        pageNumber.push(i);
    }

    return (
        <div>
            {pageNumber.map(number => (
                <button>
                    <a onClick={() => paginate(number)} className="page-link">{number}</a>
                    </button>
            ))}
        </div>
    )
}

export default Paginations

//  <ul className="pagiation">
// <li key={number} className="page-item">
//                         <a onClick={()=> paginate(number)}  className="page-link">
//                            <button>{number}</button> 
//                         </a>
//                     </li>
// </ul>