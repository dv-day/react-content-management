import React, { useEffect, useState } from "react";
import { Carousel, Row, Col, Button } from "antd";
import "../App.css";
import { connect } from "react-redux";
import ReactPlayer from "react-player";
import { bindActionCreators } from "redux";
import HomeGameStreamingPage from "../component/Homepage/HomeGameStreamingPage";
import HomeSpecialOffersPage from "../component/Homepage/HomeSpecialOffersPage";
import HomePopularPage from "../component/Homepage/HomePopularPage";
import { fetchData } from "../ActionsHome/ActionsHome";
import { useHistory } from "react-router-dom";

const Homepage = props => {
  const { fetchData, dataALL } = props;
  let history = useHistory();

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="backgroundPage">
      <Carousel
        autoplay
        style={{
          background: "#000",
          margin: "30px auto",
          width: "70%",
          height: "590px",
          overflow: "hidden",
          lineHeight: "620px",
          boxShadow: `5px 5px 5px #000`
        }}
      >
        {dataALL.slice(0, 6).map(data => {
          return (
            <div onClick={() => history.push("/gamelist/" + data.steam_appid)}>
              <img src={data.header_image} />
            </div>
          );
        })}
      </Carousel>

      <div style={{ backgroundColor: "#363636", height: "500px" }}>
        <Row>
          <Col span={12}>
            <h1
              style={{
                width: "350px",
                marginTop: "32px",
                marginLeft: "32px",
                padding: "10px 20px",
                color: "orange",
                backgroundImage: "linear-gradient(-90deg,#363636,black)"
              }}
            >
              Popular
            </h1>
          </Col>
          <Col span={12}>
            <Button
              onClick={() => history.push("/gamelist/")}
              style={{
                marginTop: 60,
                marginLeft: "70%",
                color: "orange",
                border: "1px solid orange"
              }}
              type="ghost"
            >
              See more>>
            </Button>
          </Col>
        </Row>
        <HomePopularPage />
      </div>

      <h1
        style={{
          width: "650px",
          padding: "10px 10px",
          marginTop: "64px",
          marginLeft: "10%",
          color: "orange",
          backgroundImage: "linear-gradient(-90deg,#262626,black)"
        }}
      >
        THE COMMUNITY RECOMMENDS
      </h1>
      <div className="thecommunityrecomment">
        <Carousel
          style={{
            marginTop: 16,
            marginBottom: 46,

            background: "#262626",
            height: "400px",
            width: "600px",
            boxShadow: `3px 3px 1px 1px #000`,
            padding: "10px",
            backgroundColor: "#000"
          }}
        >
          <div>
            <Row gutter={[8, 16]}>
              {dataALL.slice(0, 1).map(data => {
                return (
                  <Col
                    xs={{ span: 30 }}
                    
                  >
                    <a>
                      <img
                        src={data.header_image}
                        width="600px"
                        height="360px"
                      />
                      <ReactPlayer
                        className="klk"
                        url={data.movies[1].webm.max}
                        playing
                        muted
                      />
                    </a>
                  </Col>
                );
              })}
            </Row>
          </div>
          <div>
            <Row gutter={[8, 16]}>
              {dataALL.slice(1, 2).map(data => {
                return (
                  <Col xs={{ span: 30 }}>
                    <a>
                      <img
                        src={data.header_image}
                        width="540px"
                        height="360px"
                      />
                      <ReactPlayer
                        className="klk"
                        url={data.movies[1].webm.max}
                        playing
                        muted
                      />
                    </a>
                  </Col>
                );
              })}
            </Row>
          </div>
          <div>
            <Row gutter={[8, 16]}>
              {dataALL.slice(2, 3).map(data => {
                return (
                  <Col xs={{ span: 30 }}>
                    <a>
                      <img
                        src={data.header_image}
                        wwidth="600px"
                        height="360px"
                      />
                      <ReactPlayer
                        className="klk"
                        url={data.movies[1].webm.max}
                        playing
                        muted
                      />
                    </a>
                  </Col>
                );
              })}
            </Row>
          </div>
        </Carousel>
      </div>
      <Row className="gamedownload">
        <Col>
          <h1
            style={{
              // marginTop: "32px",
              color: "orange",
              textAlign: "center",
              backgroundImage: "linear-gradient(360deg,#363636,black)",
              paddingTop: "32px",
              height: "128px"
            }}
          >
            PLATFORM
          </h1>
          <Row gutter={[8, 16]}>
            <Col
              xs={{ span: 5, offset: 5 }}
              offset={5}
              style={{ width: "30%", height: "300px" }}
            >
              <div
                class="grid"
                align="middle"
                onClick={() => history.push("/gamelist")}
              >
                <figure class="effect-honey">
                  <img src="https://logodix.com/logo/19234.png" />
                  <figcaption>
                    <h2>
                      <span>mac OS x Platform</span>
                    </h2>
                    <a href="#"></a>
                  </figcaption>
                </figure>
              </div>
            </Col>
            <Col
              xs={{ span: 5, offset: 4 }}
              style={{ width: "30%", height: "300px" }}
            >
              <div class="grid" onClick={() => history.push("/gamelist")}>
                <figure class="effect-honey">
                  <img src="https://www.cloudhm.co.th/static/img/f_win_linux.png" />
                  <figcaption>
                    <h2>
                      <span>WINDOWS & LINUX</span>
                    </h2>
                    <a href="#"></a>
                  </figcaption>
                </figure>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <HomeSpecialOffersPage />
      <HomeGameStreamingPage />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    dataALL: state.dataHomePage.dataHome
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
