import React from "react";
import { Row, Col, BackTop, Popover } from "antd";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";

const HomeGameStreamingPage = props => {
  const { dataALL } = props;
  let history = useHistory();

  return (
    <div id="GamesStreaming">
      <h1
        style={{
          margin: "32px 32px",
          width: "450px",
          color: "orange",
          backgroundImage: "linear-gradient(-90deg,#262626,black)",
          padding: "16px 16px"
        }}
      >
        GAMES STREAMING
      </h1>
      <Row gutter={[24, 24]} style={{ marginTop: 20, background: "#262626" }}>
        {dataALL.slice(0, 8).map(data => {
          return (
            <Col span={8}>
              <Popover
                placement="leftTop"
                content={
                  <div>
                    <p>Developers : {data.developers}</p>
                    <p>Publishers : {data.publishers}</p>
                  </div>
                }
                title={data.name}
                trigger="hover"
                onClick={() => history.push("gamelist/" + data.steam_appid)}
              >
                <ReactPlayer
                  width="100%"
                  height="100%"
                  url={data.movies[1].webm.max}
                  href={data.movies[1].webm.max}
                  playing
                  muted
                  style={{
                    boxShadow: `10px -10px 8px 1px #161616`
                  }}
                />
              </Popover>
            </Col>
          );
        })}
      </Row>
      <BackTop />
      <strong style={{ color: "rgba(64, 64, 64, 0.6)" }}> </strong>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    dataALL: state.dataHomePage.dataHome
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeGameStreamingPage);
