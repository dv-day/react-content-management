import React, { useState, useEffect } from "react";
import { Carousel, Row, Col, Badge, Progress } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";

const HomePopularPage = props => {
  const { dataALL } = props;
  const history = useHistory();

  var renderDataAll = dataALL;

  // setTimeout(() => {
  //   renderDataAll.sort((a, b) => (a.metacritic.score < b.metacritic.score) ? 1 : -1) }
  //   , 3000);
  

  console.log(renderDataAll);

  return (
    <div id="Popular">
      <Carousel
        // autoplay
        style={{
          marginTop: 16,
          backgroundColor: "#363636",
          height: "400px",
          lineHeight: "400px"
        }}
      >
        <div>
          <Row gutter={[24, 8]}>
            {renderDataAll.slice(0, 4).map(data => {
              return (
                <Col
                  xs={{ span: 4, offset: 2 }}
                  style={{ height: 360, width: 260 }}
                >
                  <div
                    onClick={() =>
                      history.push("/gamelist/" + data.steam_appid)
                    }
                    style={{ position: "absolute", zIndex: 9999 }}
                  >
                    <Badge
                      count={"Score :" + data.metacritic.score}
                      style={{
                        top: -166,
                        marginLeft: 185,
                        backgroundColor: "orange",
                        color: "#000",
                        fontWeight: "bold",
                        boxShadow: `3px 3px 3px #000`
                      }}
                    />
                  </div>
                  <img
                    style={{
                      position: "absolute",
                      marginTop: 0,
                      boxShadow: `5px 5px 5px #000`
                    }}
                    height="100%"
                    width="100%"
                    src={data.img}
                  />
                </Col>
              );
            })}
          </Row>
        </div>
        <div>
          <Row gutter={[24, 8]}>
            {renderDataAll.slice(4, 8).map(data => {
              return (
                <Col
                  xs={{ span: 4, offset: 2 }}
                  style={{ height: 360, width: 260 }}
                >
                  <div
                    onClick={() =>
                      history.push("/gamelist/" + data.steam_appid)
                    }
                    style={{ position: "absolute", zIndex: 9999 }}
                  >
                    <Badge
                      count={"Score :" + data.metacritic.score}
                      style={{
                        top: -166,
                        marginLeft: 185,
                        backgroundColor: "orange",
                        color: "#000",
                        fontWeight: "bold",
                        boxShadow: `3px 3px 3px #000`
                      }}
                    />
                  </div>
                  <img
                    style={{
                      position: "absolute",
                      marginTop: 0,
                      boxShadow: `5px 5px 5px #000`
                    }}
                    height="100%"
                    width="100%"
                    src={data.img}
                  />
                </Col>
              );
            })}
          </Row>
        </div>
      </Carousel>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    dataALL: state.dataHomePage.dataHome
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(HomePopularPage);
