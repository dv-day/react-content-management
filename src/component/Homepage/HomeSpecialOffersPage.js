import React from "react";
import { Carousel, Row, Col, Popover } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useHistory } from "react-router-dom";

const HomeSpecialOffersPage = props => {
  const { dataALL } = props;
  let history = useHistory();

  return (
    <div className="SpecialOffers">
      <h1
        style={{
          margin: "32px 32px",
          width: "450px",
          color: "orange",
          backgroundImage: "linear-gradient(-90deg,#262626,black)",
          padding: "16px 16px"
        }}
      >
        SPECIAL OFFERS
      </h1>

      {/* <Carousel
        style={{ margin: "15px auto", background: "#262626", height: "350px" }}
      > */}
      <Row justify="space-around" gutter={[24, 12]}>
        {dataALL.slice(0, 4).map(data => {
          return (
            <Col xs={{ span: 4, offset: 1 }} style={{ height: 360, marginLeft: "7%" }} >
              <Popover
                placement="leftTop"
                content={
                  <div>
                    <p>Price : {data.price}</p>
                    <p>developers : {data.developers}</p>
                    <p>publishers : {data.publishers}</p>
                  </div>
                }
                title={data.name}
                trigger="hover"
              >
                <img
                  height="100%"
                  width="100%"
                  src={data.img}
                  style={{boxShadow: `3px 3px 3px 2px #000`}}
                  onClick={() => history.push("gamelist/" + data.steam_appid)}
                />
              </Popover>
            </Col>
          );
        })}
      </Row>
      {/* <Row gutter={[24, 8]}>
          {dataALL.slice(4, 8).map(data => {
            return (
              <Col xs={{ span: 4, offset: 2 }} style={{ height: 300 }}>
                <Popover
                  placement="leftTop"
                  content={
                    <div>
                      <p>Price : {data.price}</p>
                      <p>developers: {data.developers}</p>
                      <p>publishers:{data.publishers}</p>
                    </div>
                  }
                  title={data.name}
                  trigger="hover"
                >
                  <img
                    height="100%"
                    width="100%"
                    src={data.img}
                    onClick={() => history.push("gamelist/" + data.steam_appid)}
                  />
                </Popover>
              </Col>
            );
          })}
        </Row> */}
      {/* </Carousel> */}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    dataALL: state.dataHomePage.dataHome
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeSpecialOffersPage);
