import React, { useState, useEffect } from "react";
import { Layout, Menu, Icon } from "antd";
import MainRoute from "../MainRoute";
import { useHistory, Route, Redirect } from "react-router-dom";
import Homepage from "../component/Homepage";
import EditPage from "../Pages/EditPage";
import EditFormPage from "../Pages/EditFormPage";
import { Select } from "antd";
import { connect, useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchData } from "../reducer/GameReducer";
import LoginPage from "../Pages/LoginPage";
import { logout } from "../actions/AuthAction";
import PrivateRoute from "./PrivateRoute";
import { setKey, setSearchShow } from "../reducer/NavReducer";
import { loginProcess } from "../reducer/AuthReducer";
import GameListPage from "../Pages/GameListPage";
import GameDetailsPage from "../Pages/GameDetailsPage";

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;
const { Option } = Select;

const NavBar = props => {
  const { fetchData, loginProcess, logout } = props;
  const { username, password, isLogin } = props.login;
  const { data } = props.data;
  const { text, setText } = useState();
  const selKey = useSelector(state => state.toggle.key);
  // const sh = useSelector(state => state.toggle.show);
  const dispatch = useDispatch();
  const [toggle, setToggle] = useState(false);
  const [showSearch, setShowSearch] = useState(true);
  // const [selKey, setKey] = useState("1");

  let history = useHistory();

  const handleSelect = key => {
    // if (key == 1) {
    //   history.push("/");
    // } else if (key == 7) {
    //   history.push("/edit");
    // } else if (key == 9) {
    //   history.push("/login");
    // }else if(key == 10){
    //   history.push("/processLogout");
    // }
    switch (key) {
      case "1":
        history.push("/");
        return;
      case "7":
        history.push("/edit");
        return;
      case "6":
        history.push("/gamelist");
        return;
      case "9":
        history.push("/login");
        return;
      case "10":
        history.push("/processLogout");
        return;
      default:
        return;
    }
  };
  console.log(selKey);
  const url = window.location.pathname;

  if (url == "/login") {
    dispatch(setKey("9"));
  } else if (url == "/edit") {
    dispatch(setKey("7"));
  } else if (url == "/gamelist") {
    dispatch(setKey("6"));
  } else if (url == "/") {
    dispatch(setKey("1"));
  } else {
    dispatch(setKey("0"));
  }

  useEffect(() => {
    if( url == "/gamelist"){
      setShowSearch(false)
    }else{
      setShowSearch(true)
    }
    // fetchData();
  }, [url]);

  const renderOption = () => {
    if (data) {
      return data.map(value => {
        return <Option value={value.name} ><a onClick={() => history.push("/gamelist/"+ value.steam_appid)}>{value.name}</a></Option>;
      });
    }
  };

  const onToggle = () => {
    console.log(toggle + "changed");
    setToggle(!toggle);
  };

  const searchInput = () => {
    return (
      <div>
        <Select
          showSearch
          style={{ width: 200, textAlign: "center" }}
          prefix={<Icon type="search" />}
          placeholder="Search"
          optionFilterProp={data}
          filterOption={(input, option) =>
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
            0
          }
         
        >
          {renderOption()}
        </Select>
      </div>
    );
  };
  const searchIcon = () => {
    return (
      <div>
        <span>Search</span>
        <Icon type="search" style={{ marginLeft: 15 }} />
      </div>
    );
  };

  return (
    <Layout>
      <Header
        style={{
          position: "fixed",
          zIndex: 1,
          width: "100%",
          height: "128px",
          textAlign: "center",
          backgroundColor: "black"
        }}
      >
        <div className="logo" style={logoStyle}>
          <Icon type="crown" rotate={190} style={{ fontSize: "45px" }} />
        </div>

        <Menu
          mode="horizontal"
          // defaultSelectedKeys={["1"]}
          selectedKeys={[selKey]}
          onSelect={e => {
            handleSelect(e.key);
          }}
          style={{
            lineHeight: "64px",
            width: "100%",
            backgroundColor: "black",
            color: "orange"
          }}
        >
          {/* <Menu.Item key="logo" style={{ float: "left" }}>
                <Icon type="crown" spin />
              </Menu.Item> */}
          {isLogin ? (
            <Menu.Item key="10" style={{ float: "left" }}>
              <Icon type="logout" />
              <span>Log out</span>
            </Menu.Item>
          ) : (
            <Menu.Item key="9" style={{ float: "left" }}>
              <Icon type="login" />
              <span>Log in</span>
            </Menu.Item>
          )}

          <Menu.Item key="1"  style={showSearch ? ({marginLeft: "8%"}):({marginLeft: "-7.5%"}) }>
            <Icon type="desktop" /> <span>Home</span>
          </Menu.Item>
          <Menu.Item key="6">
            <Icon type="rocket" />
            <span>Games</span>
          </Menu.Item>
          <Menu.Item key="7">
            <Icon type="edit" />
            <span>Edit</span>
          </Menu.Item>

          {showSearch ? (
            <Menu.Item
              key="search"
              style={{ float: "right", width: 270 }}
              onMouseEnter={() => onToggle(true)}
              onMouseLeave={() => onToggle(false)}
            >
              {toggle ? searchInput() : searchIcon()}
            </Menu.Item>
          ) : (
            ""
          )}
        </Menu>
      </Header>
      <Content
        style={{ padding: "0 50px", marginTop: 128, backgroundColor: "black" }}
      >
        <div
          style={{
            padding: 24,
            minHeight: "90vh",
            backgroundColor: "#262626"
          }}
        >
          {/* <Route
            path="/processLogin"
            render={() => {
              loginProcess(username, password);
              return <Redirect to="/" />;
            }}
          ></Route> */}
          <Route exact path="/" component={Homepage} />
          <PrivateRoute exact path="/edit" component={EditPage} />
          <PrivateRoute path="/edit/:id" component={EditFormPage} />

          <Route exact path="/gamelist" component={GameListPage} />
          <Route path="/gamelist/:steam_appid" component={GameDetailsPage} />

          <Route path="/login" component={LoginPage} />
          <Route
            path="/processLogout"
            render={() => {
              logout();
              return <Redirect to="/" />;
            }}
          ></Route>
        </div>
      </Content>
      <Footer
        style={{ textAlign: "center", backgroundColor: "#000", color: "#fff" }}
      >
        CMS PROJECT
      </Footer>
    </Layout>
  );
};

const logoStyle = {
  color: "orange",
  //   width: "120px",
  height: "31px",
  //   background: "rgba(255, 255, 255, 0.2)",
  margin: "16px auto",
  float: "initial"
};
const mapStateToProps = state => {
  return {
    data: state.data,
    login: state.login
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchData, loginProcess, logout }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
