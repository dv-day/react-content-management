import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
const PrivateRoute = ({ path, component: Component, ...props }) => {
  const { isLogin } = props.login;

  console.log(props.login);
  return (
    <Route
      path={path}
      render={props => {
        if (isLogin == true) {
          return <Component {...props} />;
        } else {
          return <Redirect to="/login" />;
        }
      }}
      {...props}
    />
  );
};

const mapStateToProps = state => {
  return {
    login: state.login
  };
};
//   const mapDispatchToProps = dispatch => {
//     return bindActionCreators({ fetchData, SubmitAction }, dispatch);
//   };

export default connect(mapStateToProps, null)(PrivateRoute);
