export const data = [
    {
        "steam_appid": 570,
        "name": "Dota 2",
        "price": 0,
        "release_date":{
            "coming_soon":false,
            "date":"10 Jul, 2013"
        },
        "img": "https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg",
        "detailed_description": "<strong>The most-played game on Steam.</strong><br>Every day, millions of players worldwide enter battle as one of over a hundred Dota heroes. And no matter if it's their 10th hour of play or 1,000th, there's always something new to discover. With regular updates that ensure a constant evolution of gameplay, features, and heroes, Dota 2 has truly taken on a life of its own.<br><br><strong>One Battlefield. Infinite Possibilities.</strong><br>When it comes to diversity of heroes, abilities, and powerful items, Dota boasts an endless array—no two games are the same. Any hero can fill multiple roles, and there's an abundance of items to help meet the needs of each game. Dota doesn't provide limitations on how to play, it empowers you to express your own style.<br><br><strong>All heroes are free.</strong><br>Competitive balance is Dota's crown jewel, and to ensure everyone is playing on an even field, the core content of the game—like the vast pool of heroes—is available to all players. Fans can collect cosmetics for heroes and fun add-ons for the world they inhabit, but everything you need to play is already included before you join your first match.<br><br><strong>Bring your friends and party up.</strong><br>Dota is deep, and constantly evolving, but it's never too late to join. <br>Learn the ropes playing co-op vs. bots. Sharpen your skills in the hero demo mode. Jump into the behavior- and skill-based matchmaking system that ensures you'll <br>be matched with the right players each game.",
        "supported_languages": "Bulgarian, Czech, Danish, Dutch, English<strong>*</strong>, Finnish, French, German, Greek, Hungarian, Italian, Japanese, Korean<strong>*</strong>, Norwegian, Polish, Portuguese, Portuguese - Brazil, Romanian, Russian, Simplified Chinese<strong>*</strong>, Spanish - Spain, Swedish, Thai, Traditional Chinese, Turkish, Ukrainian<br><strong>*</strong>languages with full audio support",
        "header_image": "http://gadgetsngaming.com/wp-content/uploads/2015/08/Steam-Dota-2-Counter-Strike-Global-Offensive.jpg",
        "website": "http://www.dota2.com/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows 7 or newer<br></li><li><strong>Processor:</strong> Dual core from Intel or AMD at 2.8 GHz<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> nVidia GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 15 GB available space<br></li><li><strong>Sound Card:</strong> DirectX Compatible</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> OS X Mavericks 10.9 or newer<br></li><li><strong>Processor:</strong> Dual core from Intel<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> nVidia 320M or higher, or Radeon HD 2400 or higher, or Intel HD 3000 or higher<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 15 GB available space</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Ubuntu 12.04 or newer<br></li><li><strong>Processor:</strong> Dual core from Intel or AMD at 2.8 GHz<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> nVidia Geforce 8600/9600GT (Driver v331), AMD HD 2xxx-4xxx (Driver mesa 10.5.9), AMD HD 5xxx+ (Driver mesa 10.5.9 or Catalyst 15.7), Intel HD 3000 (Driver mesa 10.6)<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 15 GB available space<br></li><li><strong>Sound Card:</strong> OpenAL Compatible Sound Card</li></ul>"
        },
        "developers": [
            "Valve"
        ],
        "publishers": [
            "Valve"
        ],
        "metacritic": {
            "score": 90,
            "url": "https://www.metacritic.com/game/pc/dota-2?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "37",
                "description": "Free to Play"
            },
            {
                "id": "2",
                "description": "Strategy"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_86d675fdc73ba10462abb8f5ece7791c5047072c.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_86d675fdc73ba10462abb8f5ece7791c5047072c.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_ad8eee787704745ccdecdfde3a5cd2733704898d.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_ad8eee787704745ccdecdfde3a5cd2733704898d.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_7ab506679d42bfc0c0e40639887176494e0466d9.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_7ab506679d42bfc0c0e40639887176494e0466d9.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_c9118375a2400278590f29a3537769c986ef6e39.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_c9118375a2400278590f29a3537769c986ef6e39.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_f9ebafedaf2d5cfb80ef1f74baa18eb08cad6494.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_f9ebafedaf2d5cfb80ef1f74baa18eb08cad6494.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_27b6345f22243bd6b885cc64c5cda74e4bd9c3e8.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_27b6345f22243bd6b885cc64c5cda74e4bd9c3e8.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_b33a65678dc71cc98df4890e22a89601ee56a918.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_b33a65678dc71cc98df4890e22a89601ee56a918.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_d0f973ce376ca5b6c08e081cb035e86ced105fa9.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_d0f973ce376ca5b6c08e081cb035e86ced105fa9.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_1f3b5f5ccf8b159294914c3fe028128a787304b6.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_1f3b5f5ccf8b159294914c3fe028128a787304b6.1920x1080.jpg?t=1581406684"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_e0a92f15a6631a8186df79182d0fe28b5e37d8cb.600x338.jpg?t=1581406684",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/570/ss_e0a92f15a6631a8186df79182d0fe28b5e37d8cb.1920x1080.jpg?t=1581406684"
            }
        ],
        "movies": [
            {
                "id": 256692021,
                "name": "Dota 2 - Join the Battle",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256692021/movie.293x165.jpg?t=1554408539",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256692021/movie480.webm?t=1554408539",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256692021/movie_max.webm?t=1554408539"
                },
                "highlight": true
            },
            {
                "id": 256692017,
                "name": "Dota 2 - Sizzle Reel",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256692017/movie.293x165.jpg?t=1554408547",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256692017/movie480.webm?t=1554408547",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256692017/movie_max.webm?t=1554408547"
                },
                "highlight": true
            },
            {
                "id": 2028243,
                "name": "Dota 2 - The Greeviling",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028243/movie.293x165.jpg?t=1554408553",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028243/movie480.webm?t=1554408553",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028243/movie_max.webm?t=1554408553"
                },
                "highlight": false
            },
            {
                "id": 81026,
                "name": "Dota 2 Gamescom Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/81026/movie.293x165.jpg?t=1554408559",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/81026/movie480.webm?t=1554408559",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/81026/movie_max.webm?t=1554408559"
                },
                "highlight": false
            },
            {
                "id": 2040250,
                "name": "Dota 2 Reborn - Custom Games Are Here",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2040250/movie.293x165.jpg?t=1554408580",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2040250/movie480.webm?t=1554408580",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2040250/movie_max.webm?t=1554408580"
                },
                "highlight": false
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/570/page_bg_generated_v6b.jpg?t=1581406684"
    },
    {
        "steam_appid": 730,
        "name": "Counter-Strike: Global Offensive",
        "price": 0,
        "img": "https://images-na.ssl-images-amazon.com/images/I/81L8-mjNlrL._SX425_.jpg",
        "detailed_description": "Counter-Strike: Global Offensive (CS: GO) expands upon the team-based action gameplay that it pioneered when it was launched 19 years ago.<br />\r\n<br />\r\nCS: GO features new maps, characters, weapons, and game modes, and delivers updated versions of the classic CS content (de_dust2, etc.).<br />\r\n<br />\r\n&quot;Counter-Strike took the gaming industry by surprise when the unlikely MOD became the most played online PC action game in the world almost immediately after its release in August 1999,&quot; said Doug Lombardi at Valve. &quot;For the past 12 years, it has continued to be one of the most-played games in the world, headline competitive gaming tournaments and selling over 25 million units worldwide across the franchise. CS: GO promises to expand on CS' award-winning gameplay and deliver it to gamers on the PC as well as the next gen consoles and the Mac.&quot;",
        "supported_languages": "Czech, Danish, Dutch, English<strong>*</strong>, Finnish, French, German, Hungarian, Italian, Japanese, Korean, Norwegian, Polish, Portuguese, Portuguese - Brazil, Romanian, Russian, Simplified Chinese, Spanish - Spain, Swedish, Thai, Traditional Chinese, Turkish, Bulgarian, Ukrainian, Greek, Spanish - Latin America, Vietnamese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://www.ecopetit.cat/wpic/mpic/6-61907_133-cs-cs-go-wallpaper-hd.jpg",
        "website": "http://blog.counter-strike.net/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Windows® 7/Vista/XP<br></li><li><strong>Processor:</strong> Intel® Core™ 2 Duo E6600 or AMD Phenom™ X3 8750 processor or better<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> Video card must be 256 MB or more and should be a DirectX 9-compatible with support for Pixel Shader 3.0<br></li><li><strong>DirectX:</strong> Version 9.0c<br></li><li><strong>Storage:</strong> 15 GB available space</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> MacOS X 10.11 (El Capitan) or later<br></li><li><strong>Processor:</strong> Intel Core Duo Processor (2GHz or better)<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> ATI Radeon HD 2400 or better / NVidia 8600M or better<br></li><li><strong>Storage:</strong> 15 GB available space</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> Ubuntu 12.04<br></li><li><strong>Processor:</strong> 64-bit Dual core from Intel or AMD at 2.8 GHz<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> nVidia GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1<br></li><li><strong>Storage:</strong> 15 GB available space<br></li><li><strong>Sound Card:</strong> OpenAL Compatible Sound Card</li></ul>"
        },
        "developers": [
            "Valve",
            "Hidden Path Entertainment"
        ],
        "publishers": [
            "Valve"
        ],
        "metacritic": {
            "score": 83,
            "url": "https://www.metacritic.com/game/pc/counter-strike-global-offensive?ftag=MCD-06-10aaa1f"
        },
        "release_date": {
            "coming_soon": false,
            "date": "21 Aug, 2012"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "37",
                "description": "Free to Play"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_34090867f1a02b6c17652ba9043e3f622ed985a9.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_34090867f1a02b6c17652ba9043e3f622ed985a9.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_1d30c9a215fd621e2fd74f40d93b71587bf6409c.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_1d30c9a215fd621e2fd74f40d93b71587bf6409c.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_baa02e979cd3852e3c4182afcd603ab64e3502f9.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_baa02e979cd3852e3c4182afcd603ab64e3502f9.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_ffe584c163a2b16e9c1b733b1c8e2ba669fb1204.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_ffe584c163a2b16e9c1b733b1c8e2ba669fb1204.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_d87c102d028d545c877363166c9d8377014f0c23.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_d87c102d028d545c877363166c9d8377014f0c23.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9d0735a5fbe523fd39f2c69c047019843c326cea.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9d0735a5fbe523fd39f2c69c047019843c326cea.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9d889bec419cf38910ccf72dd80f9260227408ee.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9d889bec419cf38910ccf72dd80f9260227408ee.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_ccc4ce6edd4c454b6ce7b0757e633b63aa93921d.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_ccc4ce6edd4c454b6ce7b0757e633b63aa93921d.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9db552fd461722f1569e3292d8f2ea654c8ffdef.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_9db552fd461722f1569e3292d8f2ea654c8ffdef.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_74c1a0264ceaf57e5fb51d978205045223b48a18.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_74c1a0264ceaf57e5fb51d978205045223b48a18.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_7eaa83e44f5218a7bf5f88a0c750e36052e31d7d.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_7eaa83e44f5218a7bf5f88a0c750e36052e31d7d.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_68007896ad6071b7062bac530c481e097105efc0.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_68007896ad6071b7062bac530c481e097105efc0.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_2fcee01bace72bc47a2ad0ba82620588239e93df.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_2fcee01bace72bc47a2ad0ba82620588239e93df.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_f5875f8de419a3d5133ae7245b8296db2c027dd8.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_f5875f8de419a3d5133ae7245b8296db2c027dd8.1920x1080.jpg?t=1580765900"
            },
            {
                "id": 14,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_36f82c71ee2180159b060b155bf3d06dd8167327.600x338.jpg?t=1580765900",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/730/ss_36f82c71ee2180159b060b155bf3d06dd8167327.1920x1080.jpg?t=1580765900"
            }
        ],
        "movies": [
            {
                "id": 81958,
                "name": "CS:GO Trailer Long",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/81958/movie.293x165.jpg?t=1554409259",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/81958/movie480.webm?t=1554409259",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/81958/movie_max.webm?t=1554409259"
                },
                "highlight": true
            },
            {
                "id": 2028288,
                "name": "CS: GO Pro Tip Series: TM",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028288/movie.293x165.jpg?t=1554409287",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028288/movie480.webm?t=1554409287",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028288/movie_max.webm?t=1554409287"
                },
                "highlight": false
            },
            {
                "id": 2028287,
                "name": "CS: GO Pro Tip Series: sapphiRe",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028287/movie.293x165.jpg?t=1554409281",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028287/movie480.webm?t=1554409281",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028287/movie_max.webm?t=1554409281"
                },
                "highlight": false
            },
            {
                "id": 2028286,
                "name": "CS: GO Pro Tip Series: AZK",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028286/movie.293x165.jpg?t=1554409276",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028286/movie480.webm?t=1554409276",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028286/movie_max.webm?t=1554409276"
                },
                "highlight": false
            },
            {
                "id": 2028284,
                "name": "CS: GO Pro Tip Series: ruggah",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028284/movie.293x165.jpg?t=1554409270",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028284/movie480.webm?t=1554409270",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028284/movie_max.webm?t=1554409270"
                },
                "highlight": false
            },
            {
                "id": 2028283,
                "name": "CS: GO Pro Tip Series: nEiLZiNHo",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028283/movie.293x165.jpg?t=1554409264",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028283/movie480.webm?t=1554409264",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028283/movie_max.webm?t=1554409264"
                },
                "highlight": false
            },
            {
                "id": 2028289,
                "name": "CS: GO Pro Tip Series: Semphis",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028289/movie.jpg?t=1554409293",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028289/movie480.webm?t=1554409293",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028289/movie_max.webm?t=1554409293"
                },
                "highlight": false
            },
            {
                "id": 2028285,
                "name": "CS: GO Pro Tip Series: Fifflaren",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2028285/movie.293x165.jpg?t=1554409299",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2028285/movie480.webm?t=1554409299",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2028285/movie_max.webm?t=1554409299"
                },
                "highlight": false
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/730/page_bg_generated_v6b.jpg?t=1580765900"
    },
    {
        "steam_appid": 582010,
        "name": "MONSTER HUNTER: WORLD",
        "price": 739.00,
        "img": "https://file-cdn.gvgmall.com/product/20180720154708_gvgmall.jpg",
        "detailed_description": "<h1>Featured DLC</h1><p><a href=\"https://store.steampowered.com/app/1118010/Monster_Hunter_World_Iceborne/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/ICB_BNR_F.png?t=1581101000\" ></a></p><br><h1>Featured DLC</h1><p><a href=\"https://store.steampowered.com/bundle/12360/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/handlercostumepack.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/12358/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesturepack.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/12359/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/stickerpack.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/9788/Monster_Hunter_World__Additional_Sticker_Set_Bundle_3/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/sticker_bundle3.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/9787/Monster_Hunter_World__Additional_Gesture_Bundle_8/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesture_bundle8.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/9264/Monster_Hunter_World__Additional_Gesture_Bundle_7/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesture_bundle7.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7766/Monster_Hunter_World__Additional_Gesture_Bundle_6/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesture_bundle6.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7765/Monster_Hunter_World__Additional_Gesture_Bundle_5/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesture_bundle5.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7764/Monster_Hunter_World__Additional_Gesture_Bundle_4/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesutre_bundle4.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7763/Monster_Hunter_World__Additional_Gesture_Bundle_3/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/gesture_bundle3.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7768/Monster_Hunter_World__Additional_Sticker_Set_Bundle_2/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/sticker_bundle2.png?t=1581101000\" ></a><br><br><a href=\"https://store.steampowered.com/bundle/7769/Monster_Hunter_World__Additional_Face_Paint_Bundle/\" target=\"_blank\" rel=\"noreferrer\"  ><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/facepaint_bundle.png?t=1581101000\" ></a></p><br><h1>Digital Deluxe Edition</h1><p><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/DLC999-Special_Announcement.png?t=1581101000\" ><br>The digital deluxe edition includes the following content:<br><ul class=\"bb_ul\"><li>Samurai Set<br> Layered armor sets will change the look of your armor without changing the properties underneath. Equip this Samurai set over your favorite armor to take on the striking appearance of a feudal Japanese samurai warrior! <br> Note : No weapons are included with this set<br><br></li><li>Gesture: Zen<br></li><li>Gesture: Ninja Star<br></li><li>Gesture: Sumo Slap<br> Enjoy three new amusing gestures you can use when interacting with other players in the game.<br><br></li><li>Sticker Set: MH All-Stars Set<br></li><li>Sticker Set: Sir Loin Set<br> Fun stickers you can use when chatting with other players in the game.<br><br></li><li>Face Paint: Wyvern<br> Add a new face paint for character customisation in Monster Hunter: World.<br><br></li><li>Hairstyle: Topknot<br> Adds a new hairstyle for character customisation in Monster Hunter: World.</li></ul></p><br><h1>About the Game</h1>Welcome to a new world! Take on the role of a hunter and slay ferocious monsters in a living, breathing ecosystem where you can use the landscape and its diverse inhabitants to get the upper hand. Hunt alone or in co-op with up to three other players, and use materials collected from fallen foes to craft new gear and take on even bigger, badder beasts!<h2 class=\"bb_tag\"><strong>INTRODUCTION</strong></h2><strong>Overview</strong><br><strong>Battle gigantic monsters in epic locales.</strong><br><br>As a hunter, you'll take on quests to hunt monsters in a variety of habitats.<br>Take down these monsters and receive materials that you can use to create stronger weapons and armor in order to hunt even more dangerous monsters.<br><br>In Monster Hunter: World, the latest installment in the series, you can enjoy the ultimate hunting experience, using everything at your disposal to hunt monsters in a new world teeming with surprises and excitement.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_introduction.jpg?t=1581101000\" ><br><br><strong>Setting</strong><br>Once every decade, elder dragons trek across the sea to travel to the land known as the New World in a migration referred to as the Elder Crossing.<br><br>To get to the bottom of this mysterious phenomenon, the Guild has formed the Research Commission, dispatching them in large fleets to the New World.<br><br>As the Commission sends its Fifth Fleet in pursuit of the colossal elder dragon, Zorah Magdaros, one hunter is about to embark on a journey grander than anything they could have ever imagined.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_story.jpg?t=1581101000\" ><h2 class=\"bb_tag\"><strong>ECOSYSTEM</strong></h2><strong>A World That Breathes Life</strong><br>There are various locations teeming with wildlife. Expeditions into these locales are bound to turn up interesting discoveries.<h2 class=\"bb_tag\"><strong>HUNTING</strong></h2><strong>A Diverse Arsenal, and an Indispensable Partner</strong><br>Your equipment will give you the power to need to carve out a place for yourself in the New World.<br><br><strong>The Hunter's Arsenal</strong><br>There are fourteen different weapons at the hunter's disposal, each with its own unique characteristics and attacks. Many hunters acquire proficiency in multiple types, while others prefer to attain mastery of one.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_hunters.jpg?t=1581101000\" ><br><br><strong>Scoutflies</strong><br>Monster tracks, such as footprints and gashes, dot each locale. Your Scoutflies will remember the scent of a monster and guide you to other nearby tracks. And as you gather more tracks, the Scoutflies will give you even more information.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_shirubemushi_s.jpg?t=1581101000\" ><br><br><strong>Slinger</strong><br>The Slinger is an indispensable tool for a hunter, allowing you to arm yourself with stones and nuts that can be gathered from each locale.<br>From diversion tactics to creating shortcuts, the Slinger has a variety of uses, and allows you to hunt in new and interesting ways.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_slinger_s.jpg?t=1581101000\" ><br><br><strong>Specialized Tools</strong><br>Specialized tools activate powerful effects for a limited amount of time, and up to two can be equipped at a time. Simple to use, they can be selected and activated just like any other item you take out on a hunt.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_specialTool_s.jpg?t=1581101000\" ><br><br><strong>Palicoes</strong><br>Palicoes are hunters' reliable comrades out in the field, specialized in a variety of offensive, defensive, and restorative support abilities.<br>The hunter's Palico joins the Fifth Fleet with pride, as much a bona fide member of the Commission as any other hunter.<br><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/582010/extras/MHW_otomo.jpg?t=1581101000\" >",
        "supported_languages": "English<strong>*</strong>, French<strong>*</strong>, Italian<strong>*</strong>, German<strong>*</strong>, Spanish - Spain<strong>*</strong>, Arabic, Portuguese - Brazil, Polish, Traditional Chinese, Japanese<strong>*</strong>, Korean, Russian, Simplified Chinese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://www.beartai.com/wp-content/uploads/2018/07/MonsterHunter-1.jpg",
        "website": "http://www.monsterhunterworld.com/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> WINDOWS® 7, 8, 8.1, 10 (64-bit required)<br></li><li><strong>Processor:</strong> Intel® Core™ i5-4460, 3.20GHz or AMD FX™-6300<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA® GeForce® GTX 760 or AMD Radeon™ R7 260x (VRAM 2GB)<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space<br></li><li><strong>Sound Card:</strong> DirectSound (DirectX® 9.0c)<br></li><li><strong>Additional Notes:</strong> 1080p/30fps when graphics settings are set to &quot;Low&quot;</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> WINDOWS® 7, 8, 8.1, 10 (64-bit required)<br></li><li><strong>Processor:</strong> Intel® Core™ i7 3770 3.4GHz or Intel® Core™ i3 8350 4GHz or AMD Ryzen™ 5 1500X<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA® GeForce® GTX 1060 (VRAM 3GB) or AMD Radeon™ RX 570  (VRAM 4GB)<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space<br></li><li><strong>Sound Card:</strong> DirectSound (DirectX® 9.0c or better)<br></li><li><strong>Additional Notes:</strong> 1080p/30fps when graphics settings are set to &quot;High&quot;</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "developers": [
            "CAPCOM Co., Ltd."
        ],
        "publishers": [
            "CAPCOM Co., Ltd."
        ],
        "metacritic": {
            "score": 88,
            "url": "https://www.metacritic.c…orld?ftag=MCD-06-10aaa1f"
        },
        "release_date": {
            "coming_soon": false,
            "date": "9 Aug, 2018"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_a262c53b8629de7c6547933dc0b49d31f4e1b1f1.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_a262c53b8629de7c6547933dc0b49d31f4e1b1f1.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_6b4986a37c7b5c185a796085c002febcdd5357b5.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_6b4986a37c7b5c185a796085c002febcdd5357b5.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_0dfb20f6f09c196bfc317bd517dc430ed6e6a2a4.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_0dfb20f6f09c196bfc317bd517dc430ed6e6a2a4.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_25902a9ae6977d6d10ebff20b87e8739e51c5b8b.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_25902a9ae6977d6d10ebff20b87e8739e51c5b8b.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_681cc5358ec55a997aee9f757ffe8b418dc79a32.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_681cc5358ec55a997aee9f757ffe8b418dc79a32.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_ce69dc57e6e442c73d874f1b701f2e4af405fb19.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_ce69dc57e6e442c73d874f1b701f2e4af405fb19.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_6d26868b45c20bf4dd5f75f31264aca08ce17217.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_6d26868b45c20bf4dd5f75f31264aca08ce17217.1920x1080.jpg?t=1581101000"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_669f9008f708c19fe3c41d647516f7a73bf26d24.600x338.jpg?t=1581101000",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/582010/ss_669f9008f708c19fe3c41d647516f7a73bf26d24.1920x1080.jpg?t=1581101000"
            }
        ],
        "movies": [
            {
                "id": 256769022,
                "name": "191128_防衛隊派生武器_English_ASIA_STEAM版",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256769022/movie.293x165.jpg?t=1578591120",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256769022/movie480.webm?t=1578591120",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256769022/movie_max.webm?t=1578591120"
                },
                "highlight": true
            },
            {
                "id": 256769014,
                "name": "191128_防具_防衛隊αシリーズ_English_ASIA_STEAM版",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256769014/movie.293x165.jpg?t=1578591130",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256769014/movie480.webm?t=1578591130",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256769014/movie_max.webm?t=1578591130"
                },
                "highlight": true
            },
            {
                "id": 256765446,
                "name": "191025_New_MHW_ICEBORNE_SteamPV_English_ASIA_1080_60p.mp4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256765446/movie.293x165.jpg?t=1578591280",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256765446/movie480.webm?t=1578591280",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256765446/movie_max.webm?t=1578591280"
                },
                "highlight": true
            },
            {
                "id": 256747419,
                "name": "190318_MHW_steamPV_TW3_English(AS).mp4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256747419/movie.293x165.jpg?t=1578591413",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256747419/movie480.webm?t=1578591413",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256747419/movie_max.webm?t=1578591413"
                },
                "highlight": true
            },
            {
                "id": 256746429,
                "name": "190318_MHW_steamPV_AC_English(AS).mp4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256746429/movie.293x165.jpg?t=1578591523",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256746429/movie480.webm?t=1578591523",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256746429/movie_max.webm?t=1578591523"
                },
                "highlight": true
            },
            {
                "id": 256737505,
                "name": "181128_01_MHW_steamPV_U5_English(AS).mp4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256737505/movie.293x165.jpg?t=1578591632",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256737505/movie480.webm?t=1578591632",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256737505/movie_max.webm?t=1578591632"
                },
                "highlight": true
            },
            {
                "id": 256721637,
                "name": "MH–WORLD_PCPV1_ASIA_ENGLISH_Multi_1080p_2997.mp4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256721637/movie.293x165.jpg?t=1578591747",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256721637/movie480.webm?t=1578591747",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256721637/movie_max.webm?t=1578591747"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/730/page_bg_generated_v6b.jpg?t=1580765900"
    },
    {
        "steam_appid": 292030,
        "name": "The Witcher® 3: Wild Hunt",
        "price": 1039.99,
        "img": "https://gamechanger.co.ke/wp-content/uploads/2015/05/The-Witcher-3-Detail-Cover.jpg",
        "detailed_description": "<h1>Check out other games from CD PROJEKT RED</h1><p><a href=\"https://store.steampowered.com/app/1091500\" target=\"_blank\" rel=\"noreferrer\"  id=\"dynamiclink_0\" >https://store.steampowered.com/app/1091500</a><br><a href=\"https://store.steampowered.com/app/20920/\" target=\"_blank\" rel=\"noreferrer\"  id=\"dynamiclink_1\" >https://store.steampowered.com/app/20920/</a><br><a href=\"https://store.steampowered.com/app/20900/\" target=\"_blank\" rel=\"noreferrer\"  id=\"dynamiclink_2\" >https://store.steampowered.com/app/20900/</a></p><br><h1>Check out other games from CD PROJEKT RED</h1><p><a href=\"https://store.steampowered.com/app/973760/\" target=\"_blank\" rel=\"noreferrer\"  >https://store.steampowered.com/app/973760/</a><br><a href=\"https://store.steampowered.com/app/303800\" target=\"_blank\" rel=\"noreferrer\"  >https://store.steampowered.com/app/303800</a></p><br><h1>Special Offer</h1><p><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/292030/extras/Purchase_Offer_615x405.png?t=1581375222\" ></p><br><h1>About the Game</h1>The Witcher: Wild Hunt is a story-driven open world RPG set in a visually stunning fantasy universe full of meaningful choices and impactful consequences. In The Witcher, you play as professional monster hunter Geralt of Rivia tasked with finding a child of prophecy in a vast open world rich with merchant cities, pirate islands, dangerous mountain passes, and forgotten caverns to explore.<h2 class=\"bb_tag\">KEY FEATURES</h2><strong>PLAY AS A HIGHLY TRAINED MONSTER SLAYER FOR HIRE</strong><br>Trained from early childhood and mutated to gain superhuman skills, strength and reflexes, witchers are a counterbalance to the monster-infested world in which they live.<br><br><ul class=\"bb_ul\"><li>Gruesomely destroy foes as a professional monster hunter armed with a range of upgradeable weapons, mutating potions and combat magic.<br></li><li>Hunt down a wide range of exotic monsters — from savage beasts prowling the mountain passes, to cunning supernatural predators lurking in the shadows of densely populated towns.<br></li><li>Invest your rewards to upgrade your weaponry and buy custom armour, or spend them away in horse races, card games, fist fighting, and other pleasures the night brings.</li></ul><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/292030/extras/W3_1.gif?t=1581375222\" ><br><br><strong>EXPLORE A MORALLY INDIFFERENT FANTASY OPEN WORLD</strong><br>Built for endless adventure, the massive open world of The Witcher sets new standards in terms of size, depth and complexity.<br><br><ul class=\"bb_ul\"><li>Traverse a fantastical open world: explore forgotten ruins, caves and shipwrecks, trade with merchants and dwarven smiths in cities, and hunt across the open plains, mountains and seas.<br></li><li>Deal with treasonous generals, devious witches and corrupt royalty to provide dark and dangerous services.<br></li><li>Make choices that go beyond good &amp; evil, and face their far-reaching consequences.</li></ul><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/292030/extras/W3_3.gif?t=1581375222\" ><br><br><strong>CHASE DOWN THE CHILD OF PROPHECY</strong><br>Take on the most important contract to track down the child of prophecy, a key to save or destroy this world.<br><br><ul class=\"bb_ul\"><li>In times of war, chase down the child of prophecy, a living weapon foretold by ancient elven legends.<br></li><li>Struggle against ferocious rulers, spirits of the wilds and even a threat from beyond the veil – all hell-bent on controlling this world.<br></li><li>Define your destiny in a world that may not be worth saving.</li></ul><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/292030/extras/W3_2.gif?t=1581375222\" ><br><br><strong>FULLY REALIZED NEXT GENERATION</strong><br><ul class=\"bb_ul\"><li>Built exclusively for next generation hardware, the REDengine 3 renders the world of The Witcher visually nuanced and organic, a real true to life fantasy.<br></li><li>Dynamic weather systems and day/night cycles affect how the citizens of the towns and the monsters of the wilds behave.<br></li><li>Rich with storyline choices in both main and subplots, this grand open world is influenced by the player unlike ever before.</li></ul><img src=\"https://steamcdn-a.akamaihd.net/steam/apps/292030/extras/W3_4.gif?t=1581375222\" >",
        "supported_languages": "English<strong>*</strong>, French<strong>*</strong>, Italian, German<strong>*</strong>, Spanish - Spain, Arabic, Czech, Hungarian, Japanese<strong>*</strong>, Korean, Polish<strong>*</strong>, Portuguese - Brazil<strong>*</strong>, Russian<strong>*</strong>, Traditional Chinese, Turkish, Simplified Chinese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://www.beartai.com/wp-content/uploads/2019/06/The-Witcher-3-Wild-Hunt-1.jpg",
        "website": "http://www.thewitcher.com",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> 64-bit Windows 7, 64-bit Windows 8 (8.1) or 64-bit Windows 10<br></li><li><strong>Processor:</strong> Intel CPU Core i5-2500K 3.3GHz / AMD CPU Phenom II X4 940<br></li><li><strong>Memory:</strong> 6 GB RAM<br></li><li><strong>Graphics:</strong> Nvidia GPU GeForce GTX 660 / AMD GPU Radeon HD 7870<br></li><li><strong>Storage:</strong> 35 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class=\"bb_ul\"><li><strong>OS:</strong> 64-bit Windows 7, 64-bit Windows 8 (8.1) or 64-bit Windows 10<br></li><li><strong>Processor:</strong> Intel CPU Core i7 3770 3.4 GHz / AMD CPU AMD FX-8350 4 GHz<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> Nvidia GPU GeForce GTX 770 / AMD GPU Radeon R9 290<br></li><li><strong>Storage:</strong> 35 GB available space</li></ul>"
        },
        "mac_requirements": {

        },
        "linux_requirements": {

        },
        "developers": [
            "CD PROJEKT RED"
        ],
        "publishers": [
            "CD PROJEKT RED"
        ],
        "metacritic": {
            "score": 93,
            "url": "https://www.metacritic.com/game/pc/the-witcher-3-wild-hunt?ftag=MCD-06-10aaa1f"
        },
        "release_date": {
            "coming_soon": false,
            "date": "18 May, 2015"
        },
        "genres": [
            {
                "id": "3",
                "description": "RPG"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_107600c1337accc09104f7a8aa7f275f23cad096.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_107600c1337accc09104f7a8aa7f275f23cad096.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_64eb760f9a2b67f6731a71cce3a8fb684b9af267.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_64eb760f9a2b67f6731a71cce3a8fb684b9af267.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_eda99e7f705a113d04ab2a7a36068f3e7b343d17.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_eda99e7f705a113d04ab2a7a36068f3e7b343d17.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_d5b80eb63c12a6484f26796f3e34410651bba068.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_d5b80eb63c12a6484f26796f3e34410651bba068.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_b74d60ee215337d765e4d20c8ca6710ae2362cc2.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_b74d60ee215337d765e4d20c8ca6710ae2362cc2.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_dc55eeb409d6e187456a8e159018e8da098fa468.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_dc55eeb409d6e187456a8e159018e8da098fa468.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_849ec8dcc6f8df1c0b2c509584c9fc9e51f88cfa.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_849ec8dcc6f8df1c0b2c509584c9fc9e51f88cfa.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_90a33d7764a2d23306091bfcb52265c3506b4fdb.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_90a33d7764a2d23306091bfcb52265c3506b4fdb.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_07812c174bb6b96c29895ddc27404143df7aba6d.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_07812c174bb6b96c29895ddc27404143df7aba6d.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_ed23139c916fdb9f6dd23b2a6a01d0fbd2dd1a4f.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_ed23139c916fdb9f6dd23b2a6a01d0fbd2dd1a4f.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_dc33eb233555c13fce939ccaac667bc54e3c4a27.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_dc33eb233555c13fce939ccaac667bc54e3c4a27.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_908485cbb1401b1ebf42e3d21a860ddc53517b08.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_908485cbb1401b1ebf42e3d21a860ddc53517b08.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_608af6cfe0eab3f37265550b391732a3e88d1a4f.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_608af6cfe0eab3f37265550b391732a3e88d1a4f.1920x1080.jpg?t=1581375222"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_8ac1dc847388e59b1be1c5ea5ca592d5861756cc.600x338.jpg?t=1581375222",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/292030/ss_8ac1dc847388e59b1be1c5ea5ca592d5861756cc.1920x1080.jpg?t=1581375222"
            }
        ],
        "movies": [
            {
                "id": 256658589,
                "name": "ESRB The Witcher 3: Wild Hunt - Epic Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256658589/movie.293x165.jpg?t=1528288687",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256658589/movie480.webm?t=1528288687",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256658589/movie_max.webm?t=1528288687"
                },
                "highlight": true
            },
            {
                "id": 2039386,
                "name": "PEGI The Witcher 3: Wild Hunt - Launch Cinematic",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/2039386/movie.293x165.jpg?t=1528288460",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/2039386/movie480.webm?t=1528288460",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/2039386/movie_max.webm?t=1528288460"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/292030/page_bg_generated_v6b.jpg?t=1581375222"
    },
    {
        "steam_appid": 1174180,
        "name": "Red Dead Redemption 2",
        "price": 1599.00,
        "img": "https://gamesanookth.com/wp-content/uploads/2019/12/red-dead-redemption-2-standard-edition-cover-1024x1387.jpg",
        "detailed_description": "<h1>Special Edition</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/1174180/extras/special_615.jpg?t=1575569357' ><br>The Red Dead Redemption 2 Special Edition delivers exclusive content for Story Mode including a Bank Robbery Mission &amp; Gang Hideout, Dappled Black Thoroughbred, Talisman &amp; Medallion Gameplay Bonuses, Gameplay Boosts, Cash Bonuses &amp; Discounts, and the Nuevo Paraiso Gunslinger Outfit, plus free access to Additional Weapons.</p><br><h1>Ultimate Edition</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/1174180/extras/Ultimate_615.jpg?t=1575569357' ><br>The Red Dead Redemption 2: Ultimate Edition delivers all of the Story Mode content from the Special Edition plus additional content for online including Bonus Outfits for your online Character, Rank Bonuses, Black Chestnut Thoroughbred and free access to the Survivor Camp Theme.<br><br>Plus get free access to Additional Weapons in online.</p><br><h1>About the Game</h1><strong>Red Dead Redemption 2, the critically acclaimed open world epic from Rockstar Games and the highest rated game of the console generation, now enhanced for PC with new Story Mode content, visual upgrades and more.</strong><br><br>Red Dead Redemption 2, the critically acclaimed open world epic from Rockstar Games and the highest rated game of the console generation, now enhanced for PC with new Story Mode content, visual upgrades and more.<br><br>America, 1899. The end of the wild west era has begun. After a robbery goes badly wrong in the western town of Blackwater, Arthur Morgan and the Van der Linde gang are forced to flee. With federal agents and the best bounty hunters in the nation massing on their heels, the gang must rob, steal and fight their way across the rugged heartland of America in order to survive. As deepening internal divisions threaten to tear the gang apart, Arthur must make a choice between his own ideals and loyalty to the gang who raised him.<br><br>With all new graphical and technical enhancements for deeper immersion, Red Dead Redemption 2 for PC takes full advantage of the power of the PC to bring every corner of this massive, rich and detailed world to life including increased draw distances; higher quality global illumination and ambient occlusion for improved day and night lighting; improved reflections and deeper, higher resolution shadows at all distances; tessellated tree textures and improved grass and fur textures for added realism in every plant and animal.<br><br>Red Dead Redemption 2 for PC also offers HDR support, the ability to run high-end display setups with 4K resolution and beyond, multi-monitor configurations, widescreen configurations, faster frame rates and more.<br><br>Red Dead Redemption 2 for PC also includes additional Story Mode content including Bounty Hunting Missions, Gang Hideouts, Weapons and more; plus free access to the shared living world of Red Dead Online featuring all previously released improvements and the latest content for the complete Online experience including Frontier Pursuits and the specialist Roles of The Bounty Hunter, Trader and Collector, and much more. <br><br>Red Dead Redemption 2 for PC is the ultimate way to experience one of the most critically acclaimed games of all time, winner of over 175 Game of the Year Awards and recipient of over 250 perfect scores.<h2 class='bb_tag>Disclaimer</h2><i>Installation, activation, and online play require Rockstar Games Launcher &amp; log-in to Rockstar Games Social Club (varies 13+); internet required for activation, online play, and periodic entitlement verification; software installations required including Rockstar Games Launcher, DirectX, Microsoft Visual C++ 2015-2019 Redistributables (x64), Chromium Embedded Framework, Rockstar Games Social Club Framework and authentication software that recognizes certain hardware attributes for entitlement, digital rights management, license enforcement, support, system, and other purposes.<br><br>Internet &amp; Rockstar Games Launcher required; registration is limited to one Rockstar Games Social Club account (varies 13+) per purchase; only one PC log-in allowed per Rockstar Games Social Club account at any time; purchases are non-transferable; Rockstar Games Social Club accounts are non-transferable.<br><br>Over time downloadable content and programming changes will change the system requirements for this game. Please refer to your hardware manufacturer and <a href='https://steamcommunity.com/linkfilter/?url=http://www.rockstargames.com/support' target='_blank' rel='noopener' >www.rockstargames.com/support</a> for current compatibility information. Some system components such as mobile chipsets, integrated, and AGP graphics cards may be incompatible. Unlisted specifications may not be supported by publisher.</i>",
        "supported_languages": "English<strong>*</strong>, French, Italian, German, Spanish - Spain, Japanese, Korean, Polish, Portuguese, Russian, Simplified Chinese, Spanish - Latin America, Traditional Chinese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://www.nasdaq.com/sites/acquia.prod/files/image/4b4169c9610ae49002a3b3284ccd5c7e1b864dbb_take-two-game-art-for-rdr-2.jpg",
        "website": "https://www.rockstargames.com/reddeadredemption2/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 7 - Service Pack 1 (6.1.7601)<br></li><li><strong>Processor:</strong> Intel® Core™ i5-2500K / AMD FX-6300<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> Nvidia GeForce GTX 770 2GB / AMD Radeon R9 280 3GB<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 150 GB available space<br></li><li><strong>Sound Card:</strong> Direct X Compatible</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 10 - April 2018 Update (v1803)<br></li><li><strong>Processor:</strong> Intel® Core™ i7-4770K / AMD Ryzen 5 1500X<br></li><li><strong>Memory:</strong> 12 GB RAM<br></li><li><strong>Graphics:</strong> Nvidia GeForce GTX 1060 6GB / AMD Radeon RX 480 4GB<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 150 GB available space<br></li><li><strong>Sound Card:</strong> Direct X Compatible</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "developers": [
            "Rockstar Games"
        ],
        "publishers": [
            "Rockstar Games"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "6 Dec, 2019"
        },
        "metacritic": {
            "score": 93,
            "url": "https://www.metacritic.com/game/pc/red-dead-redemption-2?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "25",
                "description": "Adventure"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_66b553f4c209476d3e4ce25fa4714002cc914c4f.600x338.jpg?t=1575569357",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_66b553f4c209476d3e4ce25fa4714002cc914c4f.1920x1080.jpg?t=1575569357"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_bac60bacbf5da8945103648c08d27d5e202444ca.600x338.jpg?t=1575569357",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_bac60bacbf5da8945103648c08d27d5e202444ca.1920x1080.jpg?t=1575569357"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_668dafe477743f8b50b818d5bbfcec669e9ba93e.600x338.jpg?t=1575569357",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_668dafe477743f8b50b818d5bbfcec669e9ba93e.1920x1080.jpg?t=1575569357"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_4ce07ae360b166f0f650e9a895a3b4b7bf15e34f.600x338.jpg?t=1575569357",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_4ce07ae360b166f0f650e9a895a3b4b7bf15e34f.1920x1080.jpg?t=1575569357"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_d1a8f5a69155c3186c65d1da90491fcfd43663d9.600x338.jpg?t=1575569357",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/ss_d1a8f5a69155c3186c65d1da90491fcfd43663d9.1920x1080.jpg?t=1575569357"
            }
        ],
        "movies": [
            {
                "id": 256768371,
                "name": "RDR2 60 FPS Trailer (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256768371/movie.293x165.jpg?t=1574881352",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256768371/movie480.webm?t=1574881352",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256768371/movie_max.webm?t=1574881352"
                },
                "highlight": true
            },
            {
                "id": 256768370,
                "name": "RDR2 Launch Trailer (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256768370/movie.293x165.jpg?t=1574881363",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256768370/movie480.webm?t=1574881363",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256768370/movie_max.webm?t=1574881363"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/1174180/page_bg_generated_v6b.jpg?t=1575569357",

    },
    {
        "steam_appid": 578080,
        "name": "PLAYERUNKNOWN'S BATTLEGROUNDS",
        "price": 559.00,
        "img": "https://dq.lnwfile.com/bjh722.jpg",
        "detailed_description": "<strong>PLAYERUNKNOWN'S BATTLEGROUNDS</strong> is a battle royale shooter that pits 100 players against each other in a struggle for survival. Gather supplies and outwit your opponents to become the last person standing. <br><br><strong>PLAYERUNKNOWN</strong>, aka Brendan Greene, is a pioneer of the battle royale genre and the creator of the battle royale game modes in the ARMA series and H1Z1: King of the Kill. At PUBG Corp., Greene is working with a veteran team of developers to make PUBG into the world's premiere battle royale experience.",
        "supported_languages": "English, Korean, Simplified Chinese, French, German, Spanish - Spain, Arabic, Japanese, Polish, Portuguese, Russian, Turkish, Thai, Italian, Portuguese - Brazil, Traditional Chinese, Ukrainian",
        "header_image": "https://4.bp.blogspot.com/-b-Qy44sW3y8/XH6E1WSlbXI/AAAAAAAAAXw/HPuScudwA-0qOkKPb7mvyeFL4OGoWmsKgCKgBGAs/w0/pubg-playerunknowns-battlegrounds-uhdpaper.com-4K-48.jpg",
        "website": "http://www.playbattlegrounds.com",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> 64-bit Windows 7, Windows 8.1, Windows 10<br></li><li><strong>Processor:</strong> Intel Core i5-4430 / AMD FX-6300<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 960 2GB / AMD Radeon R7 370 2GB<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> 64-bit Windows 7, Windows 8.1, Windows 10<br></li><li><strong>Processor:</strong> Intel Core i5-6600K / AMD Ryzen 5 1600<br></li><li><strong>Memory:</strong> 16 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 1060 3GB / AMD Radeon RX 580 4GB<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "developers": [
            "PUBG Corporation"
        ],
        "publishers": [
            "PUBG Corporation"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "21 Dec, 2017"
        },
        "metacritic": {
            "score": 86,
            "url": "https://www.metacritic.com/game/pc/playerunknowns-battlegrounds?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "25",
                "description": "Adventure"
            },
            {
                "id": "29",
                "description": "Massively Multiplayer"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_abadb3bfc951cd05150901ff65386e3129c6011a.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_abadb3bfc951cd05150901ff65386e3129c6011a.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_cec7ea5e83407dba51c80d24a2c8076e93752d4f.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_cec7ea5e83407dba51c80d24a2c8076e93752d4f.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_8814c071f0cce53821d8e1b1a96de78d00e5d4d1.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_8814c071f0cce53821d8e1b1a96de78d00e5d4d1.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_5fe3d8ce7e90442569fc676e2315fffdf81dd1a5.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_5fe3d8ce7e90442569fc676e2315fffdf81dd1a5.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_2b1f73afd6efb1952ee267e94f8bcc24ec5e8fb3.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_2b1f73afd6efb1952ee267e94f8bcc24ec5e8fb3.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_52773275afe4c34a84fbf38e9960a598a420b3c2.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_52773275afe4c34a84fbf38e9960a598a420b3c2.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_c98c609e2f07c80c8455624cc62696c9dde9ea73.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_c98c609e2f07c80c8455624cc62696c9dde9ea73.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_07590e851053a453e09c7d6f272adf602c3f8ee8.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_07590e851053a453e09c7d6f272adf602c3f8ee8.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_56d29e4503ba1a74047feb55986c6bf4ca4297f9.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_56d29e4503ba1a74047feb55986c6bf4ca4297f9.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_f96bc802fb38617ce790c0950b87a7979f096025.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_f96bc802fb38617ce790c0950b87a7979f096025.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_23af2e59855a833c22d0c11ca23a719f54a554ff.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_23af2e59855a833c22d0c11ca23a719f54a554ff.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_108e2981889423b057b778cd07ae25ac18406cf1.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_108e2981889423b057b778cd07ae25ac18406cf1.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_a403a1f4071d36d42bea7a505089b56b570b2569.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_a403a1f4071d36d42bea7a505089b56b570b2569.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_109d7072cf85f5b3b1e3dacadf3009718db451c4.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_109d7072cf85f5b3b1e3dacadf3009718db451c4.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 14,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_e7e79847eff0933de92192bb62b8bc7068d611da.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_e7e79847eff0933de92192bb62b8bc7068d611da.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 15,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_8112cd376568d9470c2edde841908fcdf46f1529.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_8112cd376568d9470c2edde841908fcdf46f1529.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 16,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_4bbcaeac1ef977d962c60c1a5e4675cdd81de564.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_4bbcaeac1ef977d962c60c1a5e4675cdd81de564.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 17,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_88a8dff0756673179e190c2e16d090de63ecfb2e.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_88a8dff0756673179e190c2e16d090de63ecfb2e.1920x1080.jpg?t=1581036302"
            },
            {
                "id": 18,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_29d0709711f3204e84b6f9d69f3be163ebe12486.600x338.jpg?t=1581036302",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/578080/ss_29d0709711f3204e84b6f9d69f3be163ebe12486.1920x1080.jpg?t=1581036302"
            }
        ],
        "movies": [
            {
                "id": 256773105,
                "name": "Survivor Pass 6",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256773105/movie.293x165.jpg?t=1579721657",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256773105/movie480.webm?t=1579721657",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256773105/movie_max.webm?t=1579721657"
                },
                "highlight": true
            },
            {
                "id": 256774477,
                "name": "Season 5",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256774477/movie.293x165.jpg?t=1581036269",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256774477/movie480.webm?t=1581036269",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256774477/movie_max.webm?t=1581036269"
                },
                "highlight": true
            },
            {
                "id": 256774476,
                "name": "Season 5 CGI",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256774476/movie.293x165.jpg?t=1581036278",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256774476/movie480.webm?t=1581036278",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256774476/movie_max.webm?t=1581036278"
                },
                "highlight": true
            },
            {
                "id": 256774471,
                "name": "Season 4",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256774471/movie.293x165.jpg?t=1581036288",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256774471/movie480.webm?t=1581036288",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256774471/movie_max.webm?t=1581036288"
                },
                "highlight": true
            },
            {
                "id": 256774464,
                "name": "Season 3",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256774464/movie.293x165.jpg?t=1581036297",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256774464/movie480.webm?t=1581036297",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256774464/movie_max.webm?t=1581036297"
                },
                "highlight": true
            },
            {
                "id": 256757848,
                "name": "Vikendi CGI Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256757848/movie.293x165.jpg?t=1564606214",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256757848/movie480.webm?t=1564606214",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256757848/movie_max.webm?t=1564606214"
                },
                "highlight": true
            },
            {
                "id": 256720476,
                "name": "Sanhok Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256720476/movie.293x165.jpg?t=1529548698",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256720476/movie480.webm?t=1529548698",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256720476/movie_max.webm?t=1529548698"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/578080/page_bg_generated_v6b.jpg?t=1581036302",

    },
    {
        "steam_appid": 271590,
        "name": "Grand Theft Auto V",
        "price": 699.00,
        "img": "https://i.pinimg.com/564x/46/f3/fd/46f3fdb5f464dd8f1ac72f908f764ded.jpg",
        "detailed_description": "When a young street hustler, a retired bank robber and a terrifying psychopath find themselves entangled with some of the most frightening and deranged elements of the criminal underworld, the U.S. government and the entertainment industry, they must pull off a series of dangerous heists to survive in a ruthless city in which they can trust nobody, least of all each other.<br> <br>Grand Theft Auto V for PC offers players the option to explore the award-winning world of Los Santos and Blaine County in resolutions of up to 4k and beyond, as well as the chance to experience the game running at 60 frames per second. <br> <br>The game offers players a huge range of PC-specific customization options, including over 25 separate configurable settings for texture quality, shaders, tessellation, anti-aliasing and more, as well as support and extensive customization for mouse and keyboard controls. Additional options include a population density slider to control car and pedestrian traffic, as well as dual and triple monitor support, 3D compatibility, and plug-and-play controller support. <br> <br>Grand Theft Auto V for PC also includes Grand Theft Auto Online, with support for 30 players and two spectators. Grand Theft Auto Online for PC will include all existing gameplay upgrades and Rockstar-created content released since the launch of Grand Theft Auto Online, including Heists and Adversary modes.<br> <br>The PC version of Grand Theft Auto V and Grand Theft Auto Online features First Person Mode, giving players the chance to explore the incredibly detailed world of Los Santos and Blaine County in an entirely new way.<br> <br>Grand Theft Auto V for PC also brings the debut of the Rockstar Editor, a powerful suite of creative tools to quickly and easily capture, edit and share game footage from within Grand Theft Auto V and Grand Theft Auto Online. The Rockstar Editor’s Director Mode allows players the ability to stage their own scenes using prominent story characters, pedestrians, and even animals to bring their vision to life. Along with advanced camera manipulation and editing effects including fast and slow motion, and an array of camera filters, players can add their own music using songs from GTAV radio stations, or dynamically control the intensity of the game’s score. Completed videos can be uploaded directly from the Rockstar Editor to YouTube and the Rockstar Games Social Club for easy sharing. <br> <br>Soundtrack artists The Alchemist and Oh No return as hosts of the new radio station, The Lab FM. The station features new and exclusive music from the production duo based on and inspired by the game’s original soundtrack. Collaborating guest artists include Earl Sweatshirt, Freddie Gibbs, Little Dragon, Killer Mike, Sam Herring from Future Islands, and more. Players can also discover Los Santos and Blaine County while enjoying their own music through Self Radio, a new radio station that will host player-created custom soundtracks.<br><br>Special access content requires Rockstar Games Social Club account. Visit <a href='https://steamcommunity.com/linkfilter/?url=http://rockstargames.com/v/bonuscontent' target='_blank' rel='noopener' >http://rockstargames.com/v/bonuscontent</a> for details.",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/271590/header.jpg?t=1579797647",
        "website": "http://www.rockstargames.com/V/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1<br></li><li><strong>Processor:</strong> Intel Core 2 Quad CPU Q6600 @ 2.40GHz (4 CPUs) / AMD Phenom 9850 Quad-Core Processor (4 CPUs) @ 2.5GHz<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA 9800 GT 1GB / AMD HD 4870 1GB (DX 10, 10.1, 11)<br></li><li><strong>Storage:</strong> 72 GB available space<br></li><li><strong>Sound Card:</strong> 100% DirectX 10 compatible<br></li><li><strong>Additional Notes:</strong> Over time downloadable content and programming changes will change the system requirements for this game. Please refer to your hardware manufacturer and <a href='https://steamcommunity.com/linkfilter/?url=http://www.rockstargames.com/support' target='_blank' rel='noopener' >www.rockstargames.com/support</a> for current compatibility information. Some system components such as mobile chipsets, integrated, and AGP graphics cards may be incompatible. Unlisted specifications may not be supported by publisher. Other requirements: Installation and online play requires log-in to Rockstar Games Social Club (13+) network; internet connection required for activation, online play, and periodic entitlement verification; software installations required including Rockstar Games Social Club platform, DirectX , Chromium, and Microsoft Visual C++ 2008 sp1 Redistributable Package, and authentication software that recognizes certain hardware attributes for entitlement, digital rights management, system, and other support purposes. SINGLE USE SERIAL CODE REGISTRATION VIA INTERNET REQUIRED; REGISTRATION IS LIMITED TO ONE ROCKSTAR GAMES SOCIAL CLUB ACCOUNT (13+) PER SERIAL CODE; ONLY ONE PC LOG-IN ALLOWED PER SOCIAL CLUB ACCOUNT AT ANY TIME; SERIAL CODE(S) ARE NON-TRANSFERABLE ONCE USED; SOCIAL CLUB ACCOUNTS ARE NON-TRANSFERABLE. Partner Requirements: Please check the terms of service of this site before purchasing this software.</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1<br></li><li><strong>Processor:</strong> Intel Core i5 3470 @ 3.2GHz (4 CPUs) / AMD X8 FX-8350 @ 4GHz (8 CPUs)<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GTX 660 2GB / AMD HD 7870 2GB<br></li><li><strong>Storage:</strong> 72 GB available space<br></li><li><strong>Sound Card:</strong> 100% DirectX 10 compatible</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "developers": [
            "Rockstar North"
        ],
        "publishers": [
            "Rockstar Games"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "14 Apr, 2015"
        },
        "metacritic": {
            "score": 96,
            "url": "https://www.metacritic.com/game/pc/grand-theft-auto-v?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "25",
                "description": "Adventure"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_32aa18ab3175e3002217862dd5917646d298ab6b.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_32aa18ab3175e3002217862dd5917646d298ab6b.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_2744f112fa060320d191a50e8b3a92441a648a56.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_2744f112fa060320d191a50e8b3a92441a648a56.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_da39c16db175f6973770bae6b91d411251763152.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_da39c16db175f6973770bae6b91d411251763152.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bd5db78286be0a7c6b2c62519099a9e27e6b06f3.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bd5db78286be0a7c6b2c62519099a9e27e6b06f3.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_b1a1cb7959d6a0e6fcb2d06ebf97a66c9055cef3.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_b1a1cb7959d6a0e6fcb2d06ebf97a66c9055cef3.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bc5fc79d3366c837372327717249a4887aa46d63.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bc5fc79d3366c837372327717249a4887aa46d63.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_d2eb9d3e50f9e4cb8db37d2976990b3795da8187.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_d2eb9d3e50f9e4cb8db37d2976990b3795da8187.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bd944debbec9936769f6dfb39ee456ca605615e3.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_bd944debbec9936769f6dfb39ee456ca605615e3.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_66daaa8e0416b805ffb9a853235e21468d6b85bc.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_66daaa8e0416b805ffb9a853235e21468d6b85bc.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_3820844756ae43340809e247fea327025dca1e39.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_3820844756ae43340809e247fea327025dca1e39.1920x1080.jpg?t=1579797647"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_5eafc9316db476e74c1b38b25f15c1326f4d574a.600x338.jpg?t=1579797647",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/271590/ss_5eafc9316db476e74c1b38b25f15c1326f4d574a.1920x1080.jpg?t=1579797647"
            }
        ],
        "movies": [
            {
                "id": 256757115,
                "name": "GTA Online: Casino (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256757115/movie.293x165.jpg?t=1563930864",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256757115/movie480.webm?t=1563930864",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256757115/movie_max.webm?t=1563930864"
                },
                "highlight": true
            },
            {
                "id": 256738410,
                "name": "GTA Online: Arena War (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256738410/movie.293x165.jpg?t=1544815053",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256738410/movie480.webm?t=1544815053",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256738410/movie_max.webm?t=1544815053"
                },
                "highlight": true
            },
            {
                "id": 256723106,
                "name": "GTA Online: After Hours (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256723106/movie.293x165.jpg?t=1532462557",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256723106/movie480.webm?t=1532462557",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256723106/movie_max.webm?t=1532462557"
                },
                "highlight": true
            },
            {
                "id": 256703142,
                "name": "GTA Online: The Doomsday Heist (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256703142/movie.293x165.jpg?t=1513275242",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256703142/movie480.webm?t=1513275242",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256703142/movie_max.webm?t=1513275242"
                },
                "highlight": true
            },
            {
                "id": 256698003,
                "name": "GTA Online: Transform Races (INT)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256698003/movie.293x165.jpg?t=1508425892",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256698003/movie480.webm?t=1508425892",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256698003/movie_max.webm?t=1508425892"
                },
                "highlight": true
            },
            {
                "id": 256693666,
                "name": "GTA Online: Smuggler's Run Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256693666/movie.293x165.jpg?t=1504052207",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256693666/movie480.webm?t=1504052207",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256693666/movie_max.webm?t=1504052207"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/271590/page_bg_generated_v6b.jpg?t=1579797647",

    },
    {
        "steam_appid": 389730,
        "name": "TEKKEN 7",
        "price": 1290.00,
        "img": "https://i.imgur.com/SpFvo54.jpg",
        "detailed_description": "<h1>Ultimate Edition</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/389730/extras/TK7_ULTED_Custom.png?t=1580308330' ><br><br>Purchase TEKKEN 7 - Ultimate Edition and get the most complete experience from TEKKEN 7! This edition includes the full TEKKEN 7 game, all additional content eligible to Season Passes 1 &amp; 2, and the playable character Eliza.</p><br><h1>About the Game</h1><img src='https://steamcdn-a.akamaihd.net/steam/apps/389730/extras/Gifs-Tekken_3.png?t=1580308330' ><br><br>Discover the epic conclusion of the Mishima clan and unravel the reasons behind each step of their ceaseless fight. Powered by Unreal Engine 4, TEKKEN 7 features stunning story-driven cinematic battles and intense duels that can be enjoyed with friends and rivals alike through innovative fight mechanics.<br><br>Love, Revenge, Pride. Everyone has a reason to fight. Values are what define us and make us human, regardless of our strengths and weaknesses. There are no wrong motivations, just the path we choose to take.<br><br>Expand your fighter's journey by purchasing the Tekken 7 Season Pass separately and gain access to stunning additional content",
        "supported_languages": "English<strong>*</strong>, French, Italian, German, Spanish - Spain, Arabic, Japanese, Korean, Portuguese - Brazil, Russian, Traditional Chinese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/389730/header.jpg?t=1580308330",
        "website": "https://www.bandainamcostudios.com",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 7/8/10 (64-bit OS required)<br></li><li><strong>Processor:</strong> Intel Core i3-4160 @ 3.60GHz or equivalent<br></li><li><strong>Memory:</strong> 6 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 660 2GB, GTX 750Ti 2GB, or equivalent<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 60 GB available space<br></li><li><strong>Sound Card:</strong> DirectX compatible soundcard or onboard chipset</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> Windows 7/8/10 (64-bit OS required)<br></li><li><strong>Processor:</strong> Intel Core i5-4690 3.5 GHz or equivalent<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 1060 equivalent or higher <br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 60 GB available space<br></li><li><strong>Sound Card:</strong> DirectX compatible soundcard or onboard chipset </li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li>Requires a 64-bit processor and operating system</li></ul>"
        },
        "developers": [
            "BANDAI NAMCO Studios Inc."
        ],
        "publishers": [
            "BANDAI NAMCO Entertainment"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "2 Jun, 2017"
        },
        "metacritic": {
            "score": 82,
            "url": "https://www.metacritic.com/game/pc/tekken-7?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "18",
                "description": "Sports"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_d92a558644ad60ae5814fc4d2bbaebc5abf62fa3.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_d92a558644ad60ae5814fc4d2bbaebc5abf62fa3.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_40faa5ba39563cb899f1ab2ddd2afbf8b451d52f.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_40faa5ba39563cb899f1ab2ddd2afbf8b451d52f.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_ed3246605518907954918eb06e90705249be77d6.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_ed3246605518907954918eb06e90705249be77d6.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_4274653781c2c618f7749cdb569e2b9274fceba9.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_4274653781c2c618f7749cdb569e2b9274fceba9.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_744702d2289cc49bebb1b2fba7dd758b5534e1c5.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_744702d2289cc49bebb1b2fba7dd758b5534e1c5.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_e4266a29ed7485522c85ff61b454f3765151a0db.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_e4266a29ed7485522c85ff61b454f3765151a0db.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_8f6d9f87f50513ed756004cfa5b2b54cdeb1df18.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_8f6d9f87f50513ed756004cfa5b2b54cdeb1df18.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_c7a9506d38bf22e205217cede4da7a909de2d493.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_c7a9506d38bf22e205217cede4da7a909de2d493.1920x1080.jpg?t=1580308330"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_0030a7c969ad251a3cf6a194309911c99cc994b5.600x338.jpg?t=1580308330",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/389730/ss_0030a7c969ad251a3cf6a194309911c99cc994b5.1920x1080.jpg?t=1580308330"
            }
        ],
        "movies": [
            {
                "id": 256678236,
                "name": "Tekken 7 - Rage & Sorrow",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256678236/movie.293x165.jpg?t=1496410296",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256678236/movie480.webm?t=1496410296",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256678236/movie_max.webm?t=1496410296"
                },
                "highlight": true
            },
            {
                "id": 256674854,
                "name": "Tekken 7 - Golden Joystick Award Mash-up Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256674854/movie.293x165.jpg?t=1496409949",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256674854/movie480.webm?t=1496409949",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256674854/movie_max.webm?t=1496409949"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/389730/page_bg_generated_v6b.jpg?t=1580308330",

    },
    {
        "steam_appid": 728880,
        "name": "Overcooked! 2",
        "price": 319.00,
        "img": "https://cf.shopee.co.th/file/a7f469f1d6b08d2dcf7ec51902470a6e",
        "detailed_description": "<h1>Featured DLC</h1><p><a href='https://store.steampowered.com/app/1138400/Overcooked_2__Carnival_of_Chaos/' target='_blank' rel='noreferrer' id='dynamiclink_0' >https://store.steampowered.com/app/1138400/Overcooked_2__Carnival_of_Chaos/</a><h2 class='bb_tag'>New DLC and Season Pass Available Now!</h2></p><br><h1>Don't forget to</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/test.gif?t=1581075334'></p><br><h1>About the Game</h1>Overcooked returns with a brand-new helping of chaotic cooking action! Journey back to the Onion Kingdom and assemble your team of chefs in classic couch co-op or online play for up to four players. Hold onto your aprons … it’s time to save the world (again!)<br><strong><i><br>Out of the frying pan, into the fire…</i></strong><br><br>You’ve saved the world from the Ever Peckish. Now a new threat has arisen and it’s time to get back in the kitchen to stave off the hunger of The Unbread!<br><br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/Walking_Bread.jpg?t=1581075334'><h2 class='bb_tag'>ONLINE/LOCAL MULTIPLAYER MADNESS</h2> You’ll knead to work together (or against each other) to get the highest score in chaotic local and online multiplayer.<br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/Chef_Select.jpg?t=1581075334'><h2 class='bb_tag'>FEAST YOUR EYES ON THIS</h2> Journey across a brand new overworld map by land, sea and air. Get cooking in new themes ranging from sushi restaurants, magic schools, mines and even alien planets!<br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/Overworld_Map.jpg?t=1581075334'><h2 class='bb_tag'>WHET YOUR APPETITE! </h2> Travel the land cooking up a range of new recipes that are sure to cater to any tastes, including sushi, cakes, burgers and pizzas.<br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/Untitled-4.jpg?t=1581075334'><h2 class='bb_tag'>ROMAINE CALM!</h2> Travel through teleporters, across moving platforms and save time by throwing ingredients across dynamic kitchens that shift and evolve. Some kitchens even whisk your chefs away to new locations.<br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/728880/extras/moreish_mines.jpg?t=1581075334'>",
        "supported_languages": "English, French, Italian, German, Spanish - Spain, Japanese, Simplified Chinese, Korean, Polish, Portuguese - Brazil, Traditional Chinese, Russian",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/728880/header.jpg?t=1581075334",
        "website": "http://www.ghosttowngames.com/overcooked-2/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> WIN7-64 bit<br></li><li><strong>Processor:</strong> Intel i3-2100 / AMD A8-5600k<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> GeForce GTX 630 / Radeon HD 6570<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Storage:</strong> 3 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Win7 -64 bit<br></li><li><strong>Processor:</strong> Intel i5-650 / AMD A10-5800K<br></li><li><strong>Graphics:</strong> Nvidia GeForce GTX 650 / Radeon HD 7510<br></li><li><strong>Sound Card:</strong> DirectX Compatible Sound Card</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> MacOS Sierra - 10.12.6<br></li><li><strong>Processor:</strong> 2.7 GHz Intel Core i5<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> Nvidia GeForce GT 640M<br></li><li><strong>Storage:</strong> 3 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> MacOS High Sierra - 10.13.1<br></li><li><strong>Processor:</strong> 3.1 GHz Intel Core i5<br></li><li><strong>Graphics:</strong> AMD Radeon Pro 560 / Nvidia GeForce GTX 780m<br></li><li><strong>Sound Card:</strong> DirectX Compatible Sound Card</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Mint 18 / Ubuntu 16.04.01<br></li><li><strong>Processor:</strong> Intel i3-2100 / AMD A8-5600K<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> GeForce 450GTS<br></li><li><strong>Storage:</strong> 3 GB available space<br></li><li><strong>Sound Card:</strong> DirectX Compatible Sound Card</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Mint 18 / Ubuntu 16.04.01<br></li><li><strong>Processor:</strong> 3.1 GHz Intel Core i5<br></li><li><strong>Graphics:</strong> AMD Radeon Pro 560 / Nvidia GeForce GTX 780m<br></li><li><strong>Sound Card:</strong> DirectX Compatible Sound Card</li></ul>"
        },
        "developers": [
            "Ghost Town Games Ltd.",
            "Team17 Digital Ltd"
        ],
        "publishers": [
            "Team17 Digital Ltd"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "7 Aug, 2018"
        },
        "metacritic": {
            "score": 80,
            "url": "https://www.metacritic.com/game/pc/overcooked!-2?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "4",
                "description": "Casual"
            },
            {
                "id": "23",
                "description": "Indie"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_34974a4c2047b287a1f55802fde061fd7ee1a026.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_34974a4c2047b287a1f55802fde061fd7ee1a026.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_2bd9b26dae1ea739ec36e6032d3aa86de7ce2590.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_2bd9b26dae1ea739ec36e6032d3aa86de7ce2590.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a87a245229d8196f25301685d991467412e9fef8.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a87a245229d8196f25301685d991467412e9fef8.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_02a07ff51f68e4a39cdb5860c488955a52e12beb.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_02a07ff51f68e4a39cdb5860c488955a52e12beb.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_3ad0ef6b7f1f227b20ebb8d51d53869407fe119b.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_3ad0ef6b7f1f227b20ebb8d51d53869407fe119b.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_437c0f73c664f72a73ae7db8610c933cecb50cff.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_437c0f73c664f72a73ae7db8610c933cecb50cff.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_62ca794ba62988f522cb6c7cb5e6ca6b579d6351.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_62ca794ba62988f522cb6c7cb5e6ca6b579d6351.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_c60ce268ebd2b405ddf9b2934f2f993c236c4a22.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_c60ce268ebd2b405ddf9b2934f2f993c236c4a22.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_358d0488c442ca50eba876b18e8403bd7ce56d20.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_358d0488c442ca50eba876b18e8403bd7ce56d20.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_576e1f2d7ad0b02ded1b235bb2ce0c161e485131.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_576e1f2d7ad0b02ded1b235bb2ce0c161e485131.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_834c4dc6f79e1aa7b7a9e72fee9204a707c94483.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_834c4dc6f79e1aa7b7a9e72fee9204a707c94483.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_960aa5a37573734aa4d3c7ad67d6f80dd106ed9b.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_960aa5a37573734aa4d3c7ad67d6f80dd106ed9b.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_4ad737de5e5439217ce0f0d9bd381fe0e251b87a.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_4ad737de5e5439217ce0f0d9bd381fe0e251b87a.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a5ac93e93140fce2e9682742ca145d009f1ad089.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a5ac93e93140fce2e9682742ca145d009f1ad089.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 14,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_96cc7ae855d3fde9f5b1c6c21d0ab347116d9747.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_96cc7ae855d3fde9f5b1c6c21d0ab347116d9747.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 15,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_43d3b0c28fe56c8de79a9be8e97afa7d40dddf96.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_43d3b0c28fe56c8de79a9be8e97afa7d40dddf96.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 16,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_d11e36791d7c5bf5dc980bc189b3f50b53657b51.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_d11e36791d7c5bf5dc980bc189b3f50b53657b51.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 17,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a77f2aa91cb91932a6e0bb1624b780c300d9d21d.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a77f2aa91cb91932a6e0bb1624b780c300d9d21d.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 18,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a78767be5e9f3fb714721f0ab16c173cf9d78f2f.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_a78767be5e9f3fb714721f0ab16c173cf9d78f2f.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 19,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_bec2afe4b53d2dbb08119be5a7fbf1b0df3d705f.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_bec2afe4b53d2dbb08119be5a7fbf1b0df3d705f.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 20,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_1ddfdaad646d5e64de524b929854911faf55df88.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_1ddfdaad646d5e64de524b929854911faf55df88.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 21,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_88214b3459727a759728dc9c6f4e07ad8b66f383.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_88214b3459727a759728dc9c6f4e07ad8b66f383.1920x1080.jpg?t=1581075334"
            },
            {
                "id": 22,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_088a00e30b0671f4f3a0406651983ad9d05512d7.600x338.jpg?t=1581075334",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/728880/ss_088a00e30b0671f4f3a0406651983ad9d05512d7.1920x1080.jpg?t=1581075334"
            }
        ],
        "movies": [
            {
                "id": 256773149,
                "name": "Spring Festival 2020",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256773149/movie.293x165.jpg?t=1579774082",
                "webm": {
                    480: "http://steamcdn-a.akamaihd.net/steam/apps/256773149/movie480.webm?t=1579774082",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256773149/movie_max.webm?t=1579774082"
                },
                "highlight": true
            },
            {
                "id": 256724865,
                "name": "Launch Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256724865/movie.293x165.jpg?t=1576578696",
                "webm": {
                    480: "http://steamcdn-a.akamaihd.net/steam/apps/256724865/movie480.webm?t=1576578696",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256724865/movie_max.webm?t=1576578696"
                },
                "highlight": false
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/728880/page_bg_generated_v6b.jpg?t=1581075334"
    }
    ,
    {
        "steam_appid": 477160,
        "name": "Human: Fall Flat",
        "price": 315.00,
        "img": "https://scale.coolshop-cdn.com/product-media.coolshop-cdn.com/AJ6DC2/98a2bc7fee6442ecbb071fcb25333e92.jpg/f/human-fall-flat.jpg",
        "detailed_description": "<h2 class='bb_tag'><strong>***NEW LEVEL 'ICE' AVAILABLE NOW***</strong></h2><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/477160/extras/hff-ice-4.gif?t=1579711883' ><h2 class='bb_tag'><strong>Discover the funniest cooperative physics-based puzzle-platformer!</strong></h2><br>In Human: Fall Flat you play as Bob, a wobbly hero who keeps dreaming about surreal places filled with puzzles in which he’s yet to find the exit. Exploration and ingenuity are key, challenge your creativity as every option is welcome! <br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/477160/extras/HFF_6-bobs-aztec-knockover.gif?t=1579711883' ><br><br>Bob is just a normal Human with no superpowers, but given the right tools he can do a lot. Misuse the tools and he can do even more!<br><br>The world of Human: Fall Flat features advanced physics and innovative controls that cater for a wide range of challenges. Bob’s dreams of falling are riddled with puzzles to solve and distractions to experiment with for hilarious results. The worlds may be fantastical, but the laws of physics are very real.<h2 class='bb_tag'><strong><u>FEATURES:</u></strong></h2><ul class='bb_ul'><li><h2 class='bb_ul'>ONLINE MULTIPLAYER UP TO 8 PLAYERS</h2> Fall into or host private/public lobbies with your friends and watch Bob fall, flail, wobble and stumble together. Need a hand getting that boulder on to a catapult, or need someone to break that wall? The more Bob the more Mayhem!<br></li><li><h2 class='bb_tag'>THE WOBBLY ART OF PARKOUR</h2> Direct and complete control of Bob. Nothing is scripted and no limits imposed. Bob can walk (kinda straight), jump, grab anything, climb anything, carry anything. <br></li><li><h2 class='bb_tag'>LOCAL CO-OP</h2> Play with a friend or a relative in split-screen, work together to achieve any task or spend an hour throwing each other about in the craziest ways possible.<br></li><li><h2 class='bb_tag'>CUSTOMISATION</h2> Paint your own custom Bob, dress him with silly suits or even import your face onto his via webcam.<br></li><li><h2 class='bb_tag'>SURREAL LANDSCAPES</h2> Explore open-ended levels which obey the rules of physics. Interact with almost every available object in the game and go almost everywhere - like playground of freedom.<br></li><li><h2 class='bb_tag'>UNLIMITED REPLAY VALUE</h2> Fall into Bob's dreams as many time as you want, try new paths and discover all secrets. Think outside of the box is the philosophy!</li></ul>Fall into Bob's dreams now and see how good your catapulting skills are!<br><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/477160/extras/HFF_1-bob-catapult.gif?t=1579711883' >",
        "supported_languages": "English<strong>*</strong>, French, German, Spanish - Spain, Russian, Italian, Simplified Chinese, Japanese, Korean, Polish, Portuguese, Portuguese - Brazil, Thai, Turkish, Ukrainian<br><strong>*</strong>languages with full audio support",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/477160/header.jpg?t=1579711883",
        "website": "http://www.nobrakesgames.com/human/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows XP/Vista/7/8/8.1/10 x86 and x64<br></li><li><strong>Processor:</strong> Intel Core2 Duo E6750 (2 * 2660) or equivalent | AMD Athlon 64 X2 Dual Core 6000+ (2 * 3000) or equivalent<br></li><li><strong>Memory:</strong> 1024 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GT 740 (2048 MB) or equivalent | Radeon HD 5770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows XP/Vista/7/8/8.1/10 x86 and x64<br></li><li><strong>Processor:</strong> Intel Core2 Quad Q9300 (4 * 2500) or equivalent | AMD A10-5800K APU (4*3800) or equivalent<br></li><li><strong>Memory:</strong> 2048 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GTX 460 (1024 MB) or equivalent | Radeon HD 7770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> OS X 10.9 and higher<br></li><li><strong>Processor:</strong> Intel Core2 Duo E6750 (2 * 2660) or equivalent | AMD Athlon 64 X2 Dual Core 6000+ (2 * 3000) or equivalent<br></li><li><strong>Memory:</strong> 1024 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GT 740 (2048 MB) or equivalent | Radeon HD 5770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space<br></li><li><strong>Additional Notes:</strong> Requires a two-button mouse or controller</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> OS X 10.9 and higher<br></li><li><strong>Processor:</strong> Intel Core2 Quad Q9300 (4 * 2500) or equivalent | AMD A10-5800K APU (4*3800) or equivalent<br></li><li><strong>Memory:</strong> 2048 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GTX 460 (1024 MB) or equivalent | Radeon HD 7770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space<br></li><li><strong>Additional Notes:</strong> Requires a two-button mouse or controller</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Ubuntu 15.10 and higher/Mint 17.1 x86 and x64/SteamOS Brewmaster<br></li><li><strong>Processor:</strong> Intel Core2 Duo E6750 (2 * 2660) or equivalent | AMD Athlon 64 X2 Dual Core 6000+ (2 * 3000) or equivalent<br></li><li><strong>Memory:</strong> 1024 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GT 740 (2048 MB) or equivalent | Radeon HD 5770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Ubuntu 15.10 and higher/Mint 17.1 x86 and x64/SteamOS Brewmaster<br></li><li><strong>Processor:</strong> Intel Core2 Quad Q9300 (4 * 2500) or equivalent | AMD A10-5800K APU (4*3800) or equivalent<br></li><li><strong>Memory:</strong> 2048 MB RAM<br></li><li><strong>Graphics:</strong> GeForce GTX 460 (1024 MB) or equivalent | Radeon HD 7770 (1024 MB)<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>"
        },
        "developers": [
            "No Brakes Games"
        ],
        "publishers": [
            "Curve Digital"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "22 Jul, 2016"
        },
        "metacritic": {
            "score": 70,
            "url": "https://www.metacritic.com/game/pc/human-fall-flat?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "25",
                "description": "Adventure"
            },
            {
                "id": "23",
                "description": "Indie"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_aa76c11ed58c454dbd405817aa2e9262b6a8dd2f.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_aa76c11ed58c454dbd405817aa2e9262b6a8dd2f.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_a98020c889f69671677a7a1dc3bfe4dea40bb6da.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_a98020c889f69671677a7a1dc3bfe4dea40bb6da.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_e0207d506ce9ea7009fe6db4f170539664d1e940.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_e0207d506ce9ea7009fe6db4f170539664d1e940.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_980d5ba71f30a90a0fc0661c271a5656dda0993d.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_980d5ba71f30a90a0fc0661c271a5656dda0993d.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_cce7cb035a99e4e809e8e352c3b5b1cd2243e17c.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_cce7cb035a99e4e809e8e352c3b5b1cd2243e17c.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_3e7301fb73dc5b8b19b4321d0d4895ec67621d09.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_3e7301fb73dc5b8b19b4321d0d4895ec67621d09.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_f5617527e09169f85613d38b1bae98073a3f554b.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_f5617527e09169f85613d38b1bae98073a3f554b.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_abb9230fb25818a2bbc65708dca479792682121a.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_abb9230fb25818a2bbc65708dca479792682121a.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_fd273bcfc252363b0d1af710c9a273df228c900a.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_fd273bcfc252363b0d1af710c9a273df228c900a.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_54d51aeba62973c72d77bf2fa48e26971db80344.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_54d51aeba62973c72d77bf2fa48e26971db80344.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_bc87677b5a5d9f9041feb22e9a0a2e98051c4b6b.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_bc87677b5a5d9f9041feb22e9a0a2e98051c4b6b.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_50d34bf8e2fbc4aafca961f2468bdc95e616b04a.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_50d34bf8e2fbc4aafca961f2468bdc95e616b04a.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_5d4e7f5990847ddb2a42d74e10df9730c32916d0.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_5d4e7f5990847ddb2a42d74e10df9730c32916d0.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_4cb369df645490d6597f7d3acf9133bb477cae1f.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_4cb369df645490d6597f7d3acf9133bb477cae1f.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 14,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_3db408ca89aa96d4914c877ebba59165c6358a24.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_3db408ca89aa96d4914c877ebba59165c6358a24.1920x1080.jpg?t=1579711883"
            },
            {
                "id": 15,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_ae83227506e0434942c6b9eacf1b771342c9705d.600x338.jpg?t=1579711883",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/477160/ss_ae83227506e0434942c6b9eacf1b771342c9705d.1920x1080.jpg?t=1579711883"
            }
        ],
        "movies": [
            {
                "id": 256770357,
                "name": "Human: Fall Flat - Workshop Competition Winners",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256770357/movie.293x165.jpg?t=1576593431",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256770357/movie480.webm?t=1576593431",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256770357/movie_max.webm?t=1576593431"
                },
                "highlight": true
            },
            {
                "id": 256765353,
                "name": "Human: Fall Flat - Ice Level",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256765353/movie.293x165.jpg?t=1573063527",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256765353/movie480.webm?t=1573063527",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256765353/movie_max.webm?t=1573063527"
                },
                "highlight": true
            },
            {
                "id": 256759690,
                "name": "Human: Fall Flat - Level Steam",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256759690/movie.293x165.jpg?t=1566491760",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256759690/movie480.webm?t=1566491760",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256759690/movie_max.webm?t=1566491760"
                },
                "highlight": true
            },
            {
                "id": 256737917,
                "name": "Human: Fall Flat - Dark level",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256737917/movie.293x165.jpg?t=1544438734",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256737917/movie480.webm?t=1544438734",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256737917/movie_max.webm?t=1544438734"
                },
                "highlight": true
            },
            {
                "id": 256699705,
                "name": "Multiplayer Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256699705/movie.293x165.jpg?t=1509382313",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256699705/movie480.webm?t=1509382313",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256699705/movie_max.webm?t=1509382313"
                },
                "highlight": false
            },
            {
                "id": 256684534,
                "name": "Human: Fall Flat - Out Now",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256684534/movie.293x165.jpg?t=1494252573",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256684534/movie480.webm?t=1494252573",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256684534/movie_max.webm?t=1494252573"
                },
                "highlight": false
            },
            {
                "id": 256667420,
                "name": "Human: Fall Flat - Launch Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256667420/movie.293x165.jpg?t=1494252564",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256667420/movie480.webm?t=1494252564",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256667420/movie_max.webm?t=1494252564"
                },
                "highlight": false
            },
            {
                "id": 256665192,
                "name": "Human Fall Flat - Announcement Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256665192/movie.293x165.jpg?t=1466599850",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256665192/movie480.webm?t=1466599850",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256665192/movie_max.webm?t=1466599850"
                },
                "highlight": false
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/477160/page_bg_generated_v6b.jpg?t=1579711883",

    },
    {
        "steam_appid": 413150,
        "name": "Stardew Valley",
        "price": 315.00,
        "img": "https://store-images.s-microsoft.com/image/apps.44413.65985311967005000.4f51b5e9-febf-4990-8951-33ba59b634c9.924253ef-36b2-4cc0-8bb1-9a97c88d4828",
        "detailed_description": "Stardew Valley is an open-ended country-life RPG! <br><br> <img src='https://steamcdn-a.akamaihd.net/steam/apps/413150/extras/animalStrip2.png?t=1574808568' ><br><br>You've inherited your grandfather's old farm plot in Stardew Valley. Armed with hand-me-down tools and a few coins, you set out to begin your new life. Can you learn to live off the land and turn these overgrown fields into a thriving home? It won't be easy. Ever since Joja Corporation came to town, the old ways of life have all but disappeared. The community center, once the town's most vibrant hub of activity, now lies in shambles. But the valley seems full of opportunity. With a little dedication, you might just be the one to restore Stardew Valley to greatness!<br><br><strong>Features </strong><br><ul class='bb_ul'><li><strong>Turn your overgrown field into a lively farm!  </strong> Raise animals, grow crops, start an orchard, craft useful machines, and more! You'll have plenty of space to create the farm of your dreams. <br><br></li><li><strong>4 Player Farming!</strong> Invite 1-3 players to join you in the valley online! Players can work together to build a thriving farm, share resources, and improve the local community.  As more hands are better than one, players have the option to scale profit margin on produce sold for a more challenging experience.<br><br></li><li><strong>Improve your skills over time. </strong> As you make your way from a struggling greenhorn to a master farmer, you'll level up in 5 different areas: farming, mining, combat, fishing, and foraging. As you progress, you'll learn new cooking and crafting recipes, unlock new areas to explore, and customize your skills by choosing from a variety of professions. <br><br></li><li><strong>Become part of the local community. </strong> With over 30 unique characters living in Stardew Valley, you won't have a problem finding new friends! Each person has their own daily schedule, birthday, unique mini-cutscenes, and new things to say throughout the week and year. As you make friends with them, they will open up to you, ask you for help with their personal troubles, or tell you their secrets! Take part in seasonal festivals such as the luau, haunted maze, and feast of the winter star. <br><br></li><li><strong>Explore a vast, mysterious cave. </strong> As you travel deeper underground, you'll encounter new and dangerous monsters, powerful weapons, new environments, valuable gemstones, raw materials for crafting and upgrading tools, and mysteries to be uncovered. <br><br></li><li><strong>Breathe new life into the valley.</strong> Since JojaMart opened, the old way of life in Stardew Valley has changed. Much of the town's infrastructure has fallen into disrepair. Help restore Stardew Valley to it's former glory by repairing the old community center, or take the alternate route and join forces with Joja Corporation.<br><br></li><li><strong>Court and marry a partner to share your life on the farm with. </strong> There are 12 available bachelors and bachelorettes to woo, each with unique character progression cutscenes. Once married, your partner will live on the farm with you. Who knows, maybe you'll have kids and start a family?<br><br></li><li><strong>Spend a relaxing afternoon at one of the local fishing spots. </strong> The waters are teeming with seasonal varieties of delicious fish. Craft bait, bobbers, and crab pots to help you in your journey toward catching every fish and becoming a local legend! <br><br></li><li><strong>Donate artifacts and minerals to the local museum. </strong><br><br></li><li><strong>Cook delicious meals and craft useful items to help you out.  </strong>With over 100 cooking and crafting recipes, you'll have a wide variety of items to create. Some dishes you cook will even give you temporary boosts to skills, running speed, or combat prowess. Craft useful objects like scarecrows, oil makers, furnaces, or even the rare and expensive crystalarium. <br><br></li><li><strong>Customize the appearance of your character and house. </strong> With hundreds of decorative items to choose from, you'll have no trouble creating the home of your dreams!<br><br></li><li><strong>Xbox controller support (with rumble)! (Keyboard still required for text input) </strong><br><br></li><li><strong>Over two hours of original music. </strong></li></ul>",
        "supported_languages": "English, German, Spanish - Spain, Japanese, Portuguese - Brazil, Russian, Simplified Chinese, French, Italian, Hungarian, Korean, Turkish",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/413150/header.jpg?t=1574808568",
        "website": "http://www.stardewvalley.net",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows Vista or greater<br></li><li><strong>Processor:</strong> 2 Ghz<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> 256 mb video memory, shader model 3.0+<br></li><li><strong>DirectX:</strong> Version 10<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>"
        },
        "mac_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Mac OSX 10.10+<br></li><li><strong>Processor:</strong> 2 Ghz<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> 256 mb video memory, OpenGL 2<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>"
        },
        "linux_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Ubuntu 12.04 LTS<br></li><li><strong>Processor:</strong> 2 Ghz<br></li><li><strong>Memory:</strong> 2 GB RAM<br></li><li><strong>Graphics:</strong> 256 mb video memory, OpenGL 2<br></li><li><strong>Storage:</strong> 500 MB available space</li></ul>"
        },
        "developers": [
            "ConcernedApe"
        ],
        "publishers": [
            "ConcernedApe"
        ],
        "release_date": {
            "coming_soon": false,
            "date": "27 Feb, 2016"
        },
        "metacritic": {
            "score": 89,
            "url": "https://www.metacritic.com/game/pc/stardew-valley?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "23",
                "description": "Indie"
            },
            {
                "id": "3",
                "description": "RPG"
            },
            {
                "id": "28",
                "description": "Simulation"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_b887651a93b0525739049eb4194f633de2df75be.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_b887651a93b0525739049eb4194f633de2df75be.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_9ac899fe2cda15d48b0549bba77ef8c4a090a71c.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_9ac899fe2cda15d48b0549bba77ef8c4a090a71c.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_4fa0866709ede3753fdf2745349b528d5e8c4054.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_4fa0866709ede3753fdf2745349b528d5e8c4054.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_d836f0a5b0447fb6a2bdb0a6ac5f954949d3c41e.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_d836f0a5b0447fb6a2bdb0a6ac5f954949d3c41e.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_10628b4a811c0a925a1433d4323f78c7017dbbe4.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_10628b4a811c0a925a1433d4323f78c7017dbbe4.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_6422d297347258086b389e3d5d9c0e0c698312e4.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_6422d297347258086b389e3d5d9c0e0c698312e4.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_a3ddf22cda3bd722df77dbdd58dbec393906b654.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_a3ddf22cda3bd722df77dbdd58dbec393906b654.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_30aeedc47e731232ade368831a598d6545346f70.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_30aeedc47e731232ade368831a598d6545346f70.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_64d942a86eb527ac817f30cc04406796860a6fc1.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_64d942a86eb527ac817f30cc04406796860a6fc1.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_37f15ea893ec1fa7c9e73106f512e98161bac61b.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_37f15ea893ec1fa7c9e73106f512e98161bac61b.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_5e327e4cfc49d8137f8014e728eae3c0e6be2dca.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_5e327e4cfc49d8137f8014e728eae3c0e6be2dca.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_980472fb4f4860639155880938b6ec292a0648c4.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_980472fb4f4860639155880938b6ec292a0648c4.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_f79d2066dfaf32bbe87868d36db4845f771eddbd.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_f79d2066dfaf32bbe87868d36db4845f771eddbd.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 13,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_4ff3fe6e9555052aaa076866407b0ba68fe73132.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_4ff3fe6e9555052aaa076866407b0ba68fe73132.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 14,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_dee23745da417d2ceb0b16d9238bddbf5e227138.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_dee23745da417d2ceb0b16d9238bddbf5e227138.1920x1080.jpg?t=1574808568"
            },
            {
                "id": 15,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_f6f4c727570d753b2b5d8da6af4e0c38fe489059.600x338.jpg?t=1574808568",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/413150/ss_f6f4c727570d753b2b5d8da6af4e0c38fe489059.1920x1080.jpg?t=1574808568"
            }
        ],
        "movies": [
            {
                "id": 256660296,
                "name": "Stardew Valley Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256660296/movie.293x165.jpg?t=1454099186",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256660296/movie480.webm?t=1454099186",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256660296/movie_max.webm?t=1454099186"
                },
                "highlight": true
            }
        ],
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/413150/page_bg_generated_v6b.jpg?t=1574808568",
    },
    {
        "steam_appid": 310950,
        "name": "Street Fighter V",
        "price": 399.00,
        "img": "https://gpstatic.com/acache/28/83/6/de/packshot-298bea2d798adac6dd68500d2c106afc.jpg",
        "detailed_description": "<h1>STREET FIGHTER™ V: CHAMPION EDITION</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/SFV-CE_Endslate_NOW_STEAM_v002_616px.png?t=1581657666' ><br><br>Rule the ring with Street Fighter V: Champion Edition, the most robust version of the acclaimed fighting game! Choose from 40 diverse fighters, 34 dynamic stages and over 200 stylish costumes as you fight your way through a variety of exciting single-player and multi-player modes.<br><br>The Street Fighter V: Champion Edition contains the content from previous versions of Street Fighter V, including each character, stage and costume released after Street Fighter V: Arcade Edition. READY? FIGHT!<br><br>*Content excludes Fighting Chance costumes, brand collaboration costumes and Capcom Pro Tour DLC.</p><br><h1>STREET FIGHTER™ V</h1><p><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/SFV_Vanilla_STEAM_Banner_R_v001.png?t=1581657666' ><br><br>Experience the intensity of head-to-head battle with STREET FIGHTER V! Choose from 16 iconic characters, each with their own personal story and unique training challenges, then battle against friends online or offline with a robust variety of match options.<br><br>Earn Fight Money in Ranked Matches, play for fun in Casual Matches or invite friends into a Battle Lounge and see who comes out on top! PlayStation 4 and Steam players can also play against each other thanks to cross-play compatibility!</p><br><h1>About the Game</h1>Experience the intensity of head-to-head battle with Street Fighter™ V! Choose from 16 iconic characters, each with their own personal story and unique training challenges, then battle against friends online or offline with a robust variety of match options.<br><br>Earn Fight Money in Ranked Matches, play for fun in Casual Matches or invite friends into a Battle Lounge and see who comes out on top! PlayStation 4 and Steam players can also play against each other thanks to cross-play compatibility!<br> <br>This version of Street Fighter V displays the “Arcade Edition” title screen and includes Arcade Mode, Team Battle Mode and the online-enabled Extra Battle Mode, where you can earn rewards, XP and Fight Money! Fight Money can be used to purchase additional characters, costumes, stages and more! <br> <br>Download the cinematic story “A Shadow Falls” today for FREE! M. Bison deploys seven Black Moons into orbit, granting him unimaginable power as the earth falls into darkness.<br><br><br>Street Fighter V: Champion Edition is the ultimate pack that includes all content (excluding Fighting Chance costumes, brand collaboration costumes and Capcom Pro Tour DLC) from both the original release and Street Fighter V: Arcade Edition. It also includes each character, stage and costume that released after Arcade Edition. That means 40 characters, 34 stages and over 200 costumes!<br><br>V-Trigger 1 and 2<br>Release powerful techniques and exclusive moves by activating V-Trigger 1 or 2 with the character of your choice! Do huge bursts of damage or make that much needed comeback win by pressing hard punch and hard kick simultaneously while your V-Gauge is fully stocked.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/Ryu-V-Trigger-2_V2.gif?t=1581657666' ><br><br>V-Skill 1 and 2<br>Fundamentals win matches and V-Skills are the building blocks of victory. Each V-Skill grants your character a unique move and/or ability that will help fill up your V-Gauge.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/ChunLi-vs-Nash2_V2.gif?t=1581657666' ><br><br>V-Reversal<br>Put a stop to your opponent’s offense by utilizing V-Reversal. V-Reversal costs one V-Gauge stock, but is very much worth it as it can potentially give you breathing room to create your own offensive. <br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/Honda-Zangief_V2.gif?t=1581657666' ><br><br>Special Attacks and EX Special Attacks<br>A staple of Street Fighter, Special Attacks are the key to a solid strategy in Street Fighter V: Champion Edition. Use various Special Attacks to zone out opponents, impose your will, and build up your Critical Gauge for even more damage. EX Special Attacks are more powerful versions of Special Attacks at the cost of one Critical Gauge bar.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/LauraFireball_V2.gif?t=1581657666' ><br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/LauraEXSpecial_V2.gif?t=1581657666' ><br><br>Critical Art<br>The most highly damaging attack in Street Fighter V: Champion Edition is called the Critical Art. A Critical Art can be performed as long as the Critical Gauge is fully stocked.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/310950/extras/Gill_V2.gif?t=1581657666' >",
        "supported_languages": "English<strong>*</strong>, French, Italian, German, Spanish - Spain, Arabic, Japanese<strong>*</strong>, Korean, Polish, Portuguese - Brazil, Russian, Simplified Chinese, Traditional Chinese<br><strong>*</strong>languages with full audio support",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/310950/header.jpg?t=1581657666",
        "website": "http://www.streetfighter.com/",
        "pc_requirements": {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows 7 64-bit<br></li><li><strong>Processor:</strong> Intel Core i3-4160 @ 3.60GHz<br></li><li><strong>Memory:</strong> 6 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA® GeForce® GTX 480, GTX 570, GTX 670, or better<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Sound Card:</strong> DirectX compatible soundcard or onboard chipset<br></li><li><strong>Additional Notes:</strong> Compatible with XInput and DirectInput USB devices including gamepads and arcade sticks based on Xbox 360, Xbox One, and DualShock controllers. Steam Controller also supported.</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> WINDOWS® 7, 8, 8.1, 10 (64-BIT Required)<br></li><li><strong>Processor:</strong> Intel Core i5-4690K @3.50GHz or AMD FX-9370<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> VIDIA® GeForce® GTX 960 or AMD Radeon R7 370<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Sound Card:</strong> DirectX compatible soundcard or onboard chipset<br></li><li><strong>Additional Notes:</strong> Compatible with XInput and DirectInput USB devices including gamepads and arcade sticks based on Xbox 360, Xbox One, and DualShock controllers. Steam Controller also supported.</li></ul>"
        },
        "mac_requirements": [],
        "linux_requirements": [],
        "developers": [
            "Capcom"
        ],
        "publishers": [
            "Capcom"
        ],

        "metacritic": {
            "score": 89,
            "url": "https://www.metacritic.com/game/pc/street-fighter-v-arcade-edition?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "1",
                "description": "Action"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_592728c4d9cd25645dcd5eae866d83e966db2246.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_592728c4d9cd25645dcd5eae866d83e966db2246.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_881d16ef1e10f152625bd1b0d785f278f74ccb4f.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_881d16ef1e10f152625bd1b0d785f278f74ccb4f.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_6b7b8e703328f5a12fae77f42a513c8cc74006bc.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_6b7b8e703328f5a12fae77f42a513c8cc74006bc.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_30dc3cc5100e21e48be0145f27323e1559ac91e8.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_30dc3cc5100e21e48be0145f27323e1559ac91e8.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_c0520de6df18fef2058241ffd78123738d481903.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_c0520de6df18fef2058241ffd78123738d481903.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_9b32051c92ec58fb072092b26b006094b587fa65.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_9b32051c92ec58fb072092b26b006094b587fa65.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_133d7cefd5f2f582e95336162520d862a88670b2.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_133d7cefd5f2f582e95336162520d862a88670b2.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 7,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_9d85b0569ffc147d929680234b096d1debf81e32.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_9d85b0569ffc147d929680234b096d1debf81e32.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 8,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_3c3560ccc5f8cf39e50a0f77379304fb132d3f0e.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_3c3560ccc5f8cf39e50a0f77379304fb132d3f0e.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 9,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_365acebae55ada83d43b04597dc872d6819e6b00.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_365acebae55ada83d43b04597dc872d6819e6b00.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 10,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_a9d4d4b1a0059458c552d9153fd51a90626ca79c.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_a9d4d4b1a0059458c552d9153fd51a90626ca79c.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 11,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_13756a8671febf1f47d5607f3b004d1d4cbabbee.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_13756a8671febf1f47d5607f3b004d1d4cbabbee.1920x1080.jpg?t=1581657666"
            },
            {
                "id": 12,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_6655641b43f9a014979c27bdb0a411da6e0ba6dc.600x338.jpg?t=1581657666",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/310950/ss_6655641b43f9a014979c27bdb0a411da6e0ba6dc.1920x1080.jpg?t=1581657666"
            },
        ],
        "movies": [
            {
                "id": 256775169,
                "name": "SFVCE_LT_ESRB_Steam_v2",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256775169/movie.293x165.jpg?t=1581656676",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256775169/movie480.webm?t=1581656676",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256775169/movie_max.webm?t=1581656676"
                },
                "highlight": true
            },
            {
                "id": 256770134,
                "name": "SFV CE Part-2 (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256770134/movie.293x165.jpg?t=1576518078",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256770134/movie480.webm?t=1576518078",
                    "max": "http://steamcdn-a.akamaihd.net/steam//apps/256770134/movie_max.webm?t=1576518078"
                },
                "highlight": true
            },
            {
                "id": 256770133,
                "name": "Seth (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256770133/movie.293x165.jpg?t=1576518089",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256770133/movie480.webm?t=1576518089",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256770133/movie_max.webm?t=1576518089"
                },
                "highlight": true
            },
            {
                "id": 256767631,
                "name": "SFV CE Part-1 (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256767631/movie.293x165.jpg?t=1574128211",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256767631/movie480.webm?t=1574128211",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256767631/movie_max.webm?t=1574128211"
                },
                "highlight": true
            },
            {
                "id": 256767629,
                "name": "Gill (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256767629/movie.293x165.jpg?t=1574128221",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256767629/movie480.webm?t=1574128221",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256767629/movie_max.webm?t=1574128221"
                },
                "highlight": true
            },
            {
                "id": 256758178,
                "name": "EVO 2019 ESRB",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256758178/movie.293x165.jpg?t=1564978524",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256758178/movie480.webm?t=1564978524",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256758178/movie_max.webm?t=1564978524"
                },
                "highlight": true
            },
            {
                "id": 256738584,
                "name": "Kage (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256738584/movie.293x165.jpg?t=1545015362",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256738584/movie480.webm?t=1545015362",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256738584/movie_max.webm?t=1545015362"
                },
                "highlight": true
            },
            {
                "id": 256724888,
                "name": "Sagat (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256724888/movie.293x165.jpg?t=1533674988",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256724888/movie480.webm?t=1533674988",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256724888/movie_max.webm?t=1533674988"
                },
                "highlight": true
            },
            {
                "id": 256724872,
                "name": "G Trailer (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256724872/movie.293x165.jpg?t=1533674994",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256724872/movie480.webm?t=1533674994",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256724872/movie_max.webm?t=1533674994"
                },
                "highlight": true
            },
            {
                "id": 256706053,
                "name": "SFV AE Launch (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256706053/movie.293x165.jpg?t=1516128058",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256706053/movie480.webm?t=1516128058",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256706053/movie_max.webm?t=1516128058"
                },
                "highlight": true
            },
            {
                "id": 256721129,
                "name": "Vanilla Relaunch",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256721129/movie.293x165.jpg?t=1530037033",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256721129/movie480.webm?t=1530037033",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256721129/movie_max.webm?t=1530037033"
                },
                "highlight": true
            },
            {
                "id": 256702126,
                "name": "SFV AE Trailer (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256702126/movie.293x165.jpg?t=1543854439",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256702126/movie480.webm?t=1543854439",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256702126/movie_max.webm?t=1543854439"
                },
                "highlight": true
            },
            {
                "id": 256656557,
                "name": "Karin Trailer (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256656557/movie.293x165.jpg?t=1455160854",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256656557/movie480.webm?t=1455160854",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256656557/movie_max.webm?t=1455160854"
                },
                "highlight": false
            },
            {
                "id": 256655987,
                "name": "Necalli Trailer (Evo) (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256655987/movie.293x165.jpg?t=1455160913",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256655987/movie480.webm?t=1455160913",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256655987/movie_max.webm?t=1455160913"
                },
                "highlight": false
            },
            {
                "id": 256655988,
                "name": "Vega Trailer (ESRB)",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256655988/movie.293x165.jpg?t=1455160906",
                "webm": {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256655988/movie480.webm?t=1455160906",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256655988/movie_max.webm?t=1455160906"
                },
                "highlight": false
            },

        ],
        "release_date": {
            "coming_soon": false,
            "date": "16 Feb, 2016"
        },
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/310950/page_bg_generated_v6b.jpg?t=1581657666",
    },
    {
        "steam_appid": 346110,
        "name": "ARK: Survival Evolved",
        "price": 579.00,
        "img":
            "https://cdn-products.eneba.com/resized-products/abo2bjo9wt1yb0hnprml_390x400_1x-0.jpg",
        "detailed_description":
            '<h1>Valguero - Free Expansion Map</h1><p><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/ARK_VALG_LargeCapsule_Promo_467x181.jpg?t=1581439731" ><br><br>Explore new heights and hidden depths; whether it\u2019s creating your foundations in the White Cliffs or unearthing the secrets of the Aberration Trench, Valguero offers a new experience in the ARK universe. With new biomes, challenging dungeon bosses, and mysterious ruins to discover, there\u2019s always something exciting \u2014 and dangerous \u2014 on Valguero.<br><br><a href="https://store.steampowered.com/app/1100810/Valguero__ARK_Expansion_Map/" target="_blank" rel="noreferrer"  id="dynamiclink_0" >https://store.steampowered.com/app/1100810/Valguero__ARK_Expansion_Map/</a></p><br><h1>About the Game</h1>As a man or woman stranded naked, freezing and starving on the shores of a mysterious island called ARK, you must hunt, harvest resources, craft items, grow crops, research technologies, and build shelters to withstand the elements. Use your cunning and resources to kill or tame &amp; breed the leviathan dinosaurs and other primeval creatures roaming the land, and team up with or prey on hundreds of other players to survive, dominate... and escape!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/TameTrainBreedRide.png?t=1581439731" ><br><br>Dinosaurs, Creatures, &amp; Breeding! -- over 100+ creatures can be tamed using a challenging capture-&amp;-affinity process, involving weakening a feral creature to knock it unconscious, and then nursing it back to health with appropriate food. Once tamed, you can issue commands to your tames, which it may follow depending on how well you\u2019ve tamed and trained it. Tames, which can continue to level-up and consume food, can also carry Inventory and Equipment such as Armor, carry prey back to your settlement depending on their strength, and larger tames can be ridden and directly controlled! Fly a Pterodactyl over the snow-capped mountains, lift allies over enemy walls, race through the jungle with a pack of Raptors, tromp through an enemy base along a gigantic brontosaurus, or chase down prey on the back of a raging T-Rex! Take part in a dynamic ecosystem life-cycle with its own predator &amp; prey hierarchies, where you are just one creature among many species struggling for dominance and survival. Tames can also be mated with the opposite gender, to selectively breed successive generations using a trait system based on recombinant genetic inheritance. This process includes both egg-based incubation and mammalian gestation lifecycles! Or put more simply, raise babies!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/FoodWaterTemp.png?t=1581439731" ><br><br>You must eat and drink to survive, with different kinds of plants &amp; meat having different nutritional properties, including human meat. Ensuring a supply of fresh water to your home and inventory is a pressing concern. All physical actions come at a cost of food and water, long-distance travel is fraught with subsistence peril! Inventory weight makes you move slower, and the day/night cycle along with randomized weather patterns add another layer of challenge by altering the temperature of the environment, causing you to hunger or thirst more quickly. Build a fire or shelter, and craft a large variety of customizable clothing &amp; armors, to help protect yourself against locational damage &amp; extreme temperatures using the dynamic indoor/outdoor insulation calculation system!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/HarvestBuildPaint.png?t=1581439731" ><br><br> By chopping down forests full of trees and mining metal and other precious resources, you can craft the parts to build massive multi-leveled structures composed of complex snap-linked parts, including ramps, beams, pillars, windows, doors, gates, remote gates, trapdoors, water pipes, faucets, generators, wires and all manner of electrical devices, and ladders among many other types. Structures have a load system to fall apart if enough support has been destroyed, so reinforcing your buildings is important. All structures and items can be painted to customize the look of your home, as well as placing dynamically per-pixel paintable signs, textual billboards, and other decorative objects. Shelter reduces the extremes of weather and provides security for yourself and your stash! Weapons, clothing &amp; armor gear can also be painted to express your own visual style.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/PlantFarmGrow.png?t=1581439731" ><br><br>Pick seeds from the wild vegetation around you, plant them in plots that you lay down, water them and nurture them with fertilizer (everything poops after consuming calories, which can then be composted, and some fertilizer is better than others). Tend to your crops and they will grow to produce delicious and rare fruits, which can also be used to cook a plethora of logical recipes and make useful tonics! Explore to find the rarest of plant seeds that have the most powerful properties! Vegetarians &amp; vegans can flourish, and it will be possible to master and conquer the ARK in a non-violent manner!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/Summon.png?t=1581439731" ><br><br>By bringing sufficient rare sacrificial items to special Summon locations, you can capture the attention of the one of the ARK\u2019s god-like mythical creatures, who will arrive for battle. These gargantuan monstrosities provide an end-game goal for the most experienced groups of players and their armies of tames, and will yield extremely valuable progression items if they are defeated.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/Tribes.png?t=1581439731" ><br><br>Create a Tribe and add your friends to it, and all your tames can be commanded by and allied to anyone in your Tribe. Your Tribe will also be able to respawn at any of your home spawn points. Promote members to Tribe Admins to reduce the burden of management. Distribute key items and pass-codes to provide access your shared village!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/RPG.png?t=1581439731" ><br><br>All items are crafted from Blueprints that have variable statistics and qualities, and require corresponding resources. More remote and harsh locales across the ARK tend to have better resources, including the tallest mountains, darkest caves, and depths of the ocean! Level-Up your player character by gaining experience through performance actions, Level-Up your tames, and learn new &quot;Engrams&quot; to be able to craft Items from memory without the use of blueprints, even if you die! Customize the underlying physical look of your character with hair, eye, and skin tones, along with an array of body proportion modifiers. As you explore the vast ARK, you\'ll find clues left by other Survivors who have made the ARK their home in ages past, in the form of collectible detailed 3D &quot;Explorer Notes&quot;. By uncovering all of these, you can begin to piece together the true nature of the ARK, and discover its purpose!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/HardcoreMechanics.png?t=1581439731" ><br><br>Everything you craft has durability and will wear-out from extended use if not repaired, and when you leave the game, your character remains sleeping in the persistent world. Your inventory physically exists in boxes or on your character in the world. Everything can be looted &amp; stolen, so to achieve security you must build-up, team-up, or have tames to guard your stash. Death is permanent, and you can even knock out, capture, and force-feed other players to use them for your own purposes, such as extracting their blood to for transfusions, harvesting their fecal matter to use as fertilizer, or using them as food for your carnivorous tames!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/ExploreAndDiscover.png?t=1581439731" ><br><br>The mysterious ARK is a formidable and imposing environment, composed of many natural and unnatural structures, above-ground, below-ground, and underwater. By fully exploring its secrets, you\u2019ll find the most exotic procedurally randomized creatures and rare blueprints. Also to be found are Explorer Notes that are dynamically updated into the game, written by previous human denizens of the ARK from across the millennia, creatively detailing the creatures and backstory of the ARK and its creatures. Fully develop your in-game ARK-map through exploration, write custom points of interest onto it, and craft a Compass or GPS coordinates to aid exploring with other players, whom you can communicate with via proximity text &amp; voice chat, or long-distance radio. Construct &amp; draw in-game signs for other players to help them or lead them astray...  And yet.. how do you ultimately challenge the Creators and Conquer the ARK? A definitive end-game is planned.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/LargeWorld.png?t=1581439731" ><br><br>On the 100+ player servers, your character, everything you built, and your tames, stay in-game even when you leave. You can even physically travel your character and items between the network of ARK\'s by accessing the Obelisks and uploading (or downloading) your data from the Steam Economy! A galaxy of ARKs, each slightly different than the previous, to leave your mark on and conquer, one at a time -- special official ARKs will be unveiled on the World-map for limited times in singular themed events with corresponding limited-run items! Furthermore, you can now design or randomize your own unique \'Procedurally Generated ARKs\', for infinite replayability and endless surprises.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/RobustSteamWorkshop.png?t=1581439731" ><br><br>You can play single-player local games, and bring your character and items between unofficial player-hosted servers, back and forth from singleplayer to multiplayer. Mod the game, with full Steam Workshop support and customized Unreal Engine 4 editor. See how we built our ARK using our maps and assets as an example. Host your own server and configure your ARK precisely to your liking. We want to see what you create!<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/346110/extras/HighEndNextGenVisuals.png?t=1581439731" ><br><br>The over-the-top hyper real imagery of the ARK its creatures is brought to expressive life using a highly-customized Unreal Engine 4, with fully dynamic lighting &amp; global illumination, weather systems (rain, fog, snow, etc) &amp; true-to-life volumetric cloud simulation, and the latest in advanced DirectX11 and DirectX12 rendering techniques. Music by award-winning composer of &quot;Ori and the Blind Forest&quot;, Gareth Coker!',
        "supported_languages":
            "English<strong>*</strong>, French<strong>*</strong>, Italian<strong>*</strong>, German<strong>*</strong>, Spanish - Spain<strong>*</strong>, Russian<strong>*</strong>, Portuguese - Brazil<strong>*</strong>, Simplified Chinese<strong>*</strong>, Czech<strong>*</strong>, Danish<strong>*</strong>, Dutch<strong>*</strong>, Hungarian<strong>*</strong>, Polish<strong>*</strong>, Swedish<strong>*</strong>, Thai<strong>*</strong>, Traditional Chinese<strong>*</strong>, Turkish<strong>*</strong>, Ukrainian<strong>*</strong>, Finnish<strong>*</strong>, Japanese<strong>*</strong>, Korean<strong>*</strong><br><strong>*</strong>languages with full audio support",
        "header_image":
            "https://steamcdn-a.akamaihd.net/steam/apps/346110/header_alt_assets_9.jpg?t=1581439731",
        "website": "http://www.playark.com",
        "pc_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li><strong>OS:</strong> Windows 7/8.1/10 (64-bit versions)<br></li><li><strong>Processor:</strong> Intel Core i5-2400/AMD FX-8320 or better<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GTX 670 2GB/AMD Radeon HD 7870 2GB or better<br></li><li><strong>DirectX:</strong> Version 10<br></li><li><strong>Storage:</strong> 60 GB available space<br></li><li><strong>Additional Notes:</strong> Requires broadband internet connection for multiplayer</li></ul>'
        },
        "mac_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li><strong>OS:</strong> OSX 10.9 or Higher<br></li><li><strong>Processor:</strong> 2 GHz Equivalent CPU<br></li><li><strong>Memory:</strong> 4000 MB RAM<br></li><li><strong>Graphics:</strong> OpenGL 3 Compatible GPU with 1GB Video RAM<br></li><li><strong>Storage:</strong> 20000 MB available space</li></ul>'
        },
        "linux_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li><strong>OS:</strong> Ubuntu Equivalent Distro<br></li><li><strong>Processor:</strong> 2 GHz 64-bit CPU<br></li><li><strong>Memory:</strong> 4000 MB RAM<br></li><li><strong>Graphics:</strong> OpenGL 3 Compatible GPU with 1GB Video RAM<br></li><li><strong>Storage:</strong> 20000 MB available space</li></ul>'
        },
        "developers": [
            "Studio Wildcard",
            "Instinct Games",
            "Efecto Studios",
            "Virtual Basement LLC"
        ],
        "publishers": ["Studio Wildcard"],
        "metacritic": {
            "score": 70,
            "url":
                "https://www.metacritic.com/game/pc/ark-survival-evolved?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            { "id": "1", "description": "Action" },
            { "id": "25", "description": "Adventure" },
            { "id": "23", "description": "Indie" },
            { "id": "29", "description": "Massively Multiplayer" },
            { "id": "3", "description": "RPG" }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_2fd997a2f7151cb2187043a1f41589cc6a9ebf3a.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_2fd997a2f7151cb2187043a1f41589cc6a9ebf3a.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 1,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_01cbef83fe28d64ee5a3d39a86043fb1e49abd31.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_01cbef83fe28d64ee5a3d39a86043fb1e49abd31.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 2,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_164a92a53f9bcbb728b391fc0719f9769c2e1249.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_164a92a53f9bcbb728b391fc0719f9769c2e1249.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 3,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_46778c08a1a5ac5bdbaf8a5bf844fa666f66a14b.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_46778c08a1a5ac5bdbaf8a5bf844fa666f66a14b.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 4,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_ffe9f0e2e23892f3bb6188e5a3eed0f60a08baf4.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_ffe9f0e2e23892f3bb6188e5a3eed0f60a08baf4.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 5,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_cada382a940c062a1a5244db8da1326de55ddeae.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_cada382a940c062a1a5244db8da1326de55ddeae.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 6,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_1a7f5508e5384a759ccc83fa484db04513213698.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_1a7f5508e5384a759ccc83fa484db04513213698.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 7,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_ffd48da603fa700d10738ae4ee44ce2b9113cb64.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_ffd48da603fa700d10738ae4ee44ce2b9113cb64.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 8,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_7f9c3429b86d65cd63beed4597a23148d7cadf08.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_7f9c3429b86d65cd63beed4597a23148d7cadf08.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 9,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_fb806797fb216cad733dc70a3fc732134442b1e6.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_fb806797fb216cad733dc70a3fc732134442b1e6.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 10,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_f72af9a68df60b7299b13f07b8165c71b8eac2aa.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_f72af9a68df60b7299b13f07b8165c71b8eac2aa.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 11,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_1e494e45758b4dd3323b1c359047aaaa5be101d5.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_1e494e45758b4dd3323b1c359047aaaa5be101d5.1920x1080.jpg?t=1581439731"
            },
            {
                "id": 12,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_5d60317fff984fcd8d8a7bee9c51f8e0729f7e8f.600x338.jpg?t=1581439731",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/346110/ss_5d60317fff984fcd8d8a7bee9c51f8e0729f7e8f.1920x1080.jpg?t=1581439731"
            },
        ],
        "movies": [
            {
                "id": 256774822,
                "name": "ARK: Love Evolved (2020) Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256774822/movie.293x165.jpg?t=1581439721",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256774822/movie480.webm?t=1581439721",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256774822/movie_max.webm?t=1581439721"
                },
                "highlight": true
            },
            {
                "id": 256753744,
                "name": "Valguero_Announce Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256753744/movie.293x165.jpg?t=1560880101",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256753744/movie480.webm?t=1560880101",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256753744/movie_max.webm?t=1560880101"
                },
                "highlight": true
            },
            {
                "id": 256744539,
                "name": "ARK: Homestead Update Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256744539/movie.293x165.jpg?t=1551899542",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256744539/movie480.webm?t=1551899542",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256744539/movie_max.webm?t=1551899542"
                },
                "highlight": true
            },
            {
                "id": 256693939,
                "name": "ARK: Survival Evolved Global Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256693939/movie.293x165.jpg?t=1508277163",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256693939/movie480.webm?t=1508277163",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256693939/movie_max.webm?t=1508277163"
                },
                "highlight": true
            },
            {
                "id": 256655397,
                "name": "ARK Survival Evolved Gamescom 2015 Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256655397/movie.293x165.jpg?t=1503989702",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256655397/movie480.webm?t=1503989702",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256655397/movie_max.webm?t=1503989702"
                },
                "highlight": true
            },
            {
                "id": 256666035,
                "name": "ARK Redwood Biome and Spotlight: Titanosaur",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256666035/movie.293x165.jpg?t=1503989709",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256666035/movie480.webm?t=1503989709",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256666035/movie_max.webm?t=1503989709"
                },
                "highlight": true
            },
            {
                "id": 256656850,
                "name": "ARK New Biomes Expansion Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256656850/movie.293x165.jpg?t=1503989720",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256656850/movie480.webm?t=1503989720",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256656850/movie_max.webm?t=1503989720"
                },
                "highlight": true
            },
            {
                "id": 2039177,
                "name": "ARK: Survival Evolved Announcement Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/2039177/movie.293x165.jpg?t=1503989726",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/2039177/movie480.webm?t=1503989726",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/2039177/movie_max.webm?t=1503989726"
                },
                "highlight": true
            }
        ],
        "release_date": { "coming_soon": false, "date": "28 Aug, 2017" },
        "background":
            "https://steamcdn-a.akamaihd.net/steam/apps/346110/page_bg_generated_v6b.jpg?t=1581439731"
    },
    {
        "steam_appid": 381210,
        "name": "Dead by Daylight",
        "price": 369.00,
        "img": "https://static-cdn.jtvnw.net/ttv-boxart/Dead%20by%20Daylight.jpg",
        "detailed_description":
            '<h1>Digital Deluxe Edition</h1><p>The <strong>Deluxe Edition</strong> includes the <strong>base Game</strong>, the <strong>Official Soundtrack</strong>, the <strong>Digital Art Book</strong> and<strong> two Masks for PAYDAY 2.</strong><br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/381210/extras/DBD_Deluxe_DescriptionBanner.png?t=1578696045" ></p><br><h1>About the Game</h1><img src="https://steamcdn-a.akamaihd.net/steam/apps/381210/extras/DeadbyDaylight_anime_Intro_Steam.jpg?t=1578696045" ><br><br><strong>Death Is Not an Escape.</strong><br><br>Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the savage Killer, and the other four players play as Survivors, trying to escape the Killer and avoid being caught, tortured and killed.<br> <br>Survivors play in third-person and have the advantage of better situational awareness. The Killer plays in first-person and is more focused on their prey.<br> <br>The Survivors\' goal in each encounter is to escape the Killing Ground without getting caught by the Killer - something that sounds easier than it is, especially when the environment changes every time you play.<br><br>More information about the game is available at <a href="https://steamcommunity.com/linkfilter/?url=http://www.deadbydaylight.com" target="_blank" rel="noopener"  >http://www.deadbydaylight.com</a><h2 class="bb_tag">Key Features</h2>\u2022\t<strong>Survive Together\u2026 Or Not</strong> - Survivors can either cooperate with the others or be selfish. Your chance of survival will vary depending on whether you work together as a team or if you go at it alone. Will you be able to outwit the Killer and escape their Killing Ground?<br><br>\u2022\t<strong>Where Am I?</strong> - Each level is procedurally generated, so you\u2019ll never know what to expect. Random spawn points mean you will never feel safe as the world and its danger change every time you play.<br><br>\u2022\t<strong>A Feast for Killers</strong> - Dead by Daylight draws from all corners of the horror world. As a Killer you can play as anything from a powerful Slasher to terrifying paranormal entities. Familiarize yourself with your Killing Grounds and master each Killer\u2019s unique power to be able to hunt, catch and sacrifice your victims.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/381210/extras/DBD_HillB_Trapper_Wraith_anime_Steam.jpg?t=1578696045" ><br><br>\u2022\t<strong>Deeper and Deeper</strong> - Each Killer and Survivor has their own deep progression system and plenty of unlockables that can be customized to fit your own personal strategy. Experience, skills and understanding of the environment are key to being able to hunt or outwit the Killer.<br><br>\u2022\t<strong>Real People, Real Fear</strong> - The procedural levels and real human reactions to pure horror makes each game session an unexpected scenario. You will never be able to tell how it\u2019s going to turn out. Ambience, music, and chilling environments combine into a terrifying experience. With enough time, you might even discover what\u2019s hiding in the fog.<br><br><img src="https://steamcdn-a.akamaihd.net/steam/apps/381210/extras/DBD_desc_1.jpg?t=1578696045" >',
        "short_description":
            "Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the savage Killer, and the other four players play as Survivors, trying to escape the Killer and avoid being caught and killed.",
        "supported_languages":
            "English<strong>*</strong>, French<strong>*</strong>, Italian, German, Spanish - Spain, Russian, Simplified Chinese, Portuguese - Brazil, Thai, Traditional Chinese, Japanese<strong>*</strong>, Korean, Polish, Dutch, Spanish - Latin America, Swedish, Turkish<br><strong>*</strong>languages with full audio support",
        "header_image":
            "https://steamcdn-a.akamaihd.net/steam/apps/381210/header.jpg?t=1578696045",
        "website": "http://www.deadbydaylight.com",
        "pc_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> 64-bit Operating Systems (Windows 7, Windows 8.1)<br></li><li><strong>Processor:</strong> Intel Core i3-4170 or AMD FX-8120<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> DX11 Compatible GeForce GTX 460 1GB or AMD HD 6850 1GB<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 25 GB available space<br></li><li><strong>Sound Card:</strong> DX11 compatible<br></li><li><strong>Additional Notes:</strong> With these requirements, it is recommended that the game is played on Low quality settings.</li></ul>',
            "recommended":
                '<strong>Recommended:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system<br></li><li><strong>OS:</strong> 64-bit Operating Systems (Windows 7, Windows 8.1 or above)<br></li><li><strong>Processor:</strong> Intel Core i3-4170 or AMD FX-8300 or higher<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> DX11 Compatible GeForce 760 or AMD HD 8800 or higher with 4GB of RAM<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 25 GB available space<br></li><li><strong>Sound Card:</strong> DX11 compatible</li></ul>'
        },
        "mac_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system</li></ul>',
            "recommended":
                '<strong>Recommended:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system</li></ul>'
        },
        "linux_requirements": {
            "minimum":
                '<strong>Minimum:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system</li></ul>',
            "recommended":
                '<strong>Recommended:</strong><br><ul class="bb_ul"><li>Requires a 64-bit processor and operating system</li></ul>'
        },
        "metacritic": {
            "score": 71,
            "url":
                "https://www.metacritic.com/game/pc/dead-by-daylight?ftag=MCD-06-10aaa1f"
        },
        "genres": [{ "id": "1", "description": "Action" }],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_5d66c65ae32b7dd265c5eccf0f38ae3e21f54351.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_5d66c65ae32b7dd265c5eccf0f38ae3e21f54351.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 1,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_8c5b75a9ee6c69f18eec714759e3e7b84aa906ed.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_8c5b75a9ee6c69f18eec714759e3e7b84aa906ed.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 2,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_002a2dcb76e82db9878fcc8416daa279098426a4.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_002a2dcb76e82db9878fcc8416daa279098426a4.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 3,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_f703adba1cf8cdde1fbb2ec5d65b5eaadb3b866f.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_f703adba1cf8cdde1fbb2ec5d65b5eaadb3b866f.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 4,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_888c1628b8a8e10ef2fcbc6ce5bebf6ee381111c.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_888c1628b8a8e10ef2fcbc6ce5bebf6ee381111c.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 5,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_2db0a7f1ae6e9caeed7dfa7cc5ce282391530008.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_2db0a7f1ae6e9caeed7dfa7cc5ce282391530008.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 6,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_dcb60e6b314c6adef4c0f753453790c0f0eed4ca.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_dcb60e6b314c6adef4c0f753453790c0f0eed4ca.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 7,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_d0294c48a69035f9031f5fb27e728479afeeb507.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_d0294c48a69035f9031f5fb27e728479afeeb507.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 8,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_9566e860ebefe1f8c5a3147818e2447ae906f6da.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_9566e860ebefe1f8c5a3147818e2447ae906f6da.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 9,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_83c23206f3282f5e53c0ebb481ed4a1c4fba3d2d.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_83c23206f3282f5e53c0ebb481ed4a1c4fba3d2d.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 10,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_7f70fd7ab9ac2209fad078412da322819bc04164.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_7f70fd7ab9ac2209fad078412da322819bc04164.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 11,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_34cce59e8791c58b617328131103f3af382437f7.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_34cce59e8791c58b617328131103f3af382437f7.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 12,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_e1537715a4c9c970dbea742af3a57051eb34d675.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_e1537715a4c9c970dbea742af3a57051eb34d675.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 13,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_30da4e3140e946dd3fd65531f568f92de0a3b562.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_30da4e3140e946dd3fd65531f568f92de0a3b562.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 14,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_13ed090784f064fe91b1e3de7d9036e033b9ebe4.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_13ed090784f064fe91b1e3de7d9036e033b9ebe4.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 15,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_afda51008ba16a4149ab5a8aa613f13b37e44dec.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_afda51008ba16a4149ab5a8aa613f13b37e44dec.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 16,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_ccc86e5e278ec1820a2bbe120301ca06c1e65e78.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_ccc86e5e278ec1820a2bbe120301ca06c1e65e78.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 17,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_787084418a59802a891e235b1adff75c7e5d73c6.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_787084418a59802a891e235b1adff75c7e5d73c6.1920x1080.jpg?t=1578696045"
            },
            {
                "id": 18,
                "path_thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_7ef4500ebefe4cf81c8259147be658176d95c137.600x338.jpg?t=1578696045",
                "path_full":
                    "https://steamcdn-a.akamaihd.net/steam/apps/381210/ss_7ef4500ebefe4cf81c8259147be658176d95c137.1920x1080.jpg?t=1578696045"
            }
        ],
        "movies": [
            {
                "id": 256768727,
                "name": "Dead by Daylight: Cursed Legacy - Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256768727/movie.293x165.jpg?t=1575392826",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256768727/movie480.webm?t=1575392826",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256768727/movie_max.webm?t=1575392826"
                },
                "highlight": true
            },
            {
                "id": 256686761,
                "name": "Dead by Daylight - Introduction Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256686761/movie.293x165.jpg?t=1496950461",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256686761/movie480.webm?t=1496950461",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256686761/movie_max.webm?t=1496950461"
                },
                "highlight": true
            },
            {
                "id": 256759815,
                "name": "Dead by Daylight: STRANGER THINGS - Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256759815/movie.293x165.jpg?t=1566496145",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256759815/movie480.webm?t=1566496145",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256759815/movie_max.webm?t=1566496145"
                },
                "highlight": true
            },
            {
                "id": 256753745,
                "name": "Dead by Daylight: Ghost Face - Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256753745/movie.293x165.jpg?t=1560879997",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256753745/movie480.webm?t=1560879997",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256753745/movie_max.webm?t=1560879997"
                },
                "highlight": true
            },
            {
                "id": 256745746,
                "name": "Dead by Daylight: Demise of the Faithful - Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256745746/movie.293x165.jpg?t=1553014000",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256745746/movie480.webm?t=1553014000",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256745746/movie_max.webm?t=1553014000"
                },
                "highlight": true
            },
            {
                "id": 256738200,
                "name": "Dead by Daylight: Darkness Among Us - Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256738200/movie.293x165.jpg?t=1544637513",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256738200/movie480.webm?t=1544637513",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256738200/movie_max.webm?t=1544637513"
                },
                "highlight": true
            },
            {
                "id": 256738199,
                "name": "Dead by Daylight: Shattered Bloodline - Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256738199/movie.293x165.jpg?t=1544637522",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256738199/movie480.webm?t=1544637522",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256738199/movie_max.webm?t=1544637522"
                },
                "highlight": true
            },
            {
                "id": 256719713,
                "name": "Dead by Daylight: Curtain Call - Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256719713/movie.293x165.jpg?t=1528817830",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256719713/movie480.webm?t=1528817830",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256719713/movie_max.webm?t=1528817830"
                },
                "highlight": true
            },
            {
                "id": 256706771,
                "name": "Dead by Daylight - the Saw Chapter Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256706771/movie.293x165.jpg?t=1516731332",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256706771/movie480.webm?t=1516731332",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256706771/movie_max.webm?t=1516731332"
                },
                "highlight": true
            },
            {
                "id": 256699457,
                "name": "Dead by Daylight - A Nightmare on Elm Street\u2122 Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256699457/movie.293x165.jpg?t=1509046854",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256699457/movie480.webm?t=1509046854",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256699457/movie_max.webm?t=1509046854"
                },
                "highlight": true
            },
            {
                "id": 256695558,
                "name": "Dead by Daylight - LEATHERFACE Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256695558/movie.293x165.jpg?t=1505485490",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256695558/movie480.webm?t=1505485490",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256695558/movie_max.webm?t=1505485490"
                },
                "highlight": true
            },
            {
                "id": 256691480,
                "name": "Dead by Daylight - A Lullaby for the Dark Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256691480/movie.293x165.jpg?t=1501189076",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256691480/movie480.webm?t=1501189076",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256691480/movie_max.webm?t=1501189076"
                },
                "highlight": true
            },
            {
                "id": 256665589,
                "name": "Dead by Daylight: Launch Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256665589/movie.293x165.jpg?t=1465863829",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256665589/movie480.webm?t=1465863829",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256665589/movie_max.webm?t=1465863829"
                },
                "highlight": true
            },
            {
                "id": 256684852,
                "name": "Dead by Daylight - Spark of Madness Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256684852/movie.293x165.jpg?t=1494602245",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256684852/movie480.webm?t=1494602245",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256684852/movie_max.webm?t=1494602245"
                },
                "highlight": true
            },
            {
                "id": 256680993,
                "name": "Dead by Daylight - Left Behind Trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256680993/movie.293x165.jpg?t=1490708363",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256680993/movie480.webm?t=1490708363",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256680993/movie_max.webm?t=1490708363"
                },
                "highlight": true
            },
            {
                "id": 256676142,
                "name": "Dead by Daylight - Chapter III trailer",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256676142/movie.293x165.jpg?t=1481318827",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256676142/movie480.webm?t=1481318827",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256676142/movie_max.webm?t=1481318827"
                },
                "highlight": true
            },
            {
                "id": 256673265,
                "name": "Dead by Daylight - The Halloween Chapter",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256673265/movie.293x165.jpg?t=1477410909",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256673265/movie480.webm?t=1477410909",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256673265/movie_max.webm?t=1477410909"
                },
                "highlight": true
            },
            {
                "id": 256661320,
                "name": "Dead By Daylight: In game footage",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256661320/movie.293x165.jpg?t=1461365295",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256661320/movie480.webm?t=1461365295",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256661320/movie_max.webm?t=1461365295"
                },
                "highlight": true
            },
            {
                "id": 256660479,
                "name": "Dead by Daylight: Dev Diary #1",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256660479/movie.293x165.jpg?t=1461365307",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256660479/movie480.webm?t=1461365307",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256660479/movie_max.webm?t=1461365307"
                },
                "highlight": true
            },
            {
                "id": 256661662,
                "name": "Dead by Daylight: Dev Diary #2",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256661662/movie.293x165.jpg?t=1461365320",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256661662/movie480.webm?t=1461365320",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256661662/movie_max.webm?t=1461365320"
                },
                "highlight": true
            },
            {
                "id": 256672052,
                "name": "Dead by Daylight: Next Chapter Teaser",
                "thumbnail":
                    "https://steamcdn-a.akamaihd.net/steam/apps/256672052/movie.293x165.jpg?t=1477410919",
                "webm": {
                    "480":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256672052/movie480.webm?t=1477410919",
                    "max":
                        "http://steamcdn-a.akamaihd.net/steam/apps/256672052/movie_max.webm?t=1477410919"
                },
                "highlight": true
            }
        ],
        "release_date": { "coming_soon": false, "date": "15 Jun, 2016" },
        "background":
            "https://steamcdn-a.akamaihd.net/steam/apps/381210/page_bg_generated_v6b.jpg?t=1578696045"
    },
    {
        "steam_appid": 424370,
        "name": "Wolcen: Lords of Mayhem",
        "price": 469.00,
        "img": "https://www.gameoverth.com/wp-content/uploads/2019/12/box1.1-22.jpg",
        "detailed_description": "You are one of the three survivors of the slaughter of Castagath. Rescued by Grand Inquisitor Heimlock, you were drafted into the Republic\u2019s Army of the Purifiers at a very young age to be trained in the military academy and become perfect soldiers against the supernatural. You also had the chance to benefit from Heimlock\u2019s occasional advice and training, which led you and your childhood friends, Valeria and Edric, to be called the \u201cChildren of Heimlock\u201d.<br>Recently, the Brotherhood of Dawn has infiltrated the Crimson Keep, a mysterious republican fortress lost among the northern deserts known as the Red wastes. While the purpose of the attack was unclear, the republican Senate voted a retaliation act against all known locations of the Brotherhood. <br><br>Led by Grand Inquisitor Heimlock himself, troops are soon deployed on the Coast of wrecks, near the city state of Stormfall, to terminate a camp of Brothers. You are, with your two childhood friends, part of operation Dawnbane, under the supervision of Justicar Ma\u00eblys.  <br><br><u>Key features:<\/u><br><br><strong>Free character development<\/strong><br>Wield a great variety of weapons and find your own playstyle thanks to their unique stances and combos. In Wolcen, there is no class, only your weapons set the rules for your skills types. <br><br><strong>Three types of resources<\/strong><br>Rage and Willpower interact with each other, using the Resource Opposition System. Stamina allows you to use a dodge-roll to avoid danger or travel faster.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/Chest.gif?t=1581614791\" ><br><strong>Item diversity<\/strong><br>Gear up according to your offensive and defensive choices with common, magical, rare and legendary items. Break the rules and unlock new possibilities with Unique items and rare affixes.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/PST.gif?t=1581614791\" ><br><strong>Rotative Passive Skill Tree<\/strong><br>Make your own path throughout the 21 sub-class sections available in the Gate of Fates to customize your passives and make them fit to your play style.<br><br><strong>Skill customization<\/strong><br>Level up your skills with your character or alternative resources to gain modifier points and create your own unique combination of skill modifiers. Change your damage type, add new functionalities, grant buffs or debuffs, change the skill\u2019s mechanics completely. The options are limitless.<br><br><strong>Strategic fights<\/strong><br>Wolcen\u2019s creatures have complex patterns including deadly skills. Pay attention to various markers and to animation anticipation to avoid lethal attacks using your dodge roll ability.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/ApoForm.gif?t=1581614791\" ><br><strong>Aspects of Apocalypse<\/strong><br>All characters can shapeshift into one of the 4 Celestial incarnations available, each of them offering 4 different skills, and one devastating ultimate.<br><br><strong>Endless replayability<\/strong><br>Improve your gear by looting or crafting, gather resources to unlock rare missions, face advanced challenges for special rewards, experiment new builds, become the greatest achiever. Whether you like to play solo or with friends, there is always something to do.<br><br><strong>A taste for beauty<\/strong><br>Using the Cryengine technology makes Wolcen an immersive and beautiful game with highly detailed armors and weapons. In addition, 8 hours of epic orchestral music will accompany you during your journey.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/Custo.gif?t=1581614791\" ><br><strong>Unleash your fashion sense<\/strong><br>Customize your appearance by switching the visuals of your armors and weapons. Loot more than 100 different dyes and tune up your armor to have your very own unique style. The asymmetric armor system will also allow you to change your appearance for left and right shoulder and glove.<br><br><strong>Difficulty modes<\/strong><br>Choose how you want to make the campaign with 2 different difficulty settings: the Story mode and the Normal mode. The endgame is shaped to allow a progressive increase in the difficulty.<br><br><strong>Regular updates and seasonal events<\/strong><br>We\u2019re committed to make Wolcen a long-term game with regular updates and additions, including features, Acts, gameplay content, Quality of Life, PvP, Housing, and seasonal events.", "about_the_game": "You are one of the three survivors of the slaughter of Castagath. Rescued by Grand Inquisitor Heimlock, you were drafted into the Republic\u2019s Army of the Purifiers at a very young age to be trained in the military academy and become perfect soldiers against the supernatural. You also had the chance to benefit from Heimlock\u2019s occasional advice and training, which led you and your childhood friends, Valeria and Edric, to be called the \u201cChildren of Heimlock\u201d.<br>Recently, the Brotherhood of Dawn has infiltrated the Crimson Keep, a mysterious republican fortress lost among the northern deserts known as the Red wastes. While the purpose of the attack was unclear, the republican Senate voted a retaliation act against all known locations of the Brotherhood. <br><br>Led by Grand Inquisitor Heimlock himself, troops are soon deployed on the Coast of wrecks, near the city state of Stormfall, to terminate a camp of Brothers. You are, with your two childhood friends, part of operation Dawnbane, under the supervision of Justicar Ma\u00eblys.  <br><br><u>Key features:<\/u><br><br><strong>Free character development<\/strong><br>Wield a great variety of weapons and find your own playstyle thanks to their unique stances and combos. In Wolcen, there is no class, only your weapons set the rules for your skills types. <br><br><strong>Three types of resources<\/strong><br>Rage and Willpower interact with each other, using the Resource Opposition System. Stamina allows you to use a dodge-roll to avoid danger or travel faster.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/Chest.gif?t=1581614791\" ><br><strong>Item diversity<\/strong><br>Gear up according to your offensive and defensive choices with common, magical, rare and legendary items. Break the rules and unlock new possibilities with Unique items and rare affixes.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/PST.gif?t=1581614791\" ><br><strong>Rotative Passive Skill Tree<\/strong><br>Make your own path throughout the 21 sub-class sections available in the Gate of Fates to customize your passives and make them fit to your play style.<br><br><strong>Skill customization<\/strong><br>Level up your skills with your character or alternative resources to gain modifier points and create your own unique combination of skill modifiers. Change your damage type, add new functionalities, grant buffs or debuffs, change the skill\u2019s mechanics completely. The options are limitless.<br><br><strong>Strategic fights<\/strong><br>Wolcen\u2019s creatures have complex patterns including deadly skills. Pay attention to various markers and to animation anticipation to avoid lethal attacks using your dodge roll ability.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/ApoForm.gif?t=1581614791\" ><br><strong>Aspects of Apocalypse<\/strong><br>All characters can shapeshift into one of the 4 Celestial incarnations available, each of them offering 4 different skills, and one devastating ultimate.<br><br><strong>Endless replayability<\/strong><br>Improve your gear by looting or crafting, gather resources to unlock rare missions, face advanced challenges for special rewards, experiment new builds, become the greatest achiever. Whether you like to play solo or with friends, there is always something to do.<br><br><strong>A taste for beauty<\/strong><br>Using the Cryengine technology makes Wolcen an immersive and beautiful game with highly detailed armors and weapons. In addition, 8 hours of epic orchestral music will accompany you during your journey.<br><br><img src=\"https:\/\/steamcdn-a.akamaihd.net\/steam\/apps\/424370\/extras\/Custo.gif?t=1581614791\" ><br><strong>Unleash your fashion sense<\/strong><br>Customize your appearance by switching the visuals of your armors and weapons. Loot more than 100 different dyes and tune up your armor to have your very own unique style. The asymmetric armor system will also allow you to change your appearance for left and right shoulder and glove.<br><br><strong>Difficulty modes<\/strong><br>Choose how you want to make the campaign with 2 different difficulty settings: the Story mode and the Normal mode. The endgame is shaped to allow a progressive increase in the difficulty.<br><br><strong>Regular updates and seasonal events<\/strong><br>We\u2019re committed to make Wolcen a long-term game with regular updates and additions, including features, Acts, gameplay content, Quality of Life, PvP, Housing, and seasonal events.",
        "short_description": "A dynamic hack\u2019n\u2019slash with no class restrictions. Choose your path as you level-up and play your character the way you want! Explore this shattered and corrupted world to uncover its ancient secrets and hidden truths.",
        "supported_languages": "English<strong>*</strong>, German, French, Russian, Simplified Chinese, Portuguese - Brazil, Spanish - Spain, Polish<br><strong>*</strong>languages with full audio support",
        "header_image": "https://steamcdn-a.akamaihd.net/steam/apps/424370/header.jpg?t=1581614791",
        "website": "https://www.wolcengame.com",
        "pc_requirements":
        {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows 7 64-Bit SP1, Windows 8.1 64-Bit, Windows 10 64-Bit<br></li><li><strong>Processor:</strong> Intel Core i5-4570T 2.9 GHz / AMD FX-6100 3.3 GHz<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 560 Ti / AMD Radeon HD 6850<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Storage:</strong> 18 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> Windows 7 64-Bit SP1, Windows 8.1 64-Bit, Windows 10 64-Bit<br></li><li><strong>Processor:</strong> Intel Core i7-4770S 3.1 GHz / AMD FX-8320 3.5 GHz<br></li><li><strong>Memory:</strong> 16 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GeForce GTX 1060 / AMD Radeon RX 570<br></li><li><strong>DirectX:</strong> Version 11<br></li><li><strong>Storage:</strong> 18 GB available space</li></ul>"
        },
        "mac_requirements": [
        ],
        "linux_requirements": [],
        "developers": [
            "WOLCEN Studio"
        ],
        "publishers": [
            "WOLCEN Studio"
        ],
        "genres": [
            {
                "id": "1",
                "description": "Action"
            },
            {
                "id": "25",
                "description": "Adventure"
            },
            {
                "id": "23",
                "description": "Indie"
            },
            {
                "id": "3",
                "description": "RPG"
            }
        ],
        "screenshots": [{
            "id": 0,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_5f18a02d970c0e54907632ac037316b54c9f1fee.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_5f18a02d970c0e54907632ac037316b54c9f1fee.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 1,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_2a6888f545e5b7a7532bf85e18674787187469a0.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_2a6888f545e5b7a7532bf85e18674787187469a0.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 2,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_0d4185910bf9608c2985003a7447a73dc3c8f0e9.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_0d4185910bf9608c2985003a7447a73dc3c8f0e9.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 3,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_7462fb819590850cdeb497e072a09f73cc732da2.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_7462fb819590850cdeb497e072a09f73cc732da2.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 4,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_50dbebb3278191817aad6a5875091d6d870639f3.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_50dbebb3278191817aad6a5875091d6d870639f3.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 5,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_c668697ab6f16a493c62891b8f62dbc69d2d8139.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_c668697ab6f16a493c62891b8f62dbc69d2d8139.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 6,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_77f4a7d71cfa06fc4e8139ac541ce73e90dad3d5.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_77f4a7d71cfa06fc4e8139ac541ce73e90dad3d5.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 7, "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_d524595de47b5b3784d60e53fee24389e0b227ba.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_d524595de47b5b3784d60e53fee24389e0b227ba.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 8,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_9124891066689393c713e43f994320217df010d2.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_9124891066689393c713e43f994320217df010d2.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 9,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_dc8fc40da9a504142bb84c2cd19cf23ac539795a.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_dc8fc40da9a504142bb84c2cd19cf23ac539795a.1920x1080.jpg?t=1581614791"
        },
        {
            "id": 10,
            "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_a7e5c4f79237045b6d4d6bcd76529042778ef4ce.600x338.jpg?t=1581614791",
            "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/424370/ss_a7e5c4f79237045b6d4d6bcd76529042778ef4ce.1920x1080.jpg?t=1581614791"
        }],
        "movies": [
            {
                "id": 256775122,
                "name": "Wolcen: Lords of Mayhem - Release Trailer",
                "thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/256775122/movie.293x165.jpg?t=1581612044",
                "webm":
                {
                    "480": "http://steamcdn-a.akamaihd.net/steam/apps/256775122/movie480.webm?t=1581612044",
                    "max": "http://steamcdn-a.akamaihd.net/steam/apps/256775122/movie_max.webm?t=1581612044"
                },
                "highlight": true
            }
        ],
        "release_date": { "coming_soon": false, "date": "13 Feb, 2020" },
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/424370/page_bg_generated_v6b.jpg?t=1581614791",
    },
    {
        "steam_appid": 836620,
        "name": "Black Desert",
        "price": 200,
        "img": "https://honknews.com/wp-content/uploads/2019/11/Black-Desert-Online.jpg",
        "detailed_description": "<h1>Black Desert, a true MMORPG</h1><p>Open World with intense action and rich content.<br><br><strong>Black Desert</strong> has a seamless visualization without loading and the highest levels of graphics. It features MMO world that is always active with various contents.<br><br>Here's a gorgeous, deeper action.<br>We have Social structures such as various life contents and guild contents which lead to making our users enthusiastic to play Black Desert.<br><br>The best MMORPG, Black Desert is always ready to meet our adventurers with a variety of classes and awakenings (weapons)</p><br><h1>PRE-EXISTING USERS</h1><p><h2 class='bb_ta'>The Steam version of Black Desert requires the purchase of the game from Steam as well as the creation of a new Black Desert account. Pre-existing Black Desert accounts created from methods other than Steam will not be able to play via Steam.</h2></p><br><h1>About the Game</h1><h2 class='bb_tag'><strong>Storyline</strong></h2><br>Black Stones...<br>The mysterious Black Stones are a source of power that were known to Ancients and was instrumental in the rise of the Ancient Civilization. However, legend has it that it also corrupted their minds and caused the fall of their great civilization. <br><br>The Black Stones are commonly found in the vast desert between the Republic of Calpheon and the Kingdom of Valencia Calpheonians call this area the Black Desert and have declared a war to claim the resources there. Valencians know it as the Red Desert due to all the blood spilled there. <br><br>You will uncover the hidden secrets of the Ancient Civilization through the histories of Calpheon and Valencia. You will unravel lost memories and the secrets of your enigmatic guide, the Black Spirit. A journey to seek the truths of the Ancient Civilization and the Black Desert awaits you.<h2 class='bb_tag'><strong>Remastered Cutting-Edge Graphics</strong></h2>An expansive, open world MMORPG made with Pearl Abyss’ proprietary game engine.<br>With spectacular sceneries, realistic environments, and sophisticated details.<br>Enter a world created with state-of-the-art graphics and prepare for unforgettable adventures.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden1.png?t=1582059173' ><h2 class='bb_tag'><strong>Non-Target &amp; Fast-Paced Combat</strong></h2>Experience an innovative combat system unlike any other MMORPG.<br>The need for quick reactions and precise control will keep you enthralled throughout your entire experience.<br>Exciting commands/combos will always keep you on your toes.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden2.png?t=1582059173' ><h2 class='bb_tag'><strong>Large-Scale PVP Wars</strong></h2>Guilds, prepare for battle! Victory is near!<br>Join formidable guilds as they fight head-to-head on the battlefield.<br>Be victorious, become lords, and seize control of taxes, resource, and more!<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden3.png?t=1582059173' ><h2 class='bb_tag'><strong>Multitudes of In-Game Activities</strong></h2>Don't feel like fighting? Countless other activities await!<br>Farming, trading, horse training, fishing, sailing, crafting, cooking…<br>Get yourself involved, and dive into diverse life skills!<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden4.png?t=1582059173' ><h2 class='bb_tag'><strong>Knowledge-Based Game Content</strong></h2>Knowledge is power! <br>Stick around, and become an expert of the Black Desert world.<br>Gain knowledge from combat, exploring, or interacting with NPCs.<br>Knowledge will influence your gameplay and assist you on your adventures.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden5.png?t=1582059173' ><h2 class='bb_tag'><strong>Become Your True Self</strong></h2>Experience the insane level of customization.<br>From height, torso, face, skin texture, to micro-level details, you can break out of the norm!<br>Create your own unique characters, just like you've always desired, in Black Desert.<br><img src='https://steamcdn-a.akamaihd.net/steam/apps/836620/extras/featureHidden6.png?t=1582059173' >",
        "short_description": "Experience the vast open world of the Black Desert and immerse yourself in a game that redefines the MMORPG genre with its stunning graphics, skill-based combat, and Siege wars. Adventurers can also explore, fish, tame horses, cook, gather, hunt and so much more in the ultimate MMORPG, Black Desert.",
        "supported_languages": "English<strong>*</strong>, Turkish, Russian<strong>*</strong>, Thai<br><strong>*</strong>languages with full audio support",
        "header_image": "https://wallpaperaccess.com/full/1283670.jpg",
        "website": "https://steam.playblackdesert.com",
        "pc_requirements":
        {
            "minimum": "<strong>Minimum:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> 32/64 bit Windows 7, Windows 8, Windows 8.1, Windows 10<br></li><li><strong>Processor:</strong> Intel Core i3<br></li><li><strong>Memory:</strong> 4 GB RAM<br></li><li><strong>Graphics:</strong> GTS 250 / GeForce 9800GTX Radeon HD 3870 X2<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space</li></ul>",
            "recommended": "<strong>Recommended:</strong><br><ul class='bb_ul'><li><strong>OS:</strong> 32/64 bit Windows 7, Windows 8, Windows 8.1, Windows 10<br></li><li><strong>Processor:</strong> Intel Core i5<br></li><li><strong>Memory:</strong> 8 GB RAM<br></li><li><strong>Graphics:</strong> NVIDIA GTX 970 AMD RX 480<br></li><li><strong>Network:</strong> Broadband Internet connection<br></li><li><strong>Storage:</strong> 30 GB available space</li></ul>"
        },
        "mac_requirements": [],
        "linux_requirements": [],
        "developers": [
            "Pearl Abyss"
        ],
        "publishers": [
            "Pearl Abyss"
        ],
        "metacritic": {
            "score": 73,
            "url": "https://www.metacritic.com/game/pc/black-desert-online?ftag=MCD-06-10aaa1f"
        },
        "genres": [
            {
                "id": "29",
                "description": "Massively Multiplayer"
            },
            {
                "id": "3",
                "description": "RPG"
            }
        ],
        "screenshots": [
            {
                "id": 0,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_709cd913d25e67d3b8f7f37eb0602eda42754d36.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_709cd913d25e67d3b8f7f37eb0602eda42754d36.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 1,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_8166cc8400b6112720139f42e34aea95c1411ce1.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_8166cc8400b6112720139f42e34aea95c1411ce1.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 2,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_50cf579c812a6ace778973129cacfa6f2dc2fa4b.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_50cf579c812a6ace778973129cacfa6f2dc2fa4b.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 3,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_6b712cd99421588e1a3cdf087019808768b241e6.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_6b712cd99421588e1a3cdf087019808768b241e6.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 4,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_71b968f17f1c84ce0f1b2d31cfd66f567b3aaf6a.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_71b968f17f1c84ce0f1b2d31cfd66f567b3aaf6a.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 5,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_5a39cf3cbca8a96707aa14f2bd343119cc53151f.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_5a39cf3cbca8a96707aa14f2bd343119cc53151f.1920x1080.jpg?t=1582059173"
            },
            {
                "id": 6,
                "path_thumbnail": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_26549bbcd3d1700e12f09adb3df41e223aeec801.600x338.jpg?t=1582059173",
                "path_full": "https://steamcdn-a.akamaihd.net/steam/apps/836620/ss_26549bbcd3d1700e12f09adb3df41e223aeec801.1920x1080.jpg?t=1582059173"
            }],
        "movies": [
            {

                "name": "Black Desert Online - Remastered Trailer",
                "webm":
                {
                    "max": "https://www.youtube.com/watch?v=_g6XCLNQl_w"
                },
                "highlight": true
            }
        ],
        "release_date": {
            "coming_soon": false,
            "date": "18 Sep, 2019"
        },
        "background": "https://steamcdn-a.akamaihd.net/steam/apps/836620/page_bg_generated_v6b.jpg?t=1582059173",


    }


]
