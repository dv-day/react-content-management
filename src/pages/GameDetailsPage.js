import React, { useState, useEffect } from "react";
import { Layout, BackTop, Row, Col } from "antd"
import VideoSection from "../component/GameDetailsComponent/FirstSection/VideoSection"
import DetailsSection from "../component/GameDetailsComponent/SecondSection/DetailsSection"
import SystemRequireSection from "../component/GameDetailsComponent/SecondSection/SystemRequireSection"
import ReviewsSection from "../component/GameDetailsComponent/ThirdSection/ReviewsSection"
import PageHeaderSection from "../component/GameDetailsComponent/FirstSection/PageHeaderSection";
import { connect } from 'react-redux'
import { fetchData } from '../reducer/GameReducer'
import { bindActionCreators } from 'redux'

const GameDetailsPage = (props) => {
    const { Content } = Layout

    const { fetchData } = props;
    const { loadingDetail, data ,comments} = props.data;
    
    const [name, setName] = useState();
    const [detail, setDetail] = useState();
    const [release, setRelease] = useState();
    const [develop, setDevelop] = useState();
    const [publisher, setPublisher] = useState();
    const [genres, setGenres] = useState();
    const [album, setAlbum] = useState();
    const [pc, setPc] = useState();
    const [linux, setLinux] = useState();
    const [mac, setMac] = useState();
    const [movies, setMovies] = useState();
    const [metacriticScore, setMetacriticScore] = useState();
    const [metacriticUrl, setMetacriticUrl] = useState();
    const [background, setBackground] = useState();
    const [price, setPrice] = useState();
    const [targetData, settargetData] = useState();

    useEffect(() => {
        fetchData();
      
    }, []);
   
    useEffect(() => {
      if (targetData) {
        setName(targetData.name);
        // setImage(targetData.img);
        setPrice(targetData.price);
        setDetail(targetData.detailed_description);
        setDevelop(targetData.developers);
        setRelease(targetData.release_date.date);
        setPublisher(targetData.publishers);
        setGenres(targetData.genres);
        setAlbum(targetData.screenshots);
        setPc(targetData.pc_requirements.minimum);
        setLinux(targetData.linux_requirements.minimum);
        setMovies(targetData.movies[0].webm.max);
            setMetacriticScore(targetData.metacritic.score);
            setMetacriticUrl(targetData.metacritic.url)
        setBackground(targetData.background)
        setMac(targetData.mac_requirements.minimum);
      }
    }, [targetData]);
  
    useEffect(() => {
      getDetail();
    }, [data]);

  console.log(data)
  console.log(targetData)

    const getDetail = () => {
        
      const filter = data.find(value => {
        return value.steam_appid == props.match.params.steam_appid;
      });
      settargetData(filter);
    };

    const refreshPage = () => {
        if (!window.location.hash) {
            window.location = window.location + '#L';
            window.location.reload();
        }
    }

    return (
        <Layout style={{ backgroundColor: '#2B2B2B', height: '100%' }} >
            <Content>
                <Content>
                    <img src={background} style={{
                        position: 'relative',
                        backgroundSize: 'cover',
                        content: "",
                        display: 'block',
                        width: '100%',
                        height: '500px',
                        WebkitFilter: 'grayscale(100%)',
                        overflow: 'hidden',
                        marginBottom: '-500px'
                    }} />
                    <Row type='flex' justify='start'>
                        <Col>
                            <PageHeaderSection name={name} />
                        </Col>
                    </Row>
                    <Row type='flex' justify='center'>
                        <Col>
                            <VideoSection movie={movies} />
                        </Col>
                    </Row>
                </Content>

                <Row>
                    <Col>
                        <DetailsSection
                            detail={detail}
                            release={release}
                            publisher={publisher}
                            develop={develop}
                            genres={genres}
                            images={album}
                            price={price} />
                        <SystemRequireSection pc={pc} linux={linux} mac={mac} />
                        <ReviewsSection
                            metacriticScore={metacriticScore}
                            metacriticUrl={metacriticUrl} />
                    </Col>
                </Row>
            </Content>
            <BackTop />
        </Layout>
    )
}

const mapStateToProps = state => {
    return {
        data: state.data
    };
};
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(GameDetailsPage)