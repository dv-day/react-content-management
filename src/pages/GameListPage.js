import React, { useEffect, useState } from "react";
import GameListTool from "../component/GameListPageComponent/GameListTool";
import GameList from "../component/GameListPageComponent/GameList";
import { useDispatch, connect } from "react-redux";
import { fetchData, fetchUsers } from "../action/GameListAction";
import { data } from "../data";
import { Select, Row, Col } from "antd";
import { bindActionCreators } from "redux";
const { Option } = Select;
const GameListPage = () => {
  const dispatch = useDispatch();
  const [sorting, setSorting] = useState("none");

  const handleChange = value => {
    setSorting(value);
  };

  const sortArray = () => {
    if (sorting == "none") {
      return data;
    } else if (sorting == "byName") {
      return data.sort(function(a, b) {
        var nameA = a.name.toLowerCase(),
          nameB = b.name.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
      });
    } else if (sorting == "byPrice") {
      return data.sort(function(a, b) {
        if (a.price < b.price) return -1;
        if (a.price > b.price) return 1;
        return 0;
      });
    }
  };

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  return (
    <div>
      <GameList />
    </div>
  );
};

export default GameListPage;
