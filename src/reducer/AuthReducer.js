import {
  LOGIN_BEGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  SET_PASSWORD,
  SET_USERNAME,
  loginBegin,
  loginFail,
  loginSucess,
  resetError
} from "../actions/AuthAction";
import { admin } from "../admin";

export const loginProcess = (username, password) => {
  return (dispatch) => {
    dispatch(loginBegin());
    setTimeout(() => {
      let currentUser = admin.find((value) => {
        return value.username == username && value.password == password;
      });
      if (currentUser) {
        dispatch(loginSucess(username, password));
      } else {
        dispatch(loginFail());
      }
    }, 1000);
  };
};

const initialState = {
  process: false,
  isLogin: false,
  username: "",
  password: "",
  name: "",
  err: ""
};
export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_BEGIN:
      return {
        ...state,
        process: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLogin: true,
        name: action.payLoad,
        process: false
      };
    case LOGIN_FAIL:
      return {
        ...state,
        process: false,
        err: "Error"
      };
    case "RESET_ERROR":
      return {
        ...state,
        err: ""
      };
    case LOGOUT:
      return {
        ...state,
        isLogin: false
      };
    default:
      return state;
  }
};
