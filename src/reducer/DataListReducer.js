import {
  FETCH_GAMELIST,
  SEARCH_GAMELIST,
  FETCH_DATA_LIST_SUCCESS,
  FETCH_DATA_LIST_ERROR,
  FILTER_GAMELIST,
} from "../action/GameListAction";


const initialState = {
  data: [],
  loading: false,
  filterType: 'All',
  searchData: '',
  err: "",
};

export const gameListReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_GAMELIST:
      return {
        ...state,
        loading: true
      }
    case FETCH_DATA_LIST_SUCCESS:
      return {
        ...state,
        data: action.payLoad,
        loading: false,
        err: ''
      };
    case FETCH_DATA_LIST_ERROR:
      return {
        ...state,
        loading: false,
        err: action.payLoad
      }
    case SEARCH_GAMELIST:
      return {
        ...state,
        searchData: action.payLoad
      };
    case FILTER_GAMELIST:
      return {
        ...state,
        filterType: action.payLoad
      }

    default:
      return state;
  }
};


// {
//   id: 1,
//   title: "ARK: Survival Evolved",
//   img:
//     "https://cdn-products.eneba.com/resized-products/abo2bjo9wt1yb0hnprml_390x400_1x-0.jpg",
//   price: "579.00"
// },
// {
//   id: 2,
//   title: "Counter-Strike: Global Offensive",
//   img:
//     "https://images-na.ssl-images-amazon.com/images/I/81L8-mjNlrL._SX425_.jpg",
//   price: "Free"
// },
// {
//   id: 3,
//   title: "Dota 2",
//   img:
//     "https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg",
//   price: "Free"
// },
// {
//   id: 4,
//   title: "Grand Therft Auto V",
//   img: "https://www.gamesteam.in.th/assets/header/gs_8021458531911.jpg",
//   price: "699.00"
// },
// {
//   id: 5,
//   title: "Gang Beasts",
//   img: "https://static-cdn.jtvnw.net/ttv-boxart/Gang%20Beasts.jpg",
//   price: "369.00"
// },
// {
//   id: 6,
//   title: "Monster Hunter : World",
//   img: "https://file-cdn.gvgmall.com/product/20180720154708_gvgmall.jpg",
//   price: "739.00"
// },
// {
//   id: 7,
//   title: "Playerunknown's Battlegrounds",
//   img: "https://dq.lnwfile.com/bjh722.jpg",
//   price: "559.00"
// },
// {
//   id: 8,
//   title: "Tom Clancy's Rainbow Six® Siege",
//   img:
//     "https://gpstatic.com/acache/27/78/1/de/packshot-b1bb0e38b2c747f891db8e4a836f79f6.jpg",
//   price: "600.00"
// },
// {
//   id: 9,
//   title: "RESIDENT EVIL 2 / BIOHAZARD RE:2",
//   img:
//     "https://store-images.s-microsoft.com/image/apps.36567.71571739681700792.c61fa2d7-3d69-4edf-8e51-accc41d47823.a8c614fc-0dcb-4788-89ed-958635753296",
//   price: "1199.00"
// },
// {
//   id: 10,
//   title: "Devil May Cry 5",
//   img:
//     "https://gpstatic.com/acache/38/23/1/de/packshot-f1bbc1240748236563949fddf4edaf12.jpg",
//   price: "1339.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// },
// {
//   id: 11,
//   title: "Planet Zoo",
//   img: "https://keytrust.dk/wp-content/uploads/2019/11/planet-zoo.jpg",
//   price: "975.00"
// }
// ],