import { FETCH_GAME_DETAIL_BEGIN, FETCH_GAME_DETAIL_SUCCESS, FETCH_GAME_DETAIL_ERROR } from "../actions/DetailAction"

const initialState = {
    gameDetail: null,
    loading: false,
    error: ''
}


  

export const DetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GAME_DETAIL_BEGIN:
            return {
                ...state,
                loadingDetail: true
            }
        case FETCH_GAME_DETAIL_SUCCESS:
            return {
                ...state,
                gameDetail: action.payLoad,
                loadingDetail: false,
                error: ''
            }
        case FETCH_GAME_DETAIL_ERROR:
            return {
                ...state,
                loadingDetail: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
