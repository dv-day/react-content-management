import {
  FETCH_DATA_BEGIN,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_ERROR,
  fetchDataBegin,
  fetchDataSuccess,
  fetchDataError,
  fetchCommentsAction,
  SUBMIT
} from "../actions/GameActions";
import { data } from "../data";
import {comments} from '../comment'
import axios from 'axios';

export const fetchData = () => {
  return dispatch => {
    dispatch(fetchDataBegin());
    setTimeout(() => {
      axios.get('http://18.141.146.223:4999/games')
  .then(function (response) {
    console.log(response)
    dispatch(fetchDataSuccess(response.data));
  })
  .catch(function (error) {
    dispatch(fetchDataError(error))
  })
    }, 1000);
  };
};

export const fethComments = () =>{
  return dispatch =>{
    dispatch(fetchCommentsAction(comments))
    console.log(comments)
  }
}

const initialState = {
  data: [],
  loading: false,
  err: "",
  Detailerr: "",
  targetData: null,
  targetId: null,
  loadingDetail: false,
  comments : []
};
export const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_DATA_SUCCESS:
      return {
        ...state,
        data: action.payLoad,
        loading: false
      };

    case FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        err: action.payLoad
      };

      case 'FETCH_COMMENTS' :
        //alert(action.payLoad)
        return {
          ...state,
          comments : action.payLoad
        }

    case SUBMIT:
    //  alert(action.payLoad.steam_appid)
      axios.put('http://18.141.146.223:4999/update/game/'+action.payLoad.steam_appid, action.payLoad)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
      // console.log("newData", newData);

      // let newArray = [...state.data];
      // console.log("newArray", newArray);

      // let inex = 0;
      // newArray.forEach((value, index) => {
      //   if (value.steam_appid == newData.steam_appid) {
      //     console.log("value", value);
      //     inex = index;
      //     console.log("inex", inex);
      //     return;
      //   }
      // });

      // newArray[inex] = newData;

      alert("Submit");
      return state
      

    default:
      return state;
  }
};
