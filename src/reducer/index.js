import { gameListReducer } from "./DataListReducer";
import { combineReducers } from "redux";
import { gameReducer } from "./GameReducer";
import { DetailReducer } from "./DetailReducer"
import { authReducer } from "./AuthReducer";
import { NavReducer } from "./NavReducer";
import { dataHomeReducer} from "./ReducerHome"

export const rootReducer = combineReducers({
    listGame: gameListReducer,
	data: gameReducer,
	detailState: DetailReducer,
	login: authReducer, 
	toggle : NavReducer,
	dataHomePage: dataHomeReducer

})

